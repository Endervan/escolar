<?php



function onesignal_send_msg($titulo, $mensagem, $player_ids){
    
    $content = array(
        "en" => $mensagem
    );
    $title = array(
        "en" => $titulo
    );

    $fields = array(
        'app_id' => "419e54e4-d07a-4a1f-8b33-80949224285c", //  'app_id' => "20e25976-4742-413e-af5a-62cbb660a6f1",
        //'included_segments' => array('All'),
        'include_player_ids' => array($player_ids),
        'data' => array("foo" => "bar"),
        //  'large_icon' =>"ic_launcher_round.png",
        'headings' => $title,
        'contents' => $content
        //'attachments' => array("url" => 'http://www.google.com')
    );

    $fields = json_encode($fields);
    //print("\nJSON sent:\n");
    //print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic NzJlMmY0ODctM2U0Yy00ZWRkLTk1YWQtNjAzMTM2NzYxZDVk'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic NjM0YjNjYzUtMDgzZi00NzEyLTliMTYtOTJlZjg1YTFkYTJi'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);


    //  salva a notificação
    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();
    $dados = array( 
                    "titulo" => $titulo, 
                    "mensagem" => $mensagem, 
                    "player_ids" => $player_ids 
                  );
    $ci->db->insert('notificacoes_onesignal', $dados);
    
    //echo $response;
    return $response;


}



//onesignal_send_msg("Teste localhost - ". rand(), "Estou testando localmente para ver se envia.", '43654d40-ac66-428f-89f4-20e934e1f1b5');
//echo 'onesignal_send_msg' ;

?>