<div class="container">
    <div class="row">
        <div class="col-12">
            <h1><?php echo $title; ?></h1>

            <div class="table-responsive">
                <table class="table ">
                    <thead>
                        <tr>
                        <th scope="col">Usuario</th>
                        <th scope="col">Responsável</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Escola</th>
                        <th scope="col">Sala</th>
                        <th scope="col">Turno</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($query->result() as $row): ?>
                            <tr>
                                <th scope="row"><?php echo $row->usuario_nome; ?></th>
                                <td><?php echo $row->responsavel_nome; ?></td>
                                <td><?php echo $row->responsavel_cpf; ?></td>
                                <td><?php echo $row->aluno_nome; ?></td>
                                <td><?php echo $row->escola_nome; ?></td>
                                <td><?php echo $row->aluno_sala; ?></td>
                                <td><?php echo $row->aluno_turno; ?></td>
                            </tr>
                        <?php endforeach; ?> 
                    </tbody>
                </table>
            </div>


        </div>
    </div>
</div>



    
    

