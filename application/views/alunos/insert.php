<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center mt-1"><?php echo $title; ?></h1>

            <?php
            
            ?>

            <?php echo validation_errors(); ?>

            <?php echo form_open('alunos/insert'); ?>


            <div class="row">
                <div class="col-9">
                    <div class="form-group">
                        <label>Nome</label>
                        <input type="text" class="form-control" name="nome" id="nome" value="<?php echo set_value('nome'); ?>" >
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group">
                        <label>Sexo</label>
                        <input type="text" class="form-control" name="sexo" id="sexo" value="<?php echo set_value('sexo'); ?>" >
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-3">
                    <div class="form-group">
                        <label>Sala</label>
                        <input type="text" class="form-control" name="sala" id="sala" value="<?php echo set_value('sala'); ?>" >
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group">
                        <label>Turno</label>
                        <input type="text" class="form-control" name="turno" id="turno" value="<?php echo set_value('turno'); ?>" >
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group">
                        <label>Responsável</label>
                        <input type="text" class="form-control" name="id_responsavel" id="id_responsavel" value="<?php echo set_value('id_responsavel'); ?>" >
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group">
                        <label>Escola</label>
                        <input type="text" class="form-control" name="id_escola" id="id_escola" value="<?php echo set_value('id_escola'); ?>" >
                    </div>
                </div>
            
            </div>


                <input type="submit" name="submit" value="Cadastrar" class="btn btn-primary" />

            </form>


        </div>
    </div>
</div>