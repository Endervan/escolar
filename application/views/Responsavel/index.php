<div class="container">
    <div class="row">
        <div class="col-12">
            <h1><?php echo $title; ?></h1>

            <div class="table-responsive">
                <table class="table ">
                    <thead>
                        <tr>
                        <th scope="col">Usuario</th>
                        <th scope="col">Responsável</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Endereço</th>
                        <th scope="col">Complemento</th>
                        <th scope="col">Bairro</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">UF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($query->result() as $row): ?>
                            <tr>
                                <th scope="row"><?php echo $row->id_usuario; ?></th>
                                <td><?php echo $row->nome; ?></td>
                                <td><?php echo $row->cpf; ?></td>
                                <td><?php echo $row->endereco; ?></td>
                                <td><?php echo $row->complemento; ?></td>
                                <td><?php echo $row->bairro_titulo; ?></td>
                                <td><?php echo $row->cidade_titulo; ?></td>
                                <td><?php echo $row->uf_titulo; ?></td>
                            </tr>
                        <?php endforeach; ?> 
                    </tbody>
                </table>
            </div>


        </div>
    </div>
</div>



    
    

