<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Endereco_aluno extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('endereco_aluno_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }



    // funcao para atualizar a ordem
    function atualizaOrdem(){

        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

      

        $this->endereco_aluno_model->atualizaOrdem($_POST['from'], $_POST['to'], $_POST['element'], $_POST['tipo_rota']);
        $msg = array('mensage' =>  "Ordem atualizada com sucesso.", "status" => TRUE );
        echo json_encode($msg);
    }

    


    // // funcao cadastrar pelo app
    public function insert(){
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

       
        //  validando o formulario
        $data['erros'] = $this->valida_form('insert');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $idenderecoaluno = $this->endereco_aluno_model->cadastra( $this->input->post(), 'insert' );
            $msg = array('mensage' => "Endereço do aluno inserido com sucesso.", "status" => TRUE, "idenderecoaluno" => $idenderecoaluno );
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }


    //  atualiza dados
    public function update(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

           //  validando o formulario
        $data['erros'] = $this->valida_form('update');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->endereco_aluno_model->update( $this->input->post(), $_POST['idenderecoaluno'] ,'update' );
            $msg = array('mensage' => "Endereço do aluno alterado com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'] . "ERRO DE CAMPO DO FORM", "status" => FALSE );
            echo json_encode($msg);
        }
    }



    //  deleta o responsável
    public function delete($idusuario, $idenderecoaluno){
        $result = $this->endereco_aluno_model->delete($idusuario, $idenderecoaluno);
        $msg = array('mensage' => "Endereço do aluno excluído com sucesso.", "status" => TRUE);
        echo json_encode($msg);
    }



    // funcao para retornar dados
    public function getDados($id_usuario, $id_aluno){
        $this->db->select("*");
        $this->db->from('enderecos_alunos');
        // $this->db->join('alunos', 'enderecos_alunos.id_aluno = alunos.idaluno');       
        $this->db->join('alunos', 'enderecos_alunos.embarca_depois_idaluno = alunos.idaluno', 'left');       
        $this->db->where('enderecos_alunos.id_usuario', $id_usuario);
        $this->db->where('enderecos_alunos.id_aluno', $id_aluno);
        $query = $this->db->get();
        $result = $query->result_array();

        $i = 0;
        foreach ($query->result_array() as $row){


            //  busco os dados do aluno  do desembarque
            $this->db->where('idaluno', $row['desembarca_depois_idaluno']);
            $query1 = $this->db->get('alunos');
            $result1 = $query1->result_array();
            $row1 = $query1->row();

            
            if(isset($row1->nome)){
                $result[$i]['aluno_desembarque'] = $row1->nome;  // COLOCA O ALUNO DO DESEMBARQUE NO ARRAY
            }else{
                $result[$i]['aluno_desembarque'] = "Primeiro aluno";
            }
            $i++;
        }


    //     $row = $query->row();

    //     //  busco os dados do aluno  do desembarque
    //     $this->db->where('idaluno', $row->desembarca_depois_idaluno);
    //     $query1 = $this->db->get('alunos');
    //   //  $result1 = $query1->result_array();
    //     // $row1 = $query1->row();
    //     //  echo $this->db->last_query();   //  exibe o sql executado

    //     $i = 0;
    //     foreach ($query1->result_array() as $row){
    //        echo " I = $i =======";
    //         if(isset($row['nome'])){
    //             $result[$i]['aluno_desembarque'] = $row['nome'];  // COLOCA O ALUNO DO DESEMBARQUE NO ARRAY
    //         }else{
    //             $result[$i]['aluno_desembarque'] = "Primeiro aluno";
    //         }
    //         $i++;

    //     }


        




        echo json_encode($result);
    }


    //  busca os alunos dos enderecos
    public function get_alunos_endereco($id_usuario, $id_aluno = ''){
        $this->db->select("*");
        $this->db->from('enderecos_alunos');
        $this->db->join('usuarios', 'enderecos_alunos.id_usuario = usuarios.idusuario');
        $this->db->join('alunos', 'enderecos_alunos.id_aluno = alunos.idaluno');
        $this->db->where('enderecos_alunos.id_usuario', $id_usuario);

        if(!empty($id_aluno)){
            $this->db->where('enderecos_alunos.id_aluno', $id_aluno); 
        }
        
        
        $this->db->group_by('enderecos_alunos.id_aluno'); 
        $this->db->order_by('alunos.nome');
        $query = $this->db->get();
        $result = $query->result_array();

        //  coloquei a opção para o usuario selecionar o primeiro aluno
        $primeiro = array( "id_aluno" => "0", "nome" => "Primeiro aluno" );
        array_unshift($result, $primeiro);

        //  echo $this->db->last_query();   //  exibe o sql executado
        echo json_encode($result);
    }






    //  busca os alunos dos enderecos
    public function get_alunos_endereco_rota($id_usuario, $turno, $tipo_rota){
        // $this->db->select("max(tb_enderecos_alunos.idenderecoaluno) as idenderecoaluno, tb_enderecos_alunos.*, tb_alunos.*");
        $this->db->select("max(tb_enderecos_alunos.idenderecoaluno) as idenderecoaluno, enderecos_alunos.id_usuario, enderecos_alunos.rota_1_ordem, enderecos_alunos.rota_2_ordem, enderecos_alunos.rota_3_ordem, enderecos_alunos.rota_4_ordem, enderecos_alunos.id_aluno,
                           alunos.nome, alunos.idaluno, alunos.turno");
        $this->db->from('enderecos_alunos');
        $this->db->join('usuarios', 'enderecos_alunos.id_usuario = usuarios.idusuario');
        $this->db->join('alunos', 'enderecos_alunos.id_aluno = alunos.idaluno');
        $this->db->where('enderecos_alunos.id_usuario', $id_usuario);
        
        //  VERIFICO QUAL A ROTA DEVO SELECIONAR
        if($tipo_rota == 'embarque'){

            $turno = $this->endereco_aluno_model->getRotaTurno($tipo_rota, $turno);


            switch($turno['coluna_tb']){
                case "rota_1_ordem":   //  ida matutino e integral
                    $turno = array("I", "M");
                    $this->db->where_in('alunos.turno', $turno);
                    $rota_coluna = 'rota_1_ordem';
                break;
                case "rota_1_ordem": //  ida vespertino e volta matutino
                    $turno = array("I", "M");
                    $this->db->where_in('alunos.turno', $turno);
                    $rota_coluna = 'rota_1_ordem';
                break;
                case "rota_2_ordem": //  volta vespertino, integral e ida noturno
                    $turno = array("M", "V");
                    $this->db->where_in('alunos.turno', $turno);
                    $rota_coluna = 'rota_2_ordem';
                break;
                case "rota_3_ordem": //  volta noturno
                    $turno = array("V", "I", "N");
                    $this->db->where_in('alunos.turno', $turno);
                    $rota_coluna = 'rota_3_ordem';
                break;
            }



        }else{

        }
        




        // if(!empty($id_aluno)){
        //     $this->db->where('enderecos_alunos.id_aluno', $id_aluno);
        // }
        
        
        $this->db->group_by('enderecos_alunos.id_aluno');
        $this->db->order_by("enderecos_alunos.$rota_coluna");
        $query = $this->db->get();
        $result = $query->result_array();
        //    echo "<br />" . $this->db->last_query();   //  exibe o sql executado

        //  coloquei a opção para o usuario selecionar o primeiro aluno
        // $primeiro = array( "id_aluno" => "0", "nome" => "Primeiro aluno" );
        // array_unshift($result, $primeiro);

        //   echo $this->db->last_query();   //  exibe o sql executado
        echo json_encode($result);
    }


 









    //  validacao do formulario
    public function valida_form($action){

        //  seto o array para o post 
        //$this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if( $action == "update"){ //alterar
            $this->form_validation->set_rules('idenderecoaluno', 'código do endereço aluno', 'required', array('required' => 'O campo %s é obrigatório.')); 
        }
       
        //  validacao do formulario
        $this->form_validation->set_rules('titulo', 'titulo', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('endereco', 'endereço', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('id_aluno', 'código do aluno', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('cidade', 'cidade', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('uf', 'UF', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
       
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }









}
?>