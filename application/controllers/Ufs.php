<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ufs extends CI_Controller
{


    function __construct(){
        parent::__construct();
        $this->load->model('ufs_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }

    
     //  retorna todos os estados 
     public function getUfs(){
        $query = $this->ufs_model->getUfs();
        $result = $query->result_array();
        echo json_encode($result);
    }


    //  retorna as cidades de um determinado estado
    public function getCidades($iduf){
        $query = $this->ufs_model->getCidades($iduf);
        $result = $query->result_array();
        echo json_encode($result);
    }


     //  retorna os bairros de uma cidade
     public function getBairros($idcidade){
        $query = $this->ufs_model->getBairros($idcidade);
        $result = $query->result_array();
        echo json_encode($result);
    }

}
    