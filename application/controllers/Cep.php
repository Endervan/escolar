<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cep extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('cep_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }



function getCep($cep){
      
      $cep = str_replace("-", "", $cep);
      $result = $this->cep_model->getCep($cep);

      if($result){
            echo json_encode($result);        
      }else{
            $msg = array('mensage' => "Cep não encontrado.", "status" => FALSE);
            echo json_encode($msg);  
      }

      
}






}