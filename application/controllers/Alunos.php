<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alunos extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('alunos_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }


    //  envia a imagem do aluno
    public function uploadFoto(){

        $imagedata = $_POST['file'];
        $imagedata = str_replace('data:image/jpeg;base64,', '', $imagedata);
        $imagedata = str_replace('data:image/jpg;base64,', '', $imagedata);
        $imagedata = str_replace(' ', '+', $imagedata);
        $imagedata = base64_decode($imagedata);
        $target_path = time().'.jpg';   //  nome da imagem
        file_put_contents('uploads/'.$target_path, $imagedata);    //  escreve a imagem na pasta
        // echo $imagedata;

        $this->load->library('image_lib');

        //  cria a tumb
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'uploads/'.$target_path;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = FALSE;
        $config['width']         = 150;
        $config['height']       = 150;

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();


        //  redimensiona a imagem
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'uploads/'.$target_path;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width']         = 1000;
        $config['height']       = 1000;
        // $config['new_image'] = 'uploads/new-'.time().'.jpg';

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();

        
        $data['result'] = true;
        $data['image_url'] = $target_path;
        echo json_encode($data);

    }




    //  retorna todos os alunos do usuario
    public function getAlunos($idusuario, $retorna_json = "sim"){
        $query = $this->alunos_model->get_alunos($idusuario);
        
        if($retorna_json == "sim"){
            $result = $query->result_array();
            echo json_encode($result);    
        }else{
            return $result = $query->result_array();
        }
        
    }


    //  retorna todos os alunos do usuario
    public function getAluno($idusuario, $idaluno){
        $query = $this->alunos_model->get_alunos($idusuario, $idaluno);
        $result = $query->result_array();
        echo json_encode($result);
    }



    // funcao para retornar dados
    public function getDados($responsavel = 1){
        $this->db->where('id_responsavel', $responsavel);
        $query = $this->db->get('alunos');
        $result = $query->result_array();
        echo json_encode($result); 
    }


    //  busca o financeiro do aluno
    function getFinanceiroAluno($idusuario, $idaluno){
        $this->db->where('id_aluno', $idaluno);
        $query = $this->db->get('tb_fluxo_caixa');
        $nr_linhas = $result = $query->num_rows();

        //  faco um loop para organizar o array
        $i =0;
        foreach ($query->result_array() as $row){
            $dados[ $row['idfluxocaixa'] ] = $row;
            $dados[ $row['idfluxocaixa'] ]['data'] = date("Y-m-d", strtotime($row['data']));
            $dados[ $row['idfluxocaixa'] ]['valor'] = number_format($row['valor'], 2, ',', '.');
            $dados[ $row['idfluxocaixa'] ]['parcela'] = ++$i.'/'.$nr_linhas;
        }

        //  reinicio os indices do array
        $dados = array_values($dados);

        echo json_encode($dados);
    }



    //  atualiza o pagamento
    function avisoPagamentoMensalidade($usuario, $idfluxocaixa){

        //  atualizo na tabela o aviso ao responsavel
        $this->db->set('pago', 1);
        $this->db->where('idfluxocaixa', $idfluxocaixa);
        $this->db->update('fluxo_caixa');
        echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

    }



    // // funcao cadastrar pelo app
    public function insert(){     
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $data['erros'] = $this->valida_form('insert');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->alunos_model->cadastra( $this->input->post(), 'insert' );
            $msg = array('mensage' => "Aluno inserido com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }


    //  atualiza dados
    public function update(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

         //  validando o formulario
        $data['erros'] = $this->valida_form('update');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->alunos_model->update( $this->input->post(), $_POST['idaluno'] ,'update' );
            $msg = array('mensage' => "Aluno alterado com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }


    //  validacao do formulario
    public function valida_form($action){

        //  seto o array para o post 
        //$this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if( $action == "update"){ //alterar
            $this->form_validation->set_rules('idaluno', 'código do aluno', 'required', array('required' => 'O campo %s é obrigatório.')); 
        }
       
        //  validacao do formulario
        $this->form_validation->set_rules('nome', 'nome', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('id_responsavel', 'responsável', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('id_escola', 'escola', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('id_usuario', 'código do usuário', 'required', array('required' => 'O campo %s é obrigatório.'));
        
        //$this->form_validation->set_rules('sala', 'sala', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('turno', 'turno', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        //$this->form_validation->set_rules('transporte_ida', 'transporte na ida', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        //$this->form_validation->set_rules('transporte_volta', 'transporte na volta', 'trim|required', array('required' => 'O campo %s é obrigatório.'));

       // $this->form_validation->set_rules('qtd_parcelas', 'quantidade de parcelas', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
       // $this->form_validation->set_rules('dia_pagamento', 'dia do pagamento', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        //$this->form_validation->set_rules('valor_parcela', 'valor da parcela', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
       
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }


    //  deleta o responsável
    public function delete($idusuario, $idaluno){
        $result = $this->alunos_model->delete($idusuario, $idaluno);
        $msg = array('mensage' => "Aluno excluído com sucesso.", "status" => TRUE);
        echo json_encode($msg);
    }



}