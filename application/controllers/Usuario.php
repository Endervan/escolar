<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('Usuario_model', 'banco', TRUE);
        $this->load->helper('url');
        $this->load->library('email');
    }


    //  busca o usuario pelo token
    public function getIdUsuario($token){

        //  busco os dados do usuario
        return $this->banco->getIdUsuario($token);

    }

    //  busca as configuracoes do usuario
    public function getConfiguracoesUsuario($id_usuario){
        $dadosUsuario =  $this->banco->getConfiguracoesUsuario( $id_usuario );
    }


    public function login(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $retornoLogin = $this->banco->validando_login($_POST['email'], $_POST['senha'], $_POST['onesignail_idplayer']);
        
        //  verifico se tem erro no form            
        if( $retornoLogin === FALSE ){  //  cadastra o onesignail_idplayer
            $msg = array('title'=>'Atenção', 'mensage' =>  'Email ou senha não encontrado.', "status" => FALSE );
            echo json_encode($msg);
        }elseif($retornoLogin->ativo == 'nao'){ //..verifico se a conta esta ativa
            $msg = array('title'=>'Atenção', 'mensage' =>  'Sua conta está suspensa, por favor entre em contato com o suporte.', "status" => FALSE );
            echo json_encode($msg);
        }else{
            $msg = array('title'=>'Aviso', 'mensage' => "Login efetuado com sucesso", "status" => TRUE, 'idusuario' => $retornoLogin->idusuario, 'token' => $retornoLogin->token, 'usuario_nome' => $retornoLogin->nome);
            echo json_encode($msg);
        }
    }


    public function recuperaSenha(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $retornoLogin = $this->banco->recupera_senha($_POST['email']);
        
        //  verifico se tem erro no form            
        if( $retornoLogin === FALSE ){  //  cadastra o onesignail_idplayer
            $msg = array('title'=>'Atenção', 'mensage' =>  'Email não encontrado.', "status" => FALSE );
            echo json_encode($msg);
        }else{
            $this->envia_senha_recuperada($retornoLogin, $_POST['email']);
            $msg = array('title'=>'Aviso', 'mensage' => "Uma nova senha foi enviado para seu email.", "status" => TRUE);
            echo json_encode($msg);
        }
    }


    function envia_senha_recuperada($password, $email){

        $usuario = $this->banco->get_dado_usuario($email);
        $message = '
                    <p>Olá '.$usuario['nome'].', </p>
                    <p>Estamos aqui pra te ajudar.</p>
                    <p>Esqueceu sua senha? Não esquenta :)</p>
                    <p>A gente cria uma pra você bem rapidinho.</p>
                    <p>Sua senha foi alterada como solicitado. </p>
                    <p>Sua Nova senha é '.$password.' </p>
                    ';


        //Load email library
        $this->load->library('email');
        $this->email->from('no-reply@cadeavan.com.br', 'Cadê a Van');
        $this->email->to($email);
        $this->email->subject('Sua senha foi alterada');
        $this->email->message($message);
        $this->email->send();
        // echo $this->email->print_debugger();
    }


    public function cadastro(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $retornoLogin = $this->banco->verificarEmail($_POST['email']);

        //  verifico se tem erro no form            
        if (count($retornoLogin) > 0) {  //  cadastra o onesignail_idplayer
            $msg = array('title'=>'Atenção', 'mensage' =>  'O email informado já está cadastrado.', "status" => FALSE );
            echo json_encode($msg);
        }else{
            $dados = $this->banco->insert($_POST);
            $msg = array('title'=>'Aviso', 'mensage' => "Seu cadastro foi efetuado com sucesso.", "status" => TRUE, 'idusuario' => $dados['idusuario'], 'token' => $dados['token']);
            echo json_encode($msg);
        }
    }


    public function updatePassword(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $return = $this->banco->updatePassword($_POST['senha_anterior'], $_POST['nova_senha'], $_POST['idusuario']);

        //  verifico se tem erro no form            
        if ($return == 0) {  //  cadastra o onesignail_idplayer
            $msg = array('title'=>'Atenção', 'mensage' =>  'A senha informada é diferente da anterior.', "status" => FALSE );
            echo json_encode($msg);
        }else{
            $msg = array('title'=>'Aviso', 'mensage' => "Senha atualizada com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }
    }
    

    public function updateDadosUsuario(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
         //  validando o formulario
         $retornoLogin = $this->banco->verificarEmail($_POST['email'], $_POST['idusuario']);
         
        //  verifico se tem erro no form            
        if ($retornoLogin > 0) {  //  cadastra o onesignail_idplayer
            $msg = array('title'=>'Atenção', 'mensage' =>  'O email informado já está cadastrado.', "status" => FALSE );
            echo json_encode($msg);
        }else{
            $this->banco->updateDadosUsuario($_POST);
            $msg = array('title'=>'Aviso', 'mensage' => "Dados atualizado com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }
    }


    


































    public function index()
    {

    }


    //funcao cadastrar usuario
    public function cadastrar_usuario_ionic()
    {
        $msg = "";
        $request = array();
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $nome = "";
        $email = "";
        $password = "";
        $telefone = "";
        if (!empty($request)) {
            foreach ($request as $key => $val) {
                if ($key == "nome") {
                    $nome = $val;
                } else
                    if ($key == "email") {
                        $email = $val;
                    } else
                        if ($key == "telefone") {
                            $telefone = $val;
                        } else
                            if ($key == "password") {
                                $password = password_hash($val, PASSWORD_DEFAULT);
                            } else {
                                $msg = "valor enviado invalido";
                            }
            }
            $dados = array(
                'nome' => $nome,
                'email' => $email,
                'telefone' => $telefone,
                'password' => $password,
            );

            $this->load->model('banco');
            //nao efetua cadstro caso email ja exista no banco
            if (!$this->banco->verificarEmail($dados['email'])) {

                //nao efetua cadstro caso telefone ja exista no banco
                if (!$this->banco->verificarTelefone($dados['telefone'])) {
                    $this->db->insert('usuarios', $dados);
                    $insert_id = $this->db->insert_id();
                    $msg = $insert_id . "|sucesso";

                } else {
                    $msg = "0|erro";//telefone existe
                }
            } else {
                $msg = "1|erro";//email existe
            }
        } else {
            $msg = "Erro ao enviar os dados";
        }
        echo $msg;
        //echo json_encode($request);
    }

    //-------------------------------------------------------
    public function login_ionic()
    {
        $msg = "";
        $request = array();
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $email = "";
        $password = "";
        if (!empty($request)) {
            foreach ($request as $key => $val) {
                if ($key == "email") {
                    $email = $val;
                } else
                    if ($key == "password") {
                        $password = $val;
                        $password2 = password_hash($val, PASSWORD_DEFAULT);

                    } else {
                        $msg = "valor enviado invalido";
                    }
            }
            $retornoLogin = $this->banco->validando_login($email);
            if ($retornoLogin) {
                foreach ($retornoLogin as $ret) {
                    $key = md5(uniqid(rand(), TRUE));
                    if (password_verify($password, $ret->password)) {
                        $data = array(
                            'tk' => $key
                        );
                        $user_id = $ret->id;
                        $this->db->where('id', $user_id);
                        $this->db->update('usuarios', $data);

                        $msg = $user_id . "|sucesso|1|" . $key . "|" . $password2;
                    } else {
                        $msg = "Email ou senha inválidos";
                    }
                }
            } else {
                $msg = "Erro ao enviar os dados";
            }
        }
        echo $msg;
    }


    /*
    public function Recupera_senha()
    {
        //$msg = "";
        $request = array();
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        //$email = "";
        if (!empty($request)) {
            foreach ($request as $key => $val) {
                // print_r($val);

                if ($key == "email") {
                    $email = $val;
                } else {
                    $msg = "valor enviado invalido";
                }
            }
            // $dados = array(
            //    'email' => $email,
            // );

            //$this->load->model('banco');
            if ($this->banco->recupera_senha($email)) {
                $msg = "0|sucesso";//email existe

            } else {
                return $msg = "1|";//email existe

            }
        } else {
            $msg = "Erro ao enviar os dados";
        }
        echo $msg;
        //echo json_encode($request);
    }
    */


      //buscando usuario com id e token
      public function get_usuario_id($usuario_id){
        $usuarios = $this->banco->get_usuario_id2($usuario_id);
        echo json_encode($usuarios);
      }


    //buscando usuario com id e token
    public function get_usuario($usuario_id, $token)
    {

        // Verificando a permissão com token
        $this->db->where('tk', $token);
        $query = $this->db->get('usuarios');

        if ($query->num_rows() > 0) {
            $usuarios = $this->banco->get_usuario_id($usuario_id);
            $response = array();
            foreach ($usuarios as $res) {
                $usuario = array();
                $usuario["id"] = $res->id;
                $usuario["nome"] = $res->nome;
                $usuario["email"] = $res->email;
                $usuario["telefone"] = $res->telefone;
                array_push($response, $usuario);
            }
            echo json_encode($response);
        } else {
            echo "token inválido";
        }
    }

    //funcao alterar usuario

    public function alterar()
    {
        //  carrego os helpers
        //$this->load->helper('form');
        // $this->load->library('form_validation');

        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter


        //  campos para validar
        //$this->form_validation->set_rules('nome', '', 'required');
        //$this->form_validation->set_rules('email', '', 'required|differs[email_antigo]');
       // $this->form_validation->set_rules('telefone', '', 'required|differs[telefone_antigo]');


        //  verifica se houve erro

        /*
        if ($this->form_validation->run() == FALSE) {
            echo 'FALSE';
            echo validation_errors();
        } else {
            echo "TRUE CADASTRO";
            $this->Usuario_model->update();
        }

        */

        $this->Usuario_model->UPTADE();
    }

}


