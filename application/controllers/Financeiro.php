<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('financeiro_model', 'm_financeiro');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }


    //  deletar o registro
    public function delete($idfluxocaixa){
        $this->db->where('idfluxocaixa', $idfluxocaixa);
        $this->db->delete('fluxo_caixa');
        // echo $this->db->last_query();   //  exibe o sql executado
    }



    // funcao cadastrar pelo app
    public function insert(){     
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $data['erros'] = $this->valida_form('insert');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->m_financeiro->cadastra( $this->input->post(), 'insert' );
            $msg = array('mensage' => "Registro inserido com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }

    

    //  validacao do formulario
    public function valida_form($action){

        //  seto o array para o post 
        //$this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if( $action == "update"){ //alterar
            $this->form_validation->set_rules('idfluxocaixa', 'código do caixa', 'required', array('required' => 'O campo %s é obrigatório.')); 
        }
       
        //  validacao do formulario
        $this->form_validation->set_rules('titulo', 'titulo', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('data', 'vencimento', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('valor', 'valor', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        // $this->form_validation->set_rules('pago', 'pago', 'required', array('required' => 'O campo %s é obrigatório.'));
        
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }


    //  atualiza o financeiro
    function update(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

         //  validando o formulario
         $data['erros'] = $this->valida_form('update');
        
         //  verifico se tem erro no form            
         if( $data['erros'] === TRUE ){  //  cadastra
             $this->m_financeiro->update( $this->input->post(), $_POST['idfluxocaixa'] ,'update' );
             $msg = array('mensage' => "Financeiro alterado com sucesso.", "status" => TRUE);
             echo json_encode($msg);
         }else{
             $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
             echo json_encode($msg);
         }

    }




}