<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Responsavel extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('responsavel_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }

    
    public function login($onesignail_idplayer = ""){

        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
        
        //  validando o formulario
        $retornoLogin = $this->responsavel_model->validando_login($_POST['email'], $_POST['senha'], $onesignail_idplayer);
        
        //  verifico se tem erro no form            
        if( $retornoLogin === FALSE ){  //  cadastra o onesignail_idplayer
            $msg = array('title'=>'Atenção', 'mensage' =>  'Email ou senha não encontrado.', "status" => FALSE );
            echo json_encode($msg);
        }else{
            $msg = array('title'=>'Aviso', 'mensage' => "Login efetuado com sucesso", "status" => TRUE, 'token' => $retornoLogin->token);
            echo json_encode($msg);
        }
    }





    //  retorna todos os responsaveis do usuario
    public function getResponsaveis($idusuario, $start_row = 0){
        $query = $this->responsavel_model->get_responsaveis($idusuario, $start_row);
        $result = $query->result_array();
        echo json_encode($result);
    }


    //  retorna todos os responsaveis do usuario
    public function getResponsavel($idusuario, $idresponsavel){
        $query = $this->responsavel_model->get_responsaveis($idusuario, $idresponsavel);
        $result = $query->result_array();
        echo json_encode($result);
    }




    //  deleta o responsável
    public function delete($idusuario, $idresponsavel){
        $result = $this->responsavel_model->delete($idusuario, $idresponsavel);
        
        $msg = array('mensage' => "Registro excluído com sucesso.", "status" => TRUE);
        echo json_encode($msg);
    }



    //  busca o id do responsavel pelo token
    public function getIdResponsavelToken($token){

        $this->db->where('token', $token);
        $query = $this->db->get('logins_responsaveis');
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        return $query->row_array();
    }




    //  busca todos os usuarios que tem o responsavel cadastro
    public function getTodosUsuariosResponsavel($cpf){
        
        $ids = "";
        $this->db->where('cpf', $cpf);
        $query = $this->db->get('responsaveis');
        $result = $query->result_array();
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        
        if(count($result) > 0){
            
            foreach($result as $dado){
                $ids[] = $dado['idresponsavel'];
            }
        }

        //  voltos os ids dos usuarios com esse responsavel
        return $ids;

    }


    //  atualiza as notificacoes para lidas
    public function atualizaStatusNotificacoes($token){
        //  busco os dados do login
        $dados_responsavel = $this->getIdResponsavelToken($token);

        //  busco todos os usuarios que tem esse responsavel cadastrado
        $usuarios_responsaveis = $this->getTodosUsuariosResponsavel($dados_responsavel['cpf']);
        //print_r($usuarios_responsaveis);

        //  atualizo na tabela 
        $this->db->set('notificacao_lida', 1);
        $this->db->where_in('id_responsavel', $usuarios_responsaveis);
        $this->db->update('notificacoes_responsaveis');
        echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

    }


    //  busca as notificacoes do responsavel
    public function getNotificacoesResponsavel($token, $notificacao_lida = "", $data = ""){

        //  verifico se foi passado somente as lidas
        if(empty($data)){
            $data = date("Y-m-d");
        }


        //  busco os dados do login
        $dados_responsavel = $this->getIdResponsavelToken($token);

        //  busco todos os usuarios que tem esse responsavel cadastrado
        $usuarios_responsaveis = $this->getTodosUsuariosResponsavel($dados_responsavel['cpf']);
        //print_r($usuarios_responsaveis);

        
        //  verifico as notificacoes nao lidas, caso não passe pega todas
        if(empty($notificacao_lida)){
            $this->db->where('notificacoes_responsaveis.notificacao_lida', 0);
        }
        $this->db->from('notificacoes_responsaveis');
        $this->db->join('usuarios', 'notificacoes_responsaveis.id_usuario = usuarios.idusuario', 'left');
        $this->db->where_in('id_responsavel', $usuarios_responsaveis);
        $this->db->where('data', $data);
        $this->db->order_by('notificacoes_responsaveis.data', 'desc');
        $query = $this->db->get();
        
        
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        $result = $query->result_array();
        echo json_encode($result);
    }



    //  busca os alunos do responsavel
    public function getAlunosResponsavel($token){

        $result = "";

         //  busco os dados do login
         $dados_responsavel = $this->getIdResponsavelToken($token);


         if (count($dados_responsavel) > 0){    //  verifico se tem resultado
            //  busco todos os usuarios que tem esse responsavel cadastrado
            $usuarios_responsaveis = $this->getTodosUsuariosResponsavel($dados_responsavel['cpf']);
            //print_r($usuarios_responsaveis);


            //  busco todos os alunos que o responsavel tem
            $this->db->select("alunos.*, responsaveis.nome as responsavel_nome, responsaveis.cpf as responsavel_cpf, usuarios.nome as usuario_nome, usuarios.telefone as usuario_telefone");
            $this->db->from('alunos');
            $this->db->where_in('id_responsavel', $usuarios_responsaveis);
            $this->db->join('responsaveis', 'responsaveis.idresponsavel = alunos.id_responsavel');
            $this->db->join('usuarios', 'usuarios.idusuario = responsaveis.id_usuario');
            $this->db->order_by('alunos.nome', 'asc');
            
            $query = $this->db->get();
            //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
            $result = $query->result_array();
            
         }

         echo json_encode($result);

    }








    //  busca o financeiro do responsavel
    public function getFinanceiroResponsavel($token){
        $dados = [];
        
        //  busco os dados do login
        $dados_responsavel = $this->getIdResponsavelToken($token);

        //  busco todos os usuarios que tem esse responsavel cadastrado
        $usuarios_responsaveis = $this->getTodosUsuariosResponsavel($dados_responsavel['cpf']);
        //  print_r($usuarios_responsaveis);


        if (count($dados_responsavel) > 0){    //  verifico se tem resultado
        //  busco todos os alunos que o responsavel tem
        $this->db->select("fluxo_caixa.*");
        $this->db->from('fluxo_caixa');
        $this->db->join('alunos', 'alunos.idaluno = fluxo_caixa.id_aluno', 'left');
        $this->db->join('responsaveis', 'responsaveis.idresponsavel = alunos.id_responsavel', 'left');
        $this->db->where_in('responsaveis.idresponsavel', $usuarios_responsaveis);
        $this->db->order_by('fluxo_caixa.data', 'asc');
       
        $query = $this->db->get();
        //  echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        $nr_linhas = $result = $query->num_rows();

        //  faco um loop para organizar o array
        $i =0;
        foreach ($query->result_array() as $row){
            $dados[ $row['idfluxocaixa'] ] = $row;
            $dados[ $row['idfluxocaixa'] ]['data'] = date("d/m/Y", strtotime($row['data']));
            $dados[ $row['idfluxocaixa'] ]['valor'] = number_format($row['valor'], 2, ',', '.');
        }

        //  reinicio os indices do array
        $dados = array_values($dados);

        }

        echo json_encode($dados);


       

        

    }



    //  busca os alunos do responsavel
    public function getMotoristasResponsavel($token){

        $result = "";

        //  busco os dados do login
        $dados_responsavel = $this->getIdResponsavelToken($token);

        //  busco todos os usuarios que tem esse responsavel cadastrado
        $usuarios_responsaveis = $this->getTodosUsuariosResponsavel($dados_responsavel['cpf']);
        //print_r($usuarios_responsaveis);

        if (count($dados_responsavel) > 0){    //  verifico se tem resultado
          //  busco todos os alunos que o responsavel tem
          $this->db->select("usuarios.*");
          $this->db->from('usuarios');
          $this->db->where_in('responsaveis.idresponsavel', $usuarios_responsaveis);
          $this->db->join('responsaveis', 'responsaveis.id_usuario = usuarios.idusuario', 'left');
          $this->db->order_by('usuarios.nome', 'asc');
        
          $query = $this->db->get();
          //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
          $result = $query->result_array();
        }

        echo json_encode($result);


   }





    /*
    //  busca os alunos e a rotas ativas
    public function getRotasOnline($token){
        
        $data = date("Y-m-d");

        //  busco os dados do login
        $dados_responsavel = $this->getIdResponsavelToken($token);
ESTOU TENTANDO BUSCAR OS ALUNOS QUE ESTAO NA ROTA ONLINE

        //  busco todos os alunos que o responsavel tem
        $this->db->where('rotas.online', 1);    //  somente rotas online
        $this->db->where('rotas_paradas_diarias.data', $data);    //  somente do dia
        $this->db->where('responsaveis.cpf', $dados_responsavel['cpf']);
        $this->db->from('responsaveis');
        $this->db->join('usuarios', 'usuarios.idusuario = responsaveis.id_usuario');
        $this->db->join('rotas', 'rotas.id_usuario = usuarios.idusuario');
        $this->db->join('rotas_paradas', 'rotas_paradas.id_rota = rotas.idrota');
        $this->db->join('rotas_paradas_diarias', 'rotas_paradas_diarias.id_rotaparada = rotas_paradas.idrotaparada');
        
        $query = $this->db->get();
       echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        
        exit;
        print_r($dados);

        echo $dados['cpf'];



    }
    */



    

    // funcao cadastrar pelo app
    // public function insert(){     
        
    //     //  resgato os dados do post
    //     $json = file_get_contents('php://input');
    //     $dados = json_decode($json, true);  //  deve se manter

    //     //  efetua o cadastro
    //     $this->responsavel_model->cadastra( $dados );
    //     $msg = array('mensage' => "Dados inserido com sucesso.", "status" => TRUE);
    //     echo json_encode($msg);


    // }



    public function recuperaSenha(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $retornoLogin = $this->responsavel_model->recupera_senha($_POST['email']);
        
        //  verifico se tem erro no form            
        if( $retornoLogin === FALSE ){  //  cadastra o onesignail_idplayer
            $msg = array('title'=>'Atenção', 'mensage' =>  'Email não encontrado.', "status" => FALSE );
            echo json_encode($msg);
        }else{
            $msg = array('title'=>'Aviso', 'mensage' => "Uma nova senha foi enviado para seu email.", "status" => TRUE);
            echo json_encode($msg);
        }
    }




   



    //  atualiza dados
    public function update(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

           //  validando o formulario
        $data['erros'] = $this->valida_form('update');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->responsavel_model->update( $this->input->post(), $_POST['idresponsavel'] ,'update' );
            $msg = array('mensage' => "Registro alterado com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }



    // // funcao cadastrar pelo app
    public function insert(){     
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
        
       
        //  validando o formulario
        $data['erros'] = $this->valida_form('insert');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->responsavel_model->cadastra( $this->input->post(), 'insert' );
            $msg = array('mensage' => "Dados inserido com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }

    }




    // // funcao cadastrar pelo app
    public function insert_login_responsavel(){     
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
        
       
        //  validando o formulario
        $data['erros'] = $this->valida_form_login_responsavel();
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $dados = $this->responsavel_model->cadastra_login_responsavel( $this->input->post() );
            $msg = array('mensage' => "Dados inserido com sucesso.", "status" => TRUE, 'token' => $dados['token']);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }

    }







    //  validacao do formulario
    public function valida_form($action){

        //  seto o array para o post 
        //$this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if( $action == "update"){ //alterar
            $this->form_validation->set_rules('idresponsavel', 'código do responsável', 'required', array('required' => 'O campo %s é obrigatório.')); 
        }
       
        //  validacao do formulario
        $this->form_validation->set_rules('id_usuario', 'código do usuário', 'required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('responsavel_nome', 'nome', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('responsavel_cpf', 'CPF', 'trim|callback_verifica_cpf_cadastro', array('required' => 'O campo %s é obrigatório.'));
               

        //$this->form_validation->set_rules('responsavel_email', 'Email', 'trim|callback_verifica_email_cadastro', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('responsavel_telefone_celular', 'trim|celular', 'required', array('required' => 'O campo %s é obrigatório.'));
        //$this->form_validation->set_rules('responsavel_endereco', 'endereço', 'trim|required', array('required' => 'O campo %s é obrigatório.'));

       
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }




    //  validacao do formulario
    public function valida_form_login_responsavel(){

       
        //  validacao do formulario
        $this->form_validation->set_rules('nome', 'nome', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('cpf', 'CPF', 'trim|callback_verifica_cpf_cadastro_login_usuario', array('required' => 'O campo %s é obrigatório.'));       

        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[logins_responsaveis.email]', array('required' => 'O campo %s é obrigatório.', 'is_unique' => 'Esse email já está em uso.'));
        $this->form_validation->set_rules('senha', 'senha', 'trim|required', array('required' => 'O campo %s é obrigatório.'));

       
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }



    //  valida o cpf do usuário para nao efetuar cadastro duplicado, permitindo o cadastro de vários cpf para mais de um usuario
    public function verifica_cpf_cadastro_login_usuario($cpf){

        //  id do responsavel
        $idresponsavel = $this->input->post('idresponsavel');
        $idusuario =  $this->input->post('id_usuario');


        // verifico se o cpf e valido
        if(!$this->validaCPF($cpf) ){
            $this->form_validation->set_message('verifica_cpf_cadastro_login_usuario', 'Este {field} é inválido.');
            return FALSE;
        }

        
        
        $rows = $this->responsavel_model->verifica_cpf_cadastro_logins_responsaveis($cpf);

        if ($rows > 0){
            $this->form_validation->set_message('verifica_cpf_cadastro_login_usuario', 'Este {field} já está cadastrado.');
            return FALSE;
        }else{
            return TRUE;
        }
     
    }



     //  valida o cpf do usuário para nao efetuar cadastro duplicado, permitindo o cadastro de vários cpf para mais de um usuario
     public function verifica_cpf_cadastro($cpf){

        //  id do responsavel
        $idresponsavel = $this->input->post('idresponsavel');
        $idusuario =  $this->input->post('id_usuario');


        // verifico se o cpf e valido
        if(!$this->validaCPF($cpf) ){
            $this->form_validation->set_message('verifica_cpf_cadastro', 'Este {field} é inválido.');
            return FALSE;
        }

        
        
        $rows = $this->responsavel_model->verifica_cpf_cadastro($idusuario, $cpf, $idresponsavel);

        if ($rows > 0){
            $this->form_validation->set_message('verifica_cpf_cadastro', 'Este {field} já está cadastrado.');
            return FALSE;
        }else{
            return TRUE;
        }
     
    }



    function validaCPF($cpf) {
 
        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
         
        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }




    


    //  valida o cpf do usuário para nao efetuar cadastro duplicado, permitindo o cadastro de vários cpf para mais de um usuario
    public function verifica_email_cadastro($email){
      
        $idusuario = $this->input->post('id_usuario');
        $idresponsavel = $this->input->post('idresponsavel');

        $rows = $this->responsavel_model->verifica_email_cadastro($idusuario, $email, $idresponsavel);
        
        if ($rows > 0){
                $this->form_validation->set_message('verifica_email_cadastro', 'Este {field} já está cadastrado.');
                return FALSE;
        }else{
                return TRUE;
        }
     
    }





    #   ========================================================================    #
    #   SOMENTE PARA TESTE DE CRUD - TESTE DE VIEWS
    #   ========================================================================    #


    
    //  pagina inicial   AUTOR: MARCIO ANDRE
    public function index($id_usuario = 1)
    {

        $data['title'] = 'Responsáveis';
        $data['query'] = $this->responsavel_model->get_responsaveis($id_usuario);
        
        $this->load->view("includes/head");
        $this->load->view("responsavel/index", $data);
        $this->load->view("includes/rodape");
    }



    public function pageInsert($idusuario){

    
        $data['title'] = 'Cadastro de Responsável';
        $data['idusuario'] = $idusuario;    //  passo o id do usuario


        //  validando o formulario
        $data['erros'] = $this->valida_form();

        //  carregando o formulario
        $this->load->view("includes/head");
        $this->load->view("responsavel/insert", $data);
        $this->load->view("includes/rodape");
        

        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->responsavel_model->cadastra($this->input->post());
        }
         
        
        

            
        


        


        /*
        $data['title'] = 'Cadastro de Responsável';

        if ($this->form_validation->run() === FALSE)
        {
            echo 'NOME = '. validation_errors() ;
            $this->load->view("includes/head");
            $this->load->view("responsavel/insert", $data);
            $this->load->view("includes/rodape");

        }
        else
        {
            $this->responsavel_model->cadastra($_POST);
            $this->load->view("includes/head");
            $this->load->view('responsavel/success');
            $this->load->view("includes/rodape");
        }
        */

    }



    public function pageupdate($idusuario, $idresponsavel)
    {
            $query = $this->responsavel_model->get_responsaveis($idusuario, 0, $idresponsavel);
            $data = $query->result_array();
            //$data = $query->row();

            
            
            $data['title'] = 'Alteração de Responsável';

            $this->load->view("includes/head");
            $this->load->view('responsavel/update', $data);
            $this->load->view("includes/rodape");
    }



    


    #   ========================================================================    #
    #   SOMENTE PARA TESTE DE CRUD - TESTE DE VIEWS
    #   ========================================================================    #


}