<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajuda extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('ajuda_model', 'm_ajuda');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }


    // funcao para retornar as categorias
    public function getCategorias($tipo_ajuda){
        $result = $this->m_ajuda->getCategorias($tipo_ajuda);
        echo json_encode($result);
    }


    // funcao para retornar dados
    public function getPerguntas($id_categoriaajuda){
        $result = $this->m_ajuda->getPerguntas($id_categoriaajuda);
        echo json_encode($result);
    }




}