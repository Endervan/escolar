<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Veiculo extends CI_Controller{


    function __construct(){
        parent::__construct();
        //$this->load->model('Veiculo_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
        $this->load->model('Veiculo_Model', 'banco', TRUE);
    }


    // funcao para retornar dados
    public function getDados($id_usuario){
        $this->db->where('id_usuario', $id_usuario);
        $query = $this->db->get('veiculos');
        $result = $query->result_array();
        echo json_encode($result);
    }


     // // funcao cadastrar pelo app
     public function insert(){     
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
       
        //  validando o formulario
        $data['erros'] = $this->valida_form('insert');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->banco->cadastra( $this->input->post(), 'insert' );
            $msg = array('mensage' => "Veículo inserido com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }


    //  atualiza dados
    public function update(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

           //  validando o formulario
        $data['erros'] = $this->valida_form('update');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->banco->update( $this->input->post(), $_POST['idveiculo'] ,'update' );
            $msg = array('mensage' => "Veículo alterado com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }



    //  deleta o responsável
    public function delete($idusuario, $idveiculo){
        $result = $this->banco->delete($idusuario, $idveiculo);
        $msg = array('mensage' => "Registro excluído com sucesso.", "status" => TRUE);
        echo json_encode($msg);
    }




    //  validacao do formulario
    public function valida_form($action){

        //  seto o array para o post 
        //$this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if( $action == "update"){ //alterar
            $this->form_validation->set_rules('idveiculo', 'código do veículo', 'required', array('required' => 'O campo %s é obrigatório.')); 
        }
       
        //  validacao do formulario
        $this->form_validation->set_rules('placa', 'placa', 'trim|callback_verifica_placa', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('marca', 'marca', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('modelo', 'modelo', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('ano', 'ano', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('cor', 'cor', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
       
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }


    //  valida o cpf do usuário para nao efetuar cadastro duplicado, permitindo o cadastro de vários cpf para mais de um usuario
    public function verifica_placa($placa){

        //  id do responsavel
        $idveiculo = $this->input->post('idveiculo');
        $idusuario =  $this->input->post('id_usuario');
        
        $rows = $this->banco->verifica_placa($idusuario, $placa, $idveiculo);

        if ($rows > 0){
            $this->form_validation->set_message('verifica_placa', 'Essa {field} já está cadastrada.');
            return FALSE;
        }else{
                return TRUE;
        }
     
    }




    



}