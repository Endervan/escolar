<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlunoRota extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('alunoRota_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }



    // funcao para retornar dados
    public function getDados($id_usuario, $id_aluno){
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_aluno', $id_aluno);
        $query = $this->db->get('alunos_rotas');
        $result = $query->result_array();
        echo json_encode($result);
    }



    // funcao para retornar dados
    public function getnumRows($id_usuario, $id_aluno){
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_aluno', $id_aluno);
        $query = $this->db->get('alunos_rotas');
        $result = $query->result_array();
        $result = $query->result_array();
        echo json_encode($result);
    }




    function delete($idusuario, $ididenderecoaluno){
        
        $this->banco->delete($idusuario, $ididenderecoaluno);
        $msg = array('mensage' => "Endereço excluíd com sucesso.", "status" => TRUE);
        echo json_encode($msg);
    
    }






    // funcao cadastrar pelo app
    public function update(){     
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter


        //  verifica os dias marcados
        $_POST = $this->verifica_dias_rota($_POST);
;
        //  validando o formulario
        $data['erros'] = $this->valida_form('update');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->alunoRota_model->update( $this->input->post(), $_POST['id_aluno'], $_POST['id_usuario']);
            $msg = array('mensage' => "Rota atualizada com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }



    public function verifica_dias_rota($dados){

        //  verifico se foi marcado como ida todos os dias iguais
        if($dados['mesmoEnderecoIda'] == 1){            
            $dados['idIdaVeiculoTerca'] = $dados['idIdaVeiculoSegunda'];
            $dados['idIdaEnderecoTerca'] = $dados['idIdaEnderecoSegunda'];
            $dados['idIdaVeiculoQuarta'] = $dados['idIdaVeiculoSegunda'];
            $dados['idIdaEnderecoQuarta'] = $dados['idIdaEnderecoSegunda'];
            $dados['idIdaVeiculoQuinta'] = $dados['idIdaVeiculoSegunda'];
            $dados['idIdaEnderecoQuinta'] = $dados['idIdaEnderecoSegunda'];
            $dados['idIdaVeiculoSexta'] = $dados['idIdaVeiculoSegunda'];
            $dados['idIdaEnderecoSexta'] = $dados['idIdaEnderecoSegunda'];           
        }

        //  verifico se foi marcado como volta todos os dias iguais
        if($dados['mesmoEnderecoVolta'] == 1){            
            $dados['idVoltaVeiculoTerca'] = $dados['idVoltaVeiculoSegunda'];
            $dados['idVoltaEnderecoTerca'] = $dados['idVoltaEnderecoSegunda'];
            $dados['idVoltaVeiculoQuarta'] = $dados['idVoltaVeiculoSegunda'];
            $dados['idVoltaEnderecoQuarta'] = $dados['idVoltaEnderecoSegunda'];
            $dados['idVoltaVeiculoQuinta'] = $dados['idVoltaVeiculoSegunda'];
            $dados['idVoltaEnderecoQuinta'] = $dados['idVoltaEnderecoSegunda'];
            $dados['idVoltaVeiculoSexta'] = $dados['idVoltaVeiculoSegunda'];
            $dados['idVoltaEnderecoSexta'] = $dados['idVoltaEnderecoSegunda'];           
        }

        return $dados;

    }



    //  validacao do formulario
    public function valida_form($action){

        //  seto o array para o post 
        //$this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if( $action == "update"){ //alterar
            $this->form_validation->set_rules('id_aluno', 'código do aluno', 'required', array('required' => 'O campo %s é obrigatório.')); 
        }
       
        $this->form_validation->set_rules('id_usuario', 'código do usuário', 'required', array('required' => 'O campo %s é obrigatório.')); 


        //  validacao do formulario 
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }

















}