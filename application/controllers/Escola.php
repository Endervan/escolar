<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Escola extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('escola_model','banco');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }


    //  envia uma mensagem ao reponsavel
    function avisoEmbarqueDesembarEscola($id_usuario, $id_escola, $id_aluno, $id_responsavel, $idrotaparadadiaria){

        
        //  busco os dados do responsavel
        $this->db->select('responsaveis.*, logins_responsaveis.*, escolas.nome as escola_nome');
        $this->db->from('responsaveis');
        $this->db->where('responsaveis.idresponsavel', $id_responsavel);
        $this->db->join('logins_responsaveis', 'responsaveis.cpf = logins_responsaveis.cpf', 'left');
        $this->db->join('escolas', "escolas.idescola = $id_escola", 'left');
        $query = $this->db->get();
        $dados = $query->row();
      

        //  armazeno que o aluno embarcou/desembarcou na escola
        $this->db->set('aluno_embarcou_desembarcou_escola', 1);
        $this->db->where('idrotaparadadiaria', $idrotaparadadiaria);
        $this->db->where('id_usuario', $id_usuario);
        $this->db->update('rotas_paradas_diarias');
        
        echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

       
        //  envio a msg
        $titulo = "Chegada ao destino";
        $mensagem = "$dados->nome Já chegamos à $dados->escola_nome.";


        //  verifico se tem o playerId e envia uma msg para o responsavel avisando do embarque do aluno
        if(!empty( $dados->onesignail_idplayer )){
            onesignal_send_msg($titulo, $mensagem, $dados->onesignail_idplayer);
        }

    }


    // funcao para retornar dados
    public function getDados($id_usuario)
    {
        $this->db->where('id_usuario', $id_usuario);
        $this->db->order_by('nome ASC');
        $query = $this->db->get('escolas');
        $result = $query->result_array();
        echo json_encode($result);
    }


    
    // funcao para retornar os alunos da escola
    public function getAlunosEscola($id_usuario, $idescola, $turno = NULL)
    {
        //  verifico se filtro por turno
        if($turno != NULL){
            $this->db->where('turno', $turno);
        }

        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_escola', $idescola);
        $this->db->order_by('sala', 'asc');
        $this->db->order_by('nome', 'asc');
        $query = $this->db->get('alunos');
        $result = $query->result_array();
        //echo $this->db->last_query();   //  exibe o sql executado
        echo json_encode($result);
    }


    // funcao cadastrar
    public function insert(){     
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $dados = json_decode($json, true);  //  deve se manter

        //  validando o formulario
        $data['erros'] = $this->valida_form($dados);

        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->banco->cadastra( $dados );
            $msg = array('mensage' => "Dados inserido com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }

    }

     // //  Atualizar  os dados
     public function update(){
      
         //RECUPERAÇÃO DO FORMULÁRIO
         $json = file_get_contents("php://input");
         $dados = json_decode($json, true);  //  deve se manter
 
         //  validando o formulario
         $data['erros'] = $this->valida_form($dados, 'sim');
 
         //  verifico se tem erro no form            
         if( $data['erros'] === TRUE ){  //  cadastra
             $this->banco->update( $dados, $dados['idescola'] );
             $msg = array('mensage' => "Dados Aterados com sucesso.", "status" => TRUE);
             echo json_encode($msg);
         }else{
             $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
             echo json_encode($msg);
         }
 
     }



    //  validacao do formulario
    public function valida_form($dados, $update = 'nao'){
        
        //  seto o array para o post 
        $this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if($update != "nao"){
            $this->form_validation->set_rules('idescola', 'código da escola', 'required', array('required' => 'O campo %s é obrigatório.'));    
        }

        //  validacao do formulario
        $this->form_validation->set_rules('id_usuario', 'código do usuário', 'required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('nome', 'nome', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        //$this->form_validation->set_rules('endereco', 'endereço', 'trim|required', array('required' => 'O campo %s é obrigatório.'));

       
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }


    }


    

    function delete($idusuario, $idescola){
        
        $result = $this->banco->delete($idusuario, $idescola);

        if( $result == false ){
            $msg = array('mensage' => "Escola que contem aluno não pode ser excluida. Remova os alunos primeiro.", "status" => FALSE);
            echo json_encode($msg);
        }else{
            $this->banco->delete($idusuario, $idescola);
            $msg = array('mensage' => "Escola excluída com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }
    }





    
   


    // //  validacao do formulario
    // public function valida_form($update = FALSE){

    //     //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
    //     // if($update === TRUE){
    //     //     $this->form_validation->set_rules('escola_id', 'código da escola', 'required', array('required' => 'O campo %s é obrigatório.'));    
    //     // }

    //     //  validacao do formulario
    //     $this->form_validation->set_rules('usuario_id', 'código do usuário', 'required', array('required' => 'O campo %s é obrigatório.'));
    //     $this->form_validation->set_rules('nome', 'nome', 'required', array('required' => 'O campo %s é obrigatório.'));

       
    //     if ($this->form_validation->run() === FALSE)
    //     {
    //        return validation_errors();
    //     }
    //     else
    //     {
    //         return TRUE;
    //     }
    // }




    
    // public function cadastra()
    // {


    //     //  resgato os dados do post
    //     $json = file_get_contents('php://input');
    //     $dados = json_decode($json, true);  //  deve se manter

   
    //     //  cadastro os dados
    //     if ($this->banco->cadastra($dados)) {
    //         $msg = array('mensage' => "Dados inserido com sucesso.");
    //         echo json_encode($msg);
    //     } else {
    //         $msg = array('mensage' => "Houve um erro, por favor tente novamente. ");
    //         echo json_encode($msg);
    //     }


    // }

    

//    //  Deletar   dados
//     public function deletar()
//     {

//         //RECUPERAÇÃO DO FORMULÁRIO
//         $json = file_get_contents("php://input");
//         $dados = json_decode($json, true);  //  deve se manter


//          //  delete os dados
//         if ($this->banco->deleta($dados['id'])) {
//             $msg = array('mensage' => "Dados Removidos com sucesso.");
//             echo json_encode($msg);
//         } else {
//             $msg = array('mensage' => "Houve um erro, por favor tente novamente. ");
//             echo json_encode($msg);
//         }




//     }

}