<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends CI_Controller
{


    function __construct(){
        parent::__construct();
        $this->load->model('Usuario_model','model_usuario');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }


    //  numero do whastapp para suporte
    function getNumeroWhatsapp(){
        // $numero = "11984210261";
        $numero = "61994459306";
        echo json_encode($numero);
    }

    //  busca as configuracoes do usuario
    function getConfiguracoesUsuario($token){

        //  busca os dados do usuario
        $dadosUsuario = $this->model_usuario->getIdUsuario($token);
        
        //  busco as configuracoes
        $configuracoes = $this->model_usuario->getConfiguracoesUsuario($dadosUsuario['idusuario'] );

        echo json_encode($configuracoes);
    }


    function armazenaErro($erro, $pagina){

        //  aramazeno no bancos
        $dados['erro'] = urldecode($erro);
        $dados['pagina'] = $pagina;
        $this->db->insert('erros', $dados);
        
        /*
        //  envio um email
        $message = '
                    <p>Olá. </p>
                    <p>O sistema apresentou um erro, segue abaixo o log para mais detalhes.</p>
                    <p>Página: '.$pagina.' </p>
                    <br />
                    <p>Erro</p>
                    <p>'.$erro.' </p>
                    ';


        //Load email library
        $this->load->library('email');
        $this->email->from('no-reply@cadeavan.com.br', 'Cadê a Van');
        $this->email->to('suporte@cadeavan.com.br');
        $this->email->subject('Cadê a Van - Erro no sistema');
        $this->email->message($message);
        $this->email->send();
        */
    }





}