<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rota extends CI_Controller
{

 
    function __construct()
    {
        parent::__construct();
        $this->load->model('rota_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
        $this->load->model('alunos_model');    //  carrego o model devendo ser chamado pelo nome entre parentes
    }



    

 
    // funcao para atualizar a ordem
    function atualizaOrdem(){

        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

        $this->rota_model->atualizaOrdem($_POST['from'], $_POST['to'], $_POST['element'], $_POST['tipo_rota']);
        $msg = array('mensage' =>  "Ordem atualizada com sucesso.", "status" => TRUE );
        echo json_encode($msg);
    }






    // funcao para retornar dados
    public function getRotas($id_usuario){
        // $this->db->select("rotas.idrota, rotas.hora_inicio, veiculos.idveiculo, veiculos.marca, veiculos.modelo, veiculos.placa");
        $this->db->from('rotas');
        $this->db->join('veiculos', 'veiculos.idveiculo = rotas.id_veiculo');
        $this->db->where('rotas.id_usuario', $id_usuario);
        $this->db->order_by('rotas.titulo', 'asc');
        $query = $this->db->get();
        $result = $query->result_array();
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        echo json_encode($result);
    }


    //  busca a rota
    public function getRota($idrota){
        // $this->db->where_in('idrota', $idrota);
        // $query = $this->db->get('rotas');
        // $result = $query->result_array();
        $result = $this->rota_model->getRota($idrota);
        echo json_encode($result);
    }



    // // funcao cadastrar pelo app
    public function insert(){
        
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter

       
        //  validando o formulario
        $data['erros'] = $this->valida_form('insert');
        
        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $idrota = $this->rota_model->cadastra( $this->input->post(), 'insert' );    //  cadastro a rota
            $dadosRota = $this->rota_model->getRota($idrota);
            $msg = array('mensage' => "Rota inserida com sucesso.", "status" => TRUE, "idrota" => $idrota, "dadosRota" => $dadosRota);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }
    }



    // //  Atualizar  os dados
    public function update(){
      
        //RECUPERAÇÃO DO FORMULÁRIO
        $json = file_get_contents("php://input");
        $_POST = json_decode($json, true);  //  deve se manter

        //  validando o formulario
        $data['erros'] = $this->valida_form('update');

        //  verifico se tem erro no form            
        if( $data['erros'] === TRUE ){  //  cadastra
            $this->rota_model->update( $this->input->post(), $_POST['idrota'] );
            $msg = array('mensage' => "Rota Aterada com sucesso.", "status" => TRUE);
            echo json_encode($msg);
        }else{
            $msg = array('mensage' =>  $data['erros'], "status" => FALSE );
            echo json_encode($msg);
        }

    }


    //  validacao do formulario
    public function valida_form($action){

        //  seto o array para o post 
        //$this->form_validation->set_data($dados);

        //  verifico se e para atualizar os dados, se for true esse valor e a chave primaria da tabela utilizada para atulizar na clausula WHERE
        if( $action == "update"){ //alterar
            $this->form_validation->set_rules('idrota', 'código da rota', 'required', array('required' => 'O campo %s é obrigatório.')); 
        }
       
        //  validacao do formulario
        $this->form_validation->set_rules('titulo', 'titulo', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        $this->form_validation->set_rules('id_veiculo', 'veiculo', 'trim|required', array('required' => 'O campo %s é obrigatório.'));
        
        
        if ($this->form_validation->run() === FALSE)
        {
           return validation_errors();
        }
        else
        {
            return TRUE;
        }
    }


    //  verifica se a escola ou o aluno foi selecionada para a rota
    function verifica_escola_aluno_rota($id_usuario, $id_rota, $id_escola_aluno, $tipo){

        //  verifico se e para verificar aluno ou escola
        if($tipo == 'escola'){
            $this->db->where('id_escola', $id_escola_aluno);
        }else{
            $this->db->where('id_aluno', $id_escola_aluno);
        }

        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_rota', $id_rota);
        $this->db->where('ativo', 1);
        $query = $this->db->get('rotas_paradas');
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        return $query->num_rows();
    }



    //  busca as escolas das rotas
    public function getEscolasRotas($id_usuario, $id_rota){

        $dados = "";

        //  busco todas as escola do usuario
        $this->db->where('id_usuario', $id_usuario);
        $this->db->order_by('nome', 'asc');
        $query = $this->db->get('escolas');
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        //  faco um loop para organizar o array
        foreach ($query->result_array() as $key => $row){

            //  verifico se a escola esta na rota
            $num_rows = $this->verifica_escola_aluno_rota($id_usuario, $id_rota, $row['idescola'], 'escola');
            
            if($num_rows == 0){
                $row['ativo'] = false;
            }else{
                $row['ativo'] = true;
            }

            $dados[$key] = $row;
        }

        echo json_encode($dados);       
        
    }



    //  busca as escolas das rotas
    public function getAlunosRotas($id_usuario, $id_rota){

        $dados = "";

        //  busco todas as escola do usuario
        $this->db->select('alunos.*, escolas.idescola, escolas.nome as escola_nome');
        $this->db->from('alunos');
        $this->db->where('alunos.id_usuario', $id_usuario);
        $this->db->join('escolas', 'alunos.id_escola = escolas.idescola', 'left');
        $this->db->order_by('alunos.nome', 'asc');
        $query = $this->db->get();
        /*
        $this->db->where('id_usuario', $id_usuario);
        $this->db->order_by('nome', 'asc');
        $query = $this->db->get('alunos');
        */
         //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado




        
        //  faco um loop para organizar o array
        foreach ($query->result_array() as $key => $row){

            //  verifico se a escola esta na rota
            $num_rows = $this->verifica_escola_aluno_rota($id_usuario, $id_rota, $row['idaluno'], 'aluno');
            
            if($num_rows == 0){
                $row['ativo'] = false;
            }else{
                $row['ativo'] = true;
            }

            $dados[$key] = $row;
        }

        echo json_encode($dados);  



       
        /*
        
        //  busco todas os alunos
        $this->db->select('alunos.*, escolas.nome as escola_nome, rotas_paradas.ativo');
        $this->db->from('alunos');
        $this->db->join('rotas_paradas', 'rotas_paradas.id_aluno = alunos.idaluno', 'left');
        $this->db->join('escolas', 'escolas.idescola = alunos.id_escola', 'left');
        $this->db->where('alunos.id_usuario', $id_usuario);
        $this->db->order_by('alunos.nome', 'asc');
        $query = $this->db->get();
        //$result = $query->result_array();
        //  echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

         //  faco um loop para organizar o array
         foreach ($query->result_array() as $key => $row){
            if($row['ativo'] == 0){
                $row['ativo'] = true;
            }else{
                $row['ativo'] = false;
            }
            $dados[$key] = $row;

        }
        
        
        echo json_encode($dados);



        /*

        // //  busco todas as escolas
        // $this->db->where('id_usuario', $id_usuario);
        // $this->db->order_by('nome', 'asc');
        // $query = $this->db->get('alunos');
        // $result = $query->result_array();


        $this->db->select('alunos.idaluno, alunos.idaluno as aluno_id, alunos.nome as aluno_nome, alunos.sala as aluno_sala, alunos.turno as aluno_turno, alunos.genero as aluno_genero, alunos.foto as aluno_foto, alunos.contrato_ativo as aluno_contrato_ativo,
                responsaveis.idresponsavel as responsaveis_id, responsaveis.nome as responsaveis_nome, responsaveis.cpf as responsaveis_cpf, responsaveis.endereco as responsaveis_endereco, responsaveis.complemento as responsaveis_complemento, responsaveis.email as responsaveis_email, responsaveis.telefone_celular as responsaveis_telefone_celular, responsaveis.telefone_residencial as responsaveis_telefone_residencial, responsaveis.telefone_trabalho as responsaveis_telefone_trabalho, responsaveis.rg as responsaveis_rg,
                escolas.idescola as idescola, escolas.idescola as escola_id, escolas.nome as escola_nome, escolas.endereco as escola_endereco, escolas.complemento as escola_complemento, escolas.telefone as escola_telefone , 
                usuarios.idusuario as usuario_id, usuarios.nome as usuario_nome,');
        $this->db->from('alunos');
        $this->db->join('responsaveis', 'responsaveis.idresponsavel = alunos.id_responsavel');
        $this->db->join('usuarios', 'responsaveis.id_usuario = usuarios.idusuario');
        $this->db->join('escolas', 'alunos.id_escola = escolas.idescola');
        $this->db->where('usuarios.idusuario', $id_usuario);
        // $this->db->order_by('usuarios.nome', 'asc');
        // $this->db->order_by('responsaveis.nome', 'asc');
        $this->db->order_by('alunos.nome', 'asc');
        $query = $this->db->get();
         echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado


        foreach ($query->result_array() as $key => $row){
            if($row['ativo'] == 0){
                $row['ativo'] = true;
            }else{
                $row['ativo'] = false;
            }
            $dados[$key] = $row;

        }
        
        
        echo json_encode($dados);



        //  faco um loop para organizar o array
        foreach ($query->result_array() as $row){
            $alunos[ $row['idaluno'] ] = $row;
            $alunos[ $row['idaluno'] ]['checked'] = false;
        }
        
        
        //  busco os alunos marcados nessa rota
        $this->db->select("id_aluno");
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_rota', $id_rota);
        $query_paradas = $this->db->get('rotas_paradas');
        // $result_paradas = $query_paradas->result_array();
        
        //  faco o loop para ver as escolas das rotas selecionadas
        foreach ($query_paradas->result_array() as $row){
            
            //  verifico se foi checked
            if (array_key_exists($row['id_aluno'], $alunos)) {
                $alunos[ $row['id_aluno'] ]['checked'] = true;
            }
            
            
        }

        //  reinicio os indices do array
        $alunos = array_values($alunos);
        
        echo json_encode($alunos);

        //  echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        */
    }












    // funcao para buscar os alunos, mesma da escola
    function getAlunosTurno($id_usuario, $id_rota, $turno){

        //  busco todos alunos
        $this->db->select('alunos.*, escolas.nome as escola_nome');
        $this->db->from('alunos');
        $this->db->join('escolas', 'escolas.idescola = alunos.id_escola');
        $this->db->where('alunos.id_usuario', $id_usuario);
        $this->db->where('turno', $turno);
        $this->db->order_by('alunos.nome', 'asc');
        $query = $this->db->get();
        // $result = $query->result_array();


        //  faco um loop para organizar o array
        foreach ($query->result_array() as $row){
            $alunos[ $row['idaluno'] ] = $row;
            $alunos[ $row['idaluno'] ]['checked'] = false;
        }
        
        
        //  busco as escolas marcadas nessa rota
        $this->db->select("id_aluno");
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_rota', $id_rota);
        $query_paradas = $this->db->get('rotas_paradas');
        // $result_paradas = $query_paradas->result_array();
        
        //  faco o loop para ver as escolas das rotas selecionadas
        foreach ($query_paradas->result_array() as $row){
            
            
            //  verifico se foi checked
            if (array_key_exists($row['id_aluno'], $alunos)) {
                $alunos[ $row['id_aluno'] ]['checked'] = true;
            }else{
                $alunos[ $row['id_aluno'] ]['checked'] = 'false';
            }
            
        }

        //  reinicio os indices do array
        $alunos = array_values($alunos);
        
        echo json_encode($alunos);

    }



    //  atualiza a qtd de paradas de escola na rota
    public function atualizaQtdParadasEscola($id_usuario, $id_escola, $id_rota, $qtd_paradas){

        //  apago todas as escola da rota
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_rota', $id_rota);
        $this->db->where('id_escola', $id_escola);
        $this->db->delete('rotas_paradas');
        

        //  cadastro a escola na qtd de vezes desejada
        for($i=1; $i  <= $qtd_paradas; $i++){
            $dados = array( 
                            "tipo_parada" => 'escola', 
                            "id_rota" => $id_rota, 
                            "id_usuario" => $id_usuario,
                            "id_escola" => $id_escola
                          );
            
            $this->db->insert('rotas_paradas ', $dados);
            //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        }

         //  reordeno tods acrescentando um número para deixa esse como 0 
         $this->rota_model->reordena_paradas($id_rota);

    }



    function updateRota(){
        //  resgato os dados do post
        $json = file_get_contents('php://input');
        $_POST = json_decode($json, true);  //  deve se manter
      
        $ativo = $this->rota_model->updateRota( $this->input->post() );

        $msg = array('mensage' => "Rota atualizada com sucesso.", "status" => TRUE, 'ativo' => $ativo);
        echo json_encode($msg);
   
    }



    //  atualiza a localiuzação do veiculo na rota
    function atualizaLocalizacaoVeiculo($id_usuario, $idrota, $latitude, $longitude){

      //  atualizo na tabela o aviso ao responsavel
      $this->db->set('latitude', $latitude);
      $this->db->set('longitude', $longitude);
      $this->db->where('idrota', $idrota);
      $this->db->update('rotas');

    }



    function getParadas($id_usuario, $id_rota){
        
        $this->db->select('rotas_paradas.*, alunos.nome as aluno, alunos.id_escola as idescolaaluno, escolas.nome as escola');
        $this->db->from('rotas_paradas');
        $this->db->join('alunos', 'alunos.idaluno = rotas_paradas.id_aluno', 'left');
        $this->db->join('escolas', 'escolas.idescola = rotas_paradas.id_escola', 'left');
        $this->db->where('rotas_paradas.id_usuario', $id_usuario);
        $this->db->where('rotas_paradas.id_rota', $id_rota);
        $this->db->where('rotas_paradas.ativo', true);
        $this->db->order_by('rotas_paradas.ordem', 'asc');
        $query = $this->db->get();
        $result = $query->result_array();

        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        
        echo json_encode($result);
    }



    //  atualiza a ordem de embarque
    function atualizaOrdemParada(){

        //RECUPERAÇÃO DO FORMULÁRIO
        $json = file_get_contents("php://input");
        $_POST = json_decode($json, true);  //  deve se manter

        //  atualiza a ordem movida
        $this->rota_model->atualizaOrdemParada($_POST['idrota'], $_POST['from'], $_POST['to'], $_POST['element']['idrotaparada']);

        //  reorganizo a ordem a partir de 0
        $this->rota_model->reordena_paradas($_POST['idrota']);

    }




    //  busca o id do responsavel pelo token
    public function getIdResponsavelToken($token){

        $this->db->where('token', $token);
        $query = $this->db->get('logins_responsaveis');
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        return $query->row_array();
    }


    //  busca todos os usuarios que tem o responsavel cadastro
    public function getTodosUsuariosResponsavel($cpf){

        $this->db->where('cpf', $cpf);
        $query = $this->db->get('responsaveis');
        $result = $query->result_array();
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        
        if(count($result) > 0){            
            foreach($result as $dado){
                $ids[] = $dado['idresponsavel'];
            }

            //  voltos os ids dos usuarios com esse responsavel
        return $ids;

        }

        

    }


    //  busca todos os usuarios que tem o responsavel cadastro
    public function getTodosAlunosResponsavel($ids_responsavel){
        $this->db->where_in('id_responsavel', $ids_responsavel);
        $query = $this->db->get('alunos');
        $result = $query->result_array();
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        
        if(count($result) > 0){
            $ids = "";
            foreach($result as $dado){
                $ids[] = $dado['idaluno'];
            }
        }

        //  voltos os ids dos usuarios com esse responsavel
        return $ids;

    }


    


    //  busca os alunos na rota do responsavel
    public function getRotaDiaResponsavel($token, $status = 1){

        $result = "";

        $data = date("Y-m-d");

        //  dados do responsavel
        $resp = $this->getIdResponsavelToken($token);
        //print_r($resp);

        //  busco todos os usuarios que tem esse responsavel cadastrado
        $usuarios_responsaveis = $this->getTodosUsuariosResponsavel($resp['cpf']);
        //print_r($usuarios_responsaveis);

        $alunos = $this->getTodosAlunosResponsavel($usuarios_responsaveis);
        //  print_r($alunos);
        
        //  busco todos os alunos que o responsavel tem em rota
        $this->db->select("rotas_paradas_diarias.*, rotas_paradas.*, rotas.*, alunos.*, usuarios.nome as usuario_nome, veiculos.* ");
        $this->db->from('rotas_paradas');
        $this->db->join('rotas_paradas_diarias', 'rotas_paradas_diarias.id_rotaparada = rotas_paradas.idrotaparada', 'left');
        $this->db->join('alunos', 'alunos.idaluno = rotas_paradas.id_aluno', 'left');
        $this->db->join('rotas', 'rotas.idrota = rotas_paradas.id_rota', 'left');
        $this->db->join('veiculos', 'veiculos.idveiculo = rotas.id_veiculo', 'left');
        $this->db->join('usuarios', 'usuarios.idusuario = rotas_paradas.id_usuario', 'left');
        $this->db->where_in('rotas_paradas.id_aluno', $alunos);
        $this->db->where('rotas_paradas_diarias.data', $data);
        $this->db->where('rotas_paradas.ativo', 1);
        $this->db->where('rotas.online', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        
        //    echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        echo json_encode($result);
    }





     //  GERA A ROTA DO DIA
    function getRotaDia($id_usuario, $id_rota, $data = "", $id_escola = ""){
        
        
        //  verifico se não foi passada uma data, caso vazio pega do dia
        if(empty($data)){
            $data = date("Y-m-d");
        }

        

        //  verifico se a rota do dia já foi criada
        $this->verificaRotaDia($id_usuario, $id_rota, $data);

        //  atualizo o status para online
        $this->atualizaStatusRota($id_usuario, $id_rota, 1);

        //  verifico se foi passado o id da escola para pegar somente os alunos da propria escola e da rota
        if(!empty($id_escola)){
            $this->db->where('alunos.id_escola', $id_escola);
        }

       

        //  busco a rota do dia
        $this->db->select("rotas_paradas.*, rotas_paradas_diarias.*, alunos.nome as aluno_nome, alunos.sala as aluno_sala, alunos.sala as aluno_id_escola, alunos.idaluno as aluno_idaluno, alunos.foto as aluno_foto, escolas.nome as escola_nome, responsaveis.nome as responsavel_nome, responsaveis.idresponsavel as responsavel_id_responsavel, responsaveis.cpf, logins_responsaveis.onesignail_idplayer");
        $this->db->from('rotas_paradas');
        $this->db->join('rotas_paradas_diarias', 'rotas_paradas_diarias.id_rotaparada = rotas_paradas.idrotaparada');
        $this->db->join('alunos', 'alunos.idaluno = rotas_paradas.id_aluno', 'left');
        $this->db->join('escolas', 'escolas.idescola = rotas_paradas.id_escola', 'left');
        $this->db->join('responsaveis', 'responsaveis.idresponsavel = alunos.id_responsavel', 'left');
        $this->db->join('logins_responsaveis', 'logins_responsaveis.cpf = responsaveis.cpf', 'left');
        $this->db->where('rotas_paradas.id_usuario', $id_usuario);
        $this->db->where('rotas_paradas.id_rota', $id_rota);
        $this->db->where('rotas_paradas_diarias.data', $data);
        $this->db->where('rotas_paradas.ativo', 1);

        

        $this->db->where('rotas_paradas_diarias.hora_embarque', null);

        //  ordem da busca
        if(empty($id_escola)){
            $this->db->order_by('rotas_paradas.ordem', 'asc');
        }else{
            $this->db->order_by('alunos.sala', 'asc');
        }
        
        $query = $this->db->get();
        $result = $query->result_array();
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        
        
        if(count($result) > 0){
            foreach($result as $dado){



                //  msg de notificacao ao usuario

                //  verifico a saudação
                $hr = date(" H ");
                if($hr >= 12 && $hr <18) {
                    $resp = "Boa tarde";}
                else if ($hr >= 0 && $hr <12 ){
                    $resp = "Bom dia";}
                else {
                    $resp = "Boa noite";
                }

                $titulo = "Iniciando nosso itinerário.";
                $mensagem = "$resp, ".$dado['responsavel_nome']." estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de ".$dado['aluno_nome'].". Caso o aluno não irá utilizar o transporte por favor nos avise.";
                

                //  armazeno no sistema a mesagem ao responsavel
                if($dado['responsavel_notificado'] == 0 and !empty($dado['id_aluno'])){
                    $this->armazena_notificao_responsavel($titulo, $mensagem, $dado['responsavel_id_responsavel'], $dado['id_usuario']);
                }
                


                //  verifico se tem onesignal
                if(!empty($dado['onesignail_idplayer']) and $dado['responsavel_notificado'] == 0){
                    //  envia a msg via onesignal
                    onesignal_send_msg($titulo, $mensagem, $dado['onesignail_idplayer']);
                }
                //echo($dado['idrotaparada']);

                //  atualizo na tabela o aviso ao responsavel
                $this->db->set('responsavel_notificado', 1);
                $this->db->where('idrotaparadadiaria', $dado['idrotaparadadiaria']);
                $this->db->update('rotas_paradas_diarias');
                //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
                
            }
        }
        
        echo json_encode($result);
    }



    


    //  atualiza o status da rota
    public function atualizaStatusRota($id_usuario, $id_rota, $status){

        //  verifico o status
        if($status == 'true'){
            $status = TRUE;
        }else{
            $status = FALSE;
        }

        //  atualizo na tabela o aviso ao responsavel
        $this->db->set('online', $status);
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('idrota', $id_rota);
        $this->db->update('rotas');
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
    }



    //  envia mensagem para todos da rota
    public function send_msg_all_responsaveis($result){
        
    }


    //  verifica se a rota ja existe, caso false cria 
    function verificaRotaDia($id_usuario, $id_rota, $data){

        $idrotaparadadiaria = "";
                
        //  busco as paradas da rota
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_rota', $id_rota);
        $this->db->order_by('ordem', 'asc');
        $query_rotas_paradas = $this->db->get('rotas_paradas');
        // $result = $query->result_array();
        

        //  faco um loop para conferir se a parada já estão inseridas
        foreach ($query_rotas_paradas->result_array() as $key => $row){

            //  verifico se ja existe a parada no dia
            $paradaDia = $this->verificaParadaDia($data, $row['idrotaparada']);
            
            if(count($paradaDia) == 0){
                $idrotaparadadiaria = $this->insertParadaDia($data, $row['idrotaparada'], $id_usuario);
            }else{
               $idrotaparadadiaria = $paradaDia[0]['idrotaparadadiaria'];               
            }


            //  verifico se e do tipo aluno, caso true verifica se ira no dia
            if(!empty($row['id_aluno'])){
                $this->verifica_falta_aluno($data, $row['idrotaparada'], $row['id_aluno'], $idrotaparadadiaria);
            }

            
            
        }


        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

    }


    //  inseri a parada do dia
    function insertParadaDia($data, $id_rotaparada, $id_usuario){
        $dados = array( "data" => $data, "id_rotaparada" => $id_rotaparada, "id_usuario" => $id_usuario );
        $this->db->insert('rotas_paradas_diarias', $dados);
        return $this->db->insert_id();
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
    }


    //  pega a parada do dia
    function verificaParadaDia($data, $id_rotaparada){

         //  busco as paradas da rota do dia na tabela 
        $this->db->where('data', $data);
        $this->db->where('id_rotaparada', $id_rotaparada);
        $query = $this->db->get('rotas_paradas_diarias');
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        return $query->result_array();

    }




    //  verifica se o aluno ira no dia
    function verifica_falta_aluno($data, $id_rotaparada, $id_aluno, $idrotaparadadiaria){
        
        //  verifico se foi adicionado falta
        $this->db->where('data', $data);
        $this->db->where('id_rotaparada', $id_rotaparada);
        $this->db->where('id_aluno', $id_aluno);
        $this->db->order_by('idfataembraquealuno', 'DESC');
        $this->db->limit(1);  // Produces: LIMIT 10
        $query = $this->db->get('faltas_embarques_alunos');
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
   
        if($query->num_rows() > 0){

            $row = $query->row();
          
            //  atualizo na tabela o nao embarque do aluno
            $this->db->set('embarque', $row->embarque);
            $this->db->where('idrotaparadadiaria', $idrotaparadadiaria);
            $this->db->update('rotas_paradas_diarias');
            //   echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
            // exit;
        }

    }


    //  armazena a falta do aluno
    function setFaltaPresencaAluno($id_usuario, $id_rotaparada, $falta_ou_presenca, $data, $id_aluno){

        $dados = array( 
                        "embarque" => $falta_ou_presenca, 
                        "id_rotaparada" => $id_rotaparada, 
                        "id_usuario" => $id_usuario ,
                        "data"  => $data,
                        "id_aluno"  => $id_aluno
                      );
        $this->db->insert('faltas_embarques_alunos', $dados);
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

    }


    //  ARMAZENA O STATUS DO EMBARQUE DO ALUNO, SALVANDO O EMBARQUE OU NAO
    function set_embarque_aluno($id_usuario, $idrotaparadadiaria, $id_aluno, $hora_embarque, $aluno_embarcou, $idrotaparada, $id_rota, $ordem){

        // echo '$aluno_embarcou = '.$aluno_embarcou;


        //  atualizo na tabela o nao embarque do aluno
        $this->db->set('aluno_embarcou', $aluno_embarcou);
        $this->db->set('hora_embarque', $hora_embarque);
        $this->db->where('idrotaparadadiaria', $idrotaparadadiaria);
        $this->db->where('id_usuario', $id_usuario);
        $this->db->update('rotas_paradas_diarias');

        //  verifico se existe o id do aluno
        if($id_aluno != 'null'){

          //  busca os dados do aluno
          $query = $this->alunos_model->get_alunos($id_usuario, $id_aluno);
          $dados_aluno = $query->row();
          
          //  busca os dados do responsavel
          $dados_responsavel = $this->get_dados_login_responsavel( $dados_aluno->responsaveis_cpf );

          //  mensagem de notificacao do responsavel varia de acordo com o embarque
          if($aluno_embarcou == 1){
              $titulo = "Embarque de $dados_aluno->aluno_nome";
              $mensagem = "$dados_aluno->aluno_nome já está a bordo de nosso veículo, em breve chegaremos a $dados_aluno->escola_nome.";
          }else{
              $titulo = "Falta no embarque de $dados_aluno->aluno_nome";
              $mensagem = "$dados_aluno->aluno_nome não estava no local de embarque, informamos que demos continuidado a nosso itinerário.";
          }
          
          
          //  verifico se tem o playerId e envia uma msg para o responsavel avisando do embarque do aluno
          if(!empty( $dados_responsavel->onesignail_idplayer )){
              onesignal_send_msg($titulo, $mensagem, $dados_responsavel->onesignail_idplayer);

              //  armazeno a notificacao do responsavel
              $this->armazena_notificao_responsavel($titulo, $mensagem, $dados_aluno->responsaveis_id );

              //  aviso o proximo aluno que estamos a caminho
              $this->proximo_aluno_rota($ordem, $id_rota, $id_usuario);     
              
          }

        }

        
        
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
    }


    //  busca o proximo aluno da rota
    public function proximo_aluno_rota($ordem, $id_rota, $id_usuario){

    
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_rota', $id_rota);
        $this->db->where('ordem >', $ordem);
        $this->db->where('id_aluno <> ', "");
        $this->db->order_by('ordem');
        $this->db->limit(1);  // Produces: LIMIT 10
        $query = $this->db->get('rotas_paradas');
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        if($query->num_rows() > 0){

          $row = $query->row();

          //  busca os dados do aluno
          $query = $this->alunos_model->get_alunos($id_usuario, $row->id_aluno);
          $dados_aluno = $query->row();
          
          //  busca os dados do responsavel
          $dados_responsavel = $this->get_dados_login_responsavel( $dados_aluno->responsaveis_cpf );

          //  aviso ao proximo responsavel
          $titulo = "Aviso de embarque";
          $mensagem = "$dados_responsavel->nome já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado, em breve chegaremos para buscar o aluno(a) $dados_aluno->aluno_nome.";

          onesignal_send_msg($titulo, $mensagem, $dados_responsavel->onesignail_idplayer);
          //  armazeno a notificacao do responsavel
          $this->armazena_notificao_responsavel($titulo, $mensagem, $dados_aluno->responsaveis_id );


        }

        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
   

    }


    //  busca o player_id do responsavel
    public function get_dados_login_responsavel($cpf){
        $this->db->where('cpf', $cpf);
        $query = $this->db->get('tb_logins_responsaveis');
        return $query->row();
    }

    //  aramezeno a notifica ao responsavel
    public function armazena_notificao_responsavel($titulo, $mensagem, $idresponsavel, $id_usuario = "" ){
        $data = date("Y-m-d");
        $hora = date("H:i:s");
        $dados = array(
                "data"              => $data,
                "hora"              => $hora,
                "titulo" => $titulo,
                "mensagem" => $mensagem,
                "id_responsavel" => $idresponsavel,
                "id_usuario"        => $id_usuario
        );
        $this->db->insert('tb_notificacoes_responsaveis', $dados);
        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
    }


    //  aramazena notificacao ao usuario
    /*
    public function armazena_notificacoes_responsaveis($titulo, $mensagem, $id_responsavel, $id_usuario){

        $data = date("Y-m-d H:i:s");

        $dados = array(
                        "data"              => $data,
                        "titulo"            =>  $titulo,
                        "mensagem"          =>  $mensagem,
                        "id_responsavel"    => $id_usuario,
                        "id_usuario"        => $id_usuario
        );
        $this->db->insert("notificacoes_responsaveis", $dados); 
       
    }
    */







    














    


//     function getRota($id_usuario, $tipo_rota){
        
//         //  verifico se e preciso criar a rota
//         $this->geraRotas($id_usuario, $tipo_rota);

//         // 1 - BUSCO OS ALUNOS REFERENTE AO TURNO
//         switch($tipo_rota){
//             case 1: //  Ida Matutino

//             break;
//             case 2: //  Volta Matutino / Ida Vespertino

//             break;
//             case 3: //  Volta Vespertino / Ida Noturno

//             break;
//             case 4: //  Volta Noturno

//             break;
//         }


        
//         // $this->db->where('id_usuario', $id_usuario);
//         // $query = $this->db->get('escolas');
//         // $result = $query->result_array();
        
        
        
        
//         // echo json_encode($result);
//         // echo $this->db->last_query();   //  exibe o sql executado
//     }



//     //  FUNCA PARA BUSCA ROTA GERADA
//     function getAlunosRota(){
//         $this->db->select('alunos.idaluno as aluno_id, alunos.nome as aluno_nome, alunos.sala as aluno_sala, alunos.turno as aluno_turno, alunos.genero as aluno_genero, alunos.foto as aluno_foto, alunos.contrato_ativo as aluno_contrato_ativo,
//                             responsaveis.idresponsavel as responsaveis_id, responsaveis.nome as responsaveis_nome, responsaveis.cpf as responsaveis_cpf, responsaveis.endereco as responsaveis_endereco, responsaveis.complemento as responsaveis_complemento, responsaveis.email as responsaveis_email, responsaveis.telefone_celular as responsaveis_telefone_celular, responsaveis.telefone_residencial as responsaveis_telefone_residencial, responsaveis.telefone_trabalho as responsaveis_telefone_trabalho, responsaveis.rg as responsaveis_rg,
//                             escolas.idescola as idescola, escolas.idescola as escola_id, escolas.nome as escola_nome, escolas.endereco as escola_endereco, escolas.complemento as escola_complemento, escolas.telefone as escola_telefone , 
//                             usuarios.idusuario as usuario_id, usuarios.nome as usuario_nome,');
//         $this->db->from('alunos');
//         $this->db->join('responsaveis', 'responsaveis.idresponsavel = alunos.id_responsavel');
//         $this->db->join('usuarios', 'responsaveis.id_usuario = usuarios.idusuario');
//         $this->db->join('escolas', 'alunos.id_escola = escolas.idescola');
//         $this->db->where('usuarios.idusuario', $id_usuario);
//         // $this->db->order_by('usuarios.nome', 'asc');
//         // $this->db->order_by('responsaveis.nome', 'asc');
//         $this->db->order_by('alunos.nome', 'asc');
//         $query = $this->db->get();
//         // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
//         return $query;
//     }




//     //  GERA A ROTA DO DURANTE 7 DIAS
//     function geraRotas($id_usuario, $tipo_rota){

//         /*
//         PROCEDIMENTOS PARA GERAÇÃO DA ROTA

//         1 - PEGA O DIA ATUAL
//         2 - BUSCO O ALUNOS DO USUARIO
//         3 - BUSCO NA TB_ALUNOS_ROTAS O ENDERECO E O CARRO DO DIA
//         4 - BUSCO TODA AS ROTAS DO DIA DO USUARIO
//         5 - BUSCO NA TABELA DE FALTAS_ALUNOS OS ALUNOS QUE NÃO IRÃO A AULA NO DIA
//         6 - FAÇO UM LOOP NOS ALUNOS PARA INSERIR A ROTA DIARIA
        

//         5 - VERIFICO NA TABELA DE ROTAS_DIARIAS SE O A ROTA JÁ ESTÁ INSERIDA, CASO NAO ESTEJA CADSTRO E PEGO O ID, SENAO RETORNO O ID
//             5.1 - TRUE - VERIFICADO QUE ESTÁ INSERIDA EU APENAS VERIFICO SE O ALUNO IRA FALTAR, CASO TRUE REMOVO ELE DA ROTA

//         */


//         $data = date("2019-03-05");
//         // $data = date("Y-m-d");
//         $diasemana_numero = date('w', strtotime($data));


//         //  BUSCO OS ALUNOS
//         $result_alunos = $this->getAlunos($id_usuario, $tipo_rota);

// print_r($result_alunos);


//         //  6 - FAÇO UM LOOP NOS ALUNOS PARA INSERIR A ROTA DIARIA
//         foreach ($result_alunos as $row){


//             //  busco o endereco do dia do aluno
//             $dados = $this->getRotasAlunos($id_usuario, $diasemana_numero, $row['idaluno']); // tb_alunos_rotas

//             print_r($dados);

//             //  busco a rota do aluno caso esteja cadastrada ou altero quando já inserida
//             $idrotadiaria = $this->verificaRota_dia($data, $id_usuario, $row['idaluno'], $dados);

//             //  verifico se tem alteracão de ida/volta no endereço ou falta na escola
//             $verifica_ida_volta_enderecos = $this->verifica_ida_volta_enderecos($id_usuario, $data, $row['idaluno'], $idrotadiaria);   //  tb_faltas_troca_endereco_alunos
//         }
//     }





//     //  pego a ultima informação inserida na tb_faltas_troca_endereco e atualiza a ida e local do aluno
//     public function verifica_ida_volta_enderecos($id_usuario, $data, $id_aluno, $idrotadiaria){
        
//         $this->db->where('id_usuario', $id_usuario);
//         $this->db->where('id_aluno', $id_aluno);
//         $this->db->where('data =', $data);
//         $query = $this->db->get('faltas_troca_endereco_alunos');
//         $row = $query->last_row();
//         //$row = $query->row_array();
//         $num_rows = $query->num_rows();
        
//         if($num_rows > 0){ // cadastro   
      
//             //  verifico se o aluno vai para a aula
//             if($row->ida_aula_status != ''){
//                 $dados['ida_aula_status'] = $row->ida_aula_status;
//             }

//             //  verifico se o aluno não vai para a aula
//             if($row->volta_aula_status != ''){
//                 $dados['volta_aula_status'] = $row->volta_aula_status;
//             }

//             //  verifico se o aluno mudou o endereco de ida para a aula
//             if($row->ida_id_endereco != ''){
//                 $dados['ida_id_endereco'] = $row->ida_id_endereco;
//             }

//             //  verifico se o aluno mudou o endereco de volta da a aula
//             if($row->volta_id_endereco != ''){
//                 $dados['volta_id_endereco'] = $row->volta_id_endereco;
//             }

//             //  atualizo na tabela
//             $this->db->where('idrotadiaria', $idrotadiaria);
//             $this->db->update('rotas_diarias', $dados);
//             // echo "<pre>" . $this->db->last_query() . '</pre>';   //  exibe o sql executado

//         }

//     }



    

//     // //  busca os endereço de embarque e destino do aluno pela tabela alunos_rotas para saber no dia onde o aluno estará
//     // public function getEnderecosArray($id_aluno, $idrotadiaria){
        
//     //     if(count( $result_alunos_rotas ) > 0){
//     //         foreach ($result_alunos_rotas as $key => $value) {
        
//     //             if($value['id_aluno'] == $id_aluno){
//     //                 $this->db->where('idrotadiaria', $idrotadiaria);
//     //                 $this->db->update('rotas_diarias', $value);
//     //                 // echo "<br />" . $this->db->last_query();   //  exibe o sql executado
//     //             }
//     //         }
//     //     }

//     // }


//     public function verificaRota_dia($data, $id_usuario, $id_aluno, $dados){
       
//         //  verifico se ja existe a rota
//         $this->db->where('id_usuario', $id_usuario);
//         $this->db->where('id_aluno', $id_aluno);
//         $this->db->where('data =', $data);
//         $query = $this->db->get('rotas_diarias');
//         // echo "<pre>" . $this->db->last_query() . '</pre>';   //  exibe o sql executado
//         // $result  = $query->result_array();
//         $num_rows = $query->num_rows();
        
//         //  ajusto o dados 
//         $dados['data'] = $data;
//         $dados['id_usuario'] = $id_usuario;


//         if($num_rows == 0){ // cadastro   
//             $result = $this->db->insert('rotas_diarias', $dados);
//             return $this->db->insert_id();
//         }else{  //  atualizo
//             $row = $query->row_array();
//             $this->db->where('idrotadiaria', $row['idrotadiaria']);
//             $this->db->update('rotas_diarias', $dados);
//             return $row['idrotadiaria'];
//         }

//         // echo "<pre>" . $this->db->last_query() . '</pre>';   //  exibe o sql executado
//         //exit;
        
//     }

    




//     //  VERIFICO SE O SISTEMA TEM ALGUMA ALTERAÇÃO NA IDA E VOLTA DO ALUNO E NA ALTERAÇÃO DE ENDEREÇÕ
//     public function getFaltasAlunos($id_usuario, $data, $id_aluno){
//         $this->db->where('id_usuario', $id_usuario);
//         $this->db->where('id_aluno', $id_aluno);
//         $this->db->where('data_falta =', $data);
//         $query = $this->db->get('faltas_alunos');
//         echo "<br />" . $this->db->last_query();   //  exibe o sql executado
//         $result  = $query->result_array();
//         return $result ;  
//     }



   




//     public function getRotaDiaria($id_usuario, $data){

//         $this->db->where('data = ', $data);
//         $this->db->where('id_usuario', $id_usuario);
//         $query = $this->db->get('rotas_diarias');
//         $result  = $query->result_array();
//         // echo "<br />" . $this->db->last_query();   //  exibe o sql executado
//         return $result ;        
//     }




//     public function getRotasAlunos($id_usuario, $diasemana_numero, $id_aluno){
        
//         //  verifico o dia da semana para buscar o endereco e veiculo do dia
//         switch($diasemana_numero){
//             case 0: //  domingo

//             break;
//             case 1: //  segunda
//                 $this->db->select("idIdaEnderecoSegunda as ida_id_endereco, idIdaVeiculoSegunda as ida_id_veiculo, id_aluno, idVoltaEnderecoSegunda as volta_id_endereco, idVoltaVeiculoSegunda as volta_id_veiculo");
//             break;
//             case 2: //  terça
//                 $this->db->select("idIdaEnderecoTerca as ida_id_endereco, idIdaVeiculoTerca as ida_id_veiculo, id_aluno, idVoltaEnderecoTerca as volta_id_endereco, idVoltaVeiculoTerca as volta_id_veiculo");
//             break;
//             case 3: //  quarta
//                 $this->db->select("idIdaEnderecoQuarta as ida_id_endereco, idIdaVeiculoQuarta as ida_id_veiculo, id_aluno, idVoltaEnderecoQuarta as volta_id_endereco, idVoltaVeiculoQuarta as volta_id_veiculo");
//             break;
//             case 4: //  quinta
//                 $this->db->select("idIdaEnderecoQuinta as ida_id_endereco, idIdaVeiculoQuinta as ida_id_veiculo, id_aluno, idVoltaEnderecoQuinta as volta_id_endereco, idVoltaVeiculoQuinta as volta_id_veiculo");
//             break;
//             case 5: //  sexta
//                 $this->db->select("idIdaEnderecoSexta as ida_id_endereco, idIdaVeiculoSexta as ida_id_veiculo, id_aluno, idVoltaEnderecoSexta as volta_id_endereco, idVoltaVeiculoSexta as volta_id_veiculo");
//             break;
//             case 6: //  sabado

//             break;
//         }

       
//         $this->db->where('id_usuario', $id_usuario);
//         $this->db->where('id_aluno', $id_aluno);
//         $query = $this->db->get('alunos_rotas');
//         // echo "<br />" . $this->db->last_query();   //  exibe o sql executado
//         //  $result  = $query->result_array();
//         $row = $query->row_array();
//         return $row;
//     }



//     public function getAlunos($id_usuario, $tipo_rota){

//         //  VERIFICO O TIPO DE ROTA PARA PEGAR OS ALUNOS DO TURNO ESPECIFO
//         switch($tipo_rota){
//             case 1: //  Ida Matutino
//             $turnos = array('I', 'M');
//             $this->db->or_where_in('turno', $turnos);
//             break;
//             case 2: //  Volta Matutino / Ida Vespertino
//                 $turnos = array('M', 'V');
//                 $this->db->or_where_in('turno', $turnos);
//             break;
//             case 3: //  Volta Vespertino / Ida Noturno
//                 $turnos = array('I', 'V', 'N');
//                 $this->db->or_where_in('turno', $turnos);
//             break;
//             case 4: //  Volta Noturno
//                 $this->db->where('turno', 'N');
//             break;
//         }

        
//         $this->db->where('id_usuario', $id_usuario);
//         $this->db->where('contrato_ativo = 1');
//         $query = $this->db->get('alunos');
//         $result  = $query->result_array();
//         //  echo "<br />" . $this->db->last_query();   //  exibe o sql executado
//         return $result; 
//     }




    
    













}