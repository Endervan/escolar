<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Veiculo_model extends CI_Model
{

    function __construct(){
        $this->load->database();
    }


    //  colunas da tabela
    public function array_sql($dados, $action){

        $dados = array(
            'id_usuario' => $dados['id_usuario'],
            'placa' => $dados['placa'],
            'marca' => $dados['marca'],
            'ano' => $dados['ano'],
            'modelo' => $dados['modelo'],
            'cor' => $dados['cor']
        );

        return $dados;
    }


    //  cadastra o registro
    public function cadastra($dados, $action){
        $dados = $this->array_sql($dados, $action);       
        return $this->db->insert('veiculos', $dados);
    }

    //  verifica se ja existe a placa cadastrado para o usuario
    public function verifica_placa($idusuario, $placa, $idveiculo = ""){
        
        // verifico se a alteracao e alteracao
        if($idveiculo != 0){
            $this->db->where("idveiculo <>  $idveiculo ");
        }
        
        $this->db->where("id_usuario = $idusuario AND placa = '$placa' ");
        $query = $this->db->get("veiculos");
         // echo $this->db->last_query();   //  exibe o sql executado
        return $query->num_rows();
    }


    
    //  atualiza os dados
    public function update($dados, $idveiculo, $action){

        $dados = $this->array_sql($dados, $action);
        $this->db->where('idveiculo', $idveiculo);
        return $this->db->update('veiculos', $dados);
       // echo $this->db->last_query();   //  exibe o sql executado
        
    }


    //  exclui o responsavel
    public function delete($idusuario, $idveiculo){
        $this->db->where('id_usuario', $idusuario);
        $this->db->where('idveiculo', $idveiculo);
        $query = $this->db->delete('veiculos');
        //echo $this->db->last_query();   //  exibe o sql executado
        return $query;
    }


}