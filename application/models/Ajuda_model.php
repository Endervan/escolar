<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajuda_model extends CI_Model
{

    function __construct(){
        $this->load->database();    //  carrega o banco
    }


    // funcao para retornar as categorias
    public function getCategorias($tipo_ajuda){
        $this->db->where('tipo_ajuda', $tipo_ajuda);
        $this->db->where('ativo', true);
        $query = $this->db->get('categorias_ajudas');
        return $query->result_array();
    }

    //  busca as perguntas
    public function getPerguntas($id_categoriaajuda){
        $this->db->where('id_categoriaajuda', $id_categoriaajuda);
        $this->db->where('ativo', true);
        $query = $this->db->get('ajuda_respostas');
        // echo $this->db->last_query();   //  exibe o sql executado
        return $query->result_array();
    }




}