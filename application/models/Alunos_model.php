<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alunos_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }



    //  colunas da tabela
    public function array_sql($dados, $action){

        $dados = array(
            'nome' => $dados['nome'],
            'sala' => $dados['sala'],
            'turno' => $dados['turno'],
            'id_responsavel' => $dados['id_responsavel'],
            'id_usuario' => $dados['id_usuario'],
            'id_escola' => $dados['id_escola'],
            'genero' => $dados['genero'],
            'foto' => $dados['foto']
        );

        return $dados;
    }


    //  cadastra o registro
    public function cadastra($dados, $action){

        $mes = date("m");
        $ano = date("Y");
        $inicio = $ano.'-'.$mes.'-'.$dados['dia_pagamento'];
        //$inicio = $ano.'-'.$mes.'-'.'28';
        
       // $dados = $this->array_sql($dados, $action);       
        $result = $this->db->insert('alunos', $dados);
        $idaluno = $this->db->insert_id();

       
        //  cadastro o financeiro do aluno
        if($dados['qtd_parcelas'] > 0){
            for($i=1; $i <= $dados['qtd_parcelas']; $i++){
               $parcelas=$i;
               $data_termino = new DateTime($inicio);
               $data_termino->add(new DateInterval('P'.$parcelas.'M'));
               $termino_pagamento=$data_termino->format('Y-m-d');

               $dados1 = array(
                        'titulo' => 'Mensalidade aluno(a) '.$dados['nome'].' - '.$i .'/'.$dados['qtd_parcelas'],
                        'id_aluno' => $idaluno,
                        'data' => $termino_pagamento,
                        'valor' => str_replace(['.', ','], ['', '.'], $dados['valor_parcela']),
                        'tipo_fluxo' => 'receita'
               );
               $this->db->insert('fluxo_caixa', $dados1);
            }
        }
       
      




        //  insiro o id do aluno na tb_alunos_rotas para reservar o id
        //$dados1 = array("id_aluno"=>$idaluno, "id_usuario"=> $dados['id_usuario'] );
        //$result = $this->db->insert('alunos_rotas', $dados1);
        
        return $result;
    }


    //  atualiza os dados
    public function update($dados, $idaluno, $action){

  //      $dados = $this->array_sql($dados, $action);
        $this->db->where('idaluno', $idaluno);
        return $this->db->update('alunos', $dados);
        echo $this->db->last_query();   //  exibe o sql executado
        
    }



    //  deletar o registro
    public function delete($idusuario, $idaluno){
      
        //  deleta as rotas do aluno
        //$this->db->where('id_aluno', $idaluno);
        //$this->db->delete('alunos_rotas');

        //  deleta os enderecos do aluno
        //$this->db->where('id_aluno', $idaluno);
        //$this->db->delete('enderecos_alunos');

        //  deleta o aluno
        $this->db->where('id_usuario', $idusuario);
        $this->db->where('idaluno', $idaluno);
        $this->db->delete('alunos');
        echo $this->db->last_query();   //  exibe o sql executado
    }




    //  busca todos os alunos
    public function get_alunos($id_usuario, $id_aluno = ''){

         //  verifico se e apra exibir um reponsvel
         if(!empty($id_aluno)){
            $this->db->where('alunos.idaluno', $id_aluno);
        }

        $this->db->select('alunos.idaluno as aluno_id, alunos.nome as aluno_nome, alunos.sala as aluno_sala, alunos.turno as aluno_turno, alunos.genero as aluno_genero, alunos.foto as aluno_foto, alunos.contrato_ativo as aluno_contrato_ativo,
                            responsaveis.idresponsavel as responsaveis_id, responsaveis.nome as responsaveis_nome, responsaveis.cpf as responsaveis_cpf, responsaveis.endereco as responsaveis_endereco, responsaveis.complemento as responsaveis_complemento, responsaveis.email as responsaveis_email, responsaveis.telefone_celular as responsaveis_telefone_celular, responsaveis.telefone_residencial as responsaveis_telefone_residencial, responsaveis.telefone_trabalho as responsaveis_telefone_trabalho, responsaveis.rg as responsaveis_rg, responsaveis.uf as responsaveis_uf, responsaveis.bairro as responsaveis_bairro, responsaveis.cidade as responsaveis_cidade,
                            escolas.idescola as idescola, escolas.idescola as escola_id, escolas.nome as escola_nome, escolas.endereco as escola_endereco, escolas.complemento as escola_complemento, escolas.telefone as escola_telefone , 
                            usuarios.idusuario as usuario_id, usuarios.nome as usuario_nome,');
        $this->db->from('alunos');
        $this->db->join('responsaveis', 'responsaveis.idresponsavel = alunos.id_responsavel');
        $this->db->join('usuarios', 'responsaveis.id_usuario = usuarios.idusuario');
        $this->db->join('escolas', 'alunos.id_escola = escolas.idescola');
        $this->db->where('usuarios.idusuario', $id_usuario);
        // $this->db->order_by('usuarios.nome', 'asc');
        // $this->db->order_by('responsaveis.nome', 'asc');
        $this->db->order_by('alunos.nome', 'asc');
        $query = $this->db->get();
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        return $query;
    }


}