<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Endereco_aluno_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }



    //  colunas da tabela
    public function array_sql($dados, $action){

        $dados = array(
            'id_usuario' => $dados['id_usuario'],
            'titulo' => $dados['titulo'],
            'endereco' => $dados['endereco'],
            'complemento' => $dados['complemento'],
            'id_aluno' => $dados['id_aluno'],
            'bairro' => $dados['bairro'],
            'cidade' => $dados['cidade'],
            'uf' => $dados['uf'],
            'cep' => $dados['cep'],
            'endereco_principal' => $dados['endereco_principal'],
            'embarca_depois_idaluno' => $dados['embarca_depois_idaluno'],
            'desembarca_depois_idaluno' => $dados['desembarca_depois_idaluno']
        );

        return $dados;
    }


    //  atualizo os enderecos para nao principal para o inserido ou atualizado sobreponha
    function atualiza_status_endereco_principal($idenderecoaluno, $id_aluno){
        //  coloca todos os enderecos para NAO
        $dados['endereco_principal'] = 'NAO';
        $this->db->where('id_aluno', $id_aluno);
        $this->db->update('enderecos_alunos', $dados);

        //  atualiza o endereco selecionado para principal
        $dados['endereco_principal'] = 'SIM';
        $this->db->where('idenderecoaluno', $idenderecoaluno);
        $this->db->update('enderecos_alunos', $dados);

    }




    function getCodigoRotaAluno($id_usuario, $id_aluno){
        //  busco os dados do aluno
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('idaluno', $id_aluno);
        $query = $this->db->get('alunos');
        //  echo "<br />" . $this->db->last_query();   //  exibe o sql executado
        //  $result  = $query->result_array();
        $row = $query->row_array();

        //  verifico o turno do aluno para saber em qual rota ele se encaixara
        switch($row['turno']){
            case "I":
                $turno['embarque'] = 'rota_1_ordem';    //  ida 
                $turno['desembarque'] = 'rota_3_ordem'; //  volta 
                return $turno;   
            break;
            case "M":
                $turno['embarque'] = 'rota_1_ordem';    //  ida 
                $turno['desembarque'] = 'rota_2_ordem'; //  volta 
                return $turno;   
            break;
            case "V":
                $turno['embarque'] = 'rota_2_ordem';    //  ida 
                $turno['desembarque'] = 'rota_3_ordem'; //  volta 
                return $turno;   
            break;
            case "N":
                $turno['embarque'] = 'rota_3_ordem';    //  ida 
                $turno['desembarque'] = 'rota_4_ordem'; //  volta 
                return $turno;   
            break;
        }



        // if($tipo_embarque_desembarque == 'embarque'){
        //     switch($row['turno']){
        //         case "I":
        //             return 1;   //  ida matutino
        //         break;
        //         case "M":
        //             return 1;
        //         break;
        //         case "V":
        //             return 2;
        //         break;
        //         case "N":
        //             return 3;
        //         break;
        //     }
        // }else{  //  desembarque
        //     switch($row['turno']){
        //         case "I":
        //             return 3;   //  ida matutino
        //         break;
        //         case "M":
        //             return 2;
        //         break;
        //         case "V":
        //             return 3;
        //         break;
        //         case "N":
        //             return 4;
        //         break;
        //     }
        // }
        

    }



  
    function atualiza_embarque($idenderecoaluno, $id_usuario, $embarca_depois_idaluno, $turno){

        $embarque = $turno['embarque'];
        $desembarque = $turno['desembarque'];

        //  verifico se ele vai ser o primeiro aluno
        if($embarca_depois_idaluno == 0){
            //  seta o aluno para 0 esse número é utilizado apenas para colocar em primeiro antes de ordenar novamente
            $dados[ $embarque ] = 1;
            $this->db->where('idenderecoaluno', $idenderecoaluno);
            $this->db->update('enderecos_alunos', $dados);
            // echo "<br />" . $this->db->last_query();   //  exibe o sql executado
        }else{  //  BUSCO A ORDEM DO USUARIO DEPOIS DO ALUNO PARA ORDERNAR

            $this->db->where('id_aluno', $embarca_depois_idaluno);
            $this->db->where('id_usuario', $id_usuario);
            $query = $this->db->get('enderecos_alunos');
            $row = $query->row_array();
            echo "<br /> 1 ==========" . $this->db->last_query();   //  exibe o sql executado
            //  atualizo a ordem
            $dados[ $embarque ] = $row[ $embarque ] + 1;

            //  ATUALIZO A ORDEM DO ALUNO ATUAL + 1 PRA FICAR NA ORDEM ACIMA DO ALUNO ANTERIOR
            $this->db->where('idenderecoaluno', $idenderecoaluno);
            $this->db->update('enderecos_alunos', $dados);

            //  busco os alunos para atualizar a ordem para ordenar depois do aluno que acabou de entrar
            $this->db->where('idenderecoaluno <> ', $idenderecoaluno);
            $this->db->where('id_usuario', $id_usuario);
            $this->db->where("$embarque >= ", $dados[ $embarque ]);
            $this->db->order_by("$embarque ASC");
            $query = $this->db->get('enderecos_alunos');
              echo "<br /> 2 ==========" . $this->db->last_query();   //  exibe o sql executado
            
        
            //  atualizo a ordem dos alunos
            foreach ($query->result_array() as $row){
                    echo $dados[ $embarque] = $dados[ $embarque ] + 1;
                    $this->db->where('idenderecoaluno', $row['idenderecoaluno']);
                    $this->db->update('enderecos_alunos', $dados);
                     echo "<br /> 3 ==========" . $this->db->last_query();   //  exibe o sql executado
            }


        }

      
    }

   

    









    function get_ordem_rota($idenderecoaluno, $id_usuario, $id_aluno, $embarca_depois_idaluno, $desembarca_depois_idaluno){

        //  verifico quais rotas o aluno vai embarcar de acordo com seu turno
        $turno = $this->getCodigoRotaAluno($id_usuario, $id_aluno);

        
        //  atualizo a ordem do embarque
        // $this->atualiza_embarque($idenderecoaluno, $id_usuario, $embarca_depois_idaluno, $turno);    //  funcionando
        
        
        
        $this->atualiza_embarque($idenderecoaluno, $id_usuario, $embarca_depois_idaluno, $turno['embarque']);

        $this->atualiza_embarque($idenderecoaluno, $id_usuario, $desembarca_depois_idaluno, $turno['desembarque']);

        
        


       
        //  busco a ordem de embarque do aluno anterior
    





        print_r($turno);

        echo 'final';
        exit;


    }







    // ==================================================================================================   //
    //  NOVA ORDENAÇÃO
    // ==================================================================================================   //

    function getRotaTurno($tipo_rota, $turno){
        //  VERIFICO QUAL A ROTA DEVO SELECIONAR
        if($tipo_rota == 'embarque'){
            switch($turno){
                case "I":   //  ida matutino e integral
                    $turno = array("I", "M");
                    $rota['turno'] = $turno;
                    $rota['coluna_tb'] = 'rota_1_ordem';
                    return $rota;
                break;
                case "M": //  ida vespertino e volta matutino
                    $turno = array("I", "M");
                    $rota['turno'] = $turno;
                    $rota['coluna_tb'] = 'rota_1_ordem';
                    return $rota;
                break;
                case "V": //  volta vespertino, integral e ida noturno
                    $turno = array("M", "V");
                    $rota['turno'] = $turno;
                    $rota['coluna_tb'] = 'rota_2_ordem';
                    return $rota;
                break;
                case "N": //  ida noturno
                    $turno = array("V", "I", "N");
                    $rota['turno'] = $turno;
                    $rota['coluna_tb'] = 'rota_3_ordem';
                    return $rota;
                break;
            }
        }else{

        }
    }




    function atualizaOrdem($posicao_inicial, $posicao_final, $dados_aluno, $tipo_rota){

        $dados = [];
        //  busco a rota do aluno
        $turno = $this->getRotaTurno($tipo_rota, $dados_aluno['turno']);
        $rota_coluna = $turno['coluna_tb'];

       
        //  busca a ordem do aluno atualizada
        $this->db->where('idenderecoaluno = ', $dados_aluno['idenderecoaluno']);
        $query = $this->db->get('enderecos_alunos');
        $row = $query->row_array();

       
        echo '$posicao_inicial = ' . $posicao_inicial;
        echo '$posicao_final = ' . $posicao_final;
        
        echo '| posicao anterior | '. $row['rota_1_ordem'];

        echo '| subtração | '. ($posicao_inicial - $posicao_final );

        if($posicao_inicial > $posicao_final){
            echo '    SUBIU';
            echo '| posicao_rota = '.  $posicao_final = ($row['rota_1_ordem'] - ($posicao_inicial - $posicao_final ));
        }else{
            echo '    DESCEU';
            echo '| posicao_rota = '.  $posicao_final = ($row['rota_1_ordem'] + ($posicao_final - $posicao_inicial ));
        }
        


       print_r($dados_aluno);

        
        // echo '------posicao_final somada = '.       $posicao_final = $posicao_final + 1;

        // echo 'posicao_inicial = '. $posicao_inicial;
        // echo 'posicao_final = '. $posicao_final;
        // echo 'tipo_rota = '. $tipo_rota;
       //  print_r($dados);

        






       
       exit;


        //  atualizd a posicao do aluno movido
        $dados["$rota_coluna"] = $posicao_final;
        $this->db->where('idenderecoaluno', $dados_aluno['idenderecoaluno']);
        $this->db->update('enderecos_alunos', $dados);
        echo $this->db->last_query();   //  exibe o sql executado

       

        //  busco os alunos para atualizar a ordem do embarque retirando o atual aluno pois a ordem já foi atualizada
        $this->db->where('idenderecoaluno <> ', $dados_aluno['idenderecoaluno']);
        $this->db->where('id_usuario', $dados_aluno['id_usuario']);
        $this->db->where("$rota_coluna >= ", $posicao_final);
        $this->db->order_by("CAST(tb_enderecos_alunos.$rota_coluna AS unsigned)");
        $query = $this->db->get('enderecos_alunos');
        
        echo $this->db->last_query();   //  exibe o sql executado



        //  atualizo a ordem dos alunos
        foreach ($query->result_array() as $row){
                $dados["$rota_coluna"] = $dados["$rota_coluna"] + 1;
                $this->db->where('idenderecoaluno', $row['idenderecoaluno']);
                $this->db->update('enderecos_alunos', $dados);
                echo $this->db->last_query();   //  exibe o sql executado
        }
        


    }

    // ==================================================================================================   //
    //  NOVA ORDENAÇÃO
    // ==================================================================================================   //




























    



    function ajusta_ordem_rota($id_usuario, $id_aluno){

         //  busco os dados do aluno
         $this->db->where('id_usuario', $id_usuario);
         $this->db->where('idaluno', $id_aluno);
         $query = $this->db->get('alunos');
         //  echo "<br />" . $this->db->last_query();   //  exibe o sql executado
         $row = $query->row_array();

         // verifico o turno do aluno para atualizar a rota
        switch($row['turno']){
            case "M":
                $turno = array('I', 'M');
            break;
        }

        //  pega os alunos da rota
        $query = $this->getAlunosRota($id_usuario, $turno);
       

        $i = 0;
        foreach ($query->result_array() as $key => $row){
            
            // if($row['embarca_depois_idaluno'] == 0){
            //     // echo "PRIMEIRO ALUNO ID = ". $row['aluno_id'] . ' na key = ' . $key . ' e o próximo é ';
            //     echo 'próximo aluno = ' . $this->busca_ordem_aluno($query, $row['aluno_id'], 'embarca_depois_idaluno');
            // }   
            
            echo  'Aluno = '. $row['aluno_id'] . ' e o próximo aluno = ' . $this->busca_ordem_aluno($query, $row['aluno_id'], 'embarca_depois_idaluno');


            if($i == 3){
                exit;
            }

            $i++;
            

        }   




        echo 'fim';



        // //  busco todos os aluno do turno especifico
        // $this->db->where('id_usuario', $id_usuario);
        // $query = $this->db->get('alunos');




        
        // $this->db->where('id_usuario', $id_usuario);
        // $query = $this->db->get('enderecos_alunos');

        // echo $this->db->last_query();   //  exibe o sql executado

        // print_r($query);


    }


    function busca_ordem_aluno($query, $id_aluno, $embarque_ou_desembarque){


        foreach ($query->result_array() as $key => $row){

            if($row[ $embarque_ou_desembarque ] == $id_aluno){
                return $row[ $embarque_ou_desembarque ];
            }

        }

    }



    //  busca os alunos do turno informado
    function getAlunosRota($id_usuario, $turno){

        $this->db->select('enderecos_alunos.idenderecoaluno, enderecos_alunos.rota_1_ordem, enderecos_alunos.rota_2_ordem, enderecos_alunos.rota_3_ordem, enderecos_alunos.rota_4_ordem, enderecos_alunos.embarca_depois_idaluno, enderecos_alunos.desembarca_depois_idaluno,
                           alunos.idaluno as aluno_id, alunos.nome as aluno_nome, alunos.sala as aluno_sala, alunos.turno as aluno_turno, alunos.genero as aluno_genero, alunos.foto as aluno_foto, alunos.contrato_ativo as aluno_contrato_ativo
                           ');
        $this->db->from('enderecos_alunos');
        $this->db->join('alunos', 'alunos.idaluno = enderecos_alunos.id_aluno');
        $this->db->where('alunos.id_usuario', $id_usuario);
        $this->db->where_in('alunos.turno', $turno);
        // $this->db->order_by('CAST( alunos.nome as UNSIGNED )', 'asc');
        $query = $this->db->get();
        // echo $this->db->last_query();   //  exibe o sql executado
        return $query;
    }

    
























    
























    // //  atualizo a ordem do embarque
    // function atualiza_ordem_embarque($idenderecoaluno, $id_usuario, $id_aluno, $embarca_depois_idaluno){


    // //    echo $rota_embarque = $this->getCodigoRotaAluno($id_usuario, $id_aluno, 'embarque');

    //     //  verifico se ele vai ser o primeiro aluno
    //     if($embarca_depois_idaluno == 0){
    //         //  seta o aluno para 0 esse número é utilizado apenas para colocar em primeiro antes de ordenar novamente
    //         $dados['ordem_embarque'] = 1;
    //  //       $dados[$rota_embarque] = 1;
    //         $this->db->where('idenderecoaluno', $idenderecoaluno);
    //         $this->db->update('enderecos_alunos', $dados);
    //         // echo "<br />" . $this->db->last_query();   //  exibe o sql executado

    //     }else{  //  BUSCO A ORDEM DO USUARIO DEPOIS DO ALUNO PARA ORDERNAR
                       
    //         $dados['ordem_embarque'] = $this->get_ordem_embarque_desembarque($id_usuario, $embarca_depois_idaluno, '', 'embarque') + 1;
    //         $dados[$rota_embarque] = $dados['ordem_embarque'];
    //         //  ATUALIZO A ORDEM DO ALUNO ATUAL + 1 PRA FICAR NA ORDEM CORRERA
    //         $this->db->where('idenderecoaluno', $idenderecoaluno);
    //         $this->db->update('enderecos_alunos', $dados);
    //     }


    //     //  busco os alunos para atualizar a ordem do embarque retirando o atual aluno pois a ordem já foi atualizada
    //     $this->db->where('idenderecoaluno <> ', $idenderecoaluno);
    //     $this->db->where('id_usuario', $id_usuario);
    //     $this->db->where('ordem_embarque >= ', $dados['ordem_embarque']);
    //     $this->db->order_by('ordem_embarque ASC');
    //     $query = $this->db->get('enderecos_alunos');
        
       
    //     //  atualizo a ordem dos alunos
    //     foreach ($query->result_array() as $row){
    //             $dados['ordem_embarque'] = $dados['ordem_embarque'] + 1;
    //             $this->db->where('idenderecoaluno', $row['idenderecoaluno']);
    //             $this->db->update('enderecos_alunos', $dados);
    //     }
        
    // }




    //  atualizo a ordem do desembarque
    function atualiza_ordem_desembarque($idenderecoaluno, $id_usuario, $id_aluno, $desembarca_depois_idaluno){

        //  verifico se ele vai ser o primeiro aluno
        if($desembarca_depois_idaluno == 0){
            //  seta o aluno para 0 esse número é utilizado apenas para colocar em primeiro antes de ordenar novamente
            $dados['ordem_desembarque'] = 1;
            $this->db->where('idenderecoaluno', $idenderecoaluno);
            $this->db->update('enderecos_alunos', $dados);
        }else{  //  BUSCO A ORDEM DO USUARIO DEPOIS DO ALUNO PARA ORDERNAR
            $dados['ordem_desembarque'] = $this->get_ordem_embarque_desembarque($id_usuario, '', $desembarca_depois_idaluno, 'desembarque') + 1;

            //  ATUALIZO A ORDEM DO ALUNO ATUAL + 1 PRA FICAR NA ORDEM CORRERA
            $this->db->where('idenderecoaluno', $idenderecoaluno);
            $this->db->update('enderecos_alunos', $dados);
        }
        

        //  busco os alunos para atualizar a ordem do embarque retirando o atual aluno pois a ordem já foi atualizada
        $this->db->where('idenderecoaluno <> ', $idenderecoaluno);
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('ordem_desembarque >= ', $dados['ordem_desembarque']);
        $this->db->order_by('ordem_desembarque ASC');
        $query = $this->db->get('enderecos_alunos');
        
       
        //  atualizo a ordem dos alunos
        foreach ($query->result_array() as $row){
                $dados['ordem_desembarque'] = $dados['ordem_desembarque'] + 1;
                $this->db->where('idenderecoaluno', $row['idenderecoaluno']);
                $this->db->update('enderecos_alunos', $dados);
        }
        
    }




    public function get_ordem_embarque_desembarque($id_usuario, $embarca_depois_idaluno, $desembarca_depois_idaluno, $tipo_consulta){
        
        //  A CONSULTA RETORNA A ORDEM DO embarque OU desembarque DO ALUNO
        if($tipo_consulta == 'embarque'){
            $this->db->where('id_aluno', $embarca_depois_idaluno);
        }else{
            $this->db->where('id_aluno', $desembarca_depois_idaluno);
        }
        
        $this->db->where('id_usuario', $id_usuario);
        $query = $this->db->get('enderecos_alunos');
        $row = $query->row();
        if (isset($row)){

            if($tipo_consulta == 'embarque'){
                return $row->ordem_embarque;    // A ORDEM DO EMBARQUE VAI INICIAR A PARTIR DESSE ALUNO
            }else{
                return $row->ordem_desembarque;    // A ORDEM DO DESEMBARQUE VAI INICIAR A PARTIR DESSE ALUNO            
            }
            
        }else{
            return false;
        }
        
        // echo ' ========== get_ordem_embarque_desembarque ========' . $this->db->last_query();   //  exibe o sql executado

    }



    //  cadastra o registro
    public function cadastra($dados, $action){
        //$dados = $this->array_sql($dados, $action);      
        
        //  inseri no banco
        $this->db->insert('enderecos_alunos', $dados);
        $idenderecoaluno = $this->db->insert_id();

        // //  verifica se o endereço foi marcado como principal
        // if($dados['endereco_principal'] == 'SIM'){
        //     $this->atualiza_status_endereco_principal($idenderecoaluno, $dados['id_aluno']);
        // }

        // //  atualizo as ordens do embarque e desembarque
        // if(isset($dados['embarca_depois_idaluno'])){
        //     $this->atualiza_ordem_embarque($idenderecoaluno, $dados['id_usuario'], $dados['id_aluno'], $dados['embarca_depois_idaluno']);
        // }
        

        // //  atualizo as ordens dos desembarques
        // if(isset($dados['desembarca_depois_idaluno'])){
        //     $this->atualiza_ordem_desembarque($idenderecoaluno, $dados['id_usuario'], $dados['id_aluno'], $dados['desembarca_depois_idaluno']);
        // }

        return $idenderecoaluno;

    }


    //  atualiza os dados
    public function update($dados, $idenderecoaluno, $action){

        

     //   $dados = $this->array_sql($dados, $action);

        $this->db->where('idenderecoaluno', $idenderecoaluno);
        $this->db->update('enderecos_alunos', $dados);

        print_r($dados);
        exit;




        // //  verifica se o endereço foi marcado como principal
        // if($dados['endereco_principal'] == 'SIM'){
        //     $this->atualiza_status_endereco_principal($idenderecoaluno, $dados['id_aluno']);
        // }

        // //  atualizo as ordens do embarque e desembarque
        // if(isset($dados['embarca_depois_idaluno'])){
        //     $this->atualiza_ordem_embarque($idenderecoaluno, $dados['id_usuario'], $dados['id_aluno'], $dados['embarca_depois_idaluno']);
        // }
        

        // //  atualizo as ordens dos desembarques
        // if(isset($dados['desembarca_depois_idaluno'])){
        //     $this->atualiza_ordem_desembarque($idenderecoaluno, $dados['id_usuario'], $dados['id_aluno'], $dados['desembarca_depois_idaluno']);
        // }




        // $this->ajusta_ordem_rota($dados['id_usuario'], $dados['id_aluno']);
        

        // //  seta as variavies caso nao tenham sido
        // if(!isset($dados['embarca_depois_idaluno'])){
        //     $dados['embarca_depois_idaluno'] = '';
        // }

        // if(!isset($dados['desembarca_depois_idaluno'])){
        //     $dados['desembarca_depois_idaluno'] = '';
        // }


        // //  organiza a ordem da rota
        // $this->get_ordem_rota($idenderecoaluno, $dados['id_usuario'], $dados['id_aluno'], $dados['embarca_depois_idaluno'], $dados['desembarca_depois_idaluno']);


       // echo $this->db->last_query();   //  exibe o sql executado
        
    }


    //  exclui o responsavel
    public function delete($id_usuario, $idenderecoaluno){
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('idenderecoaluno', $idenderecoaluno);
        $query = $this->db->delete('enderecos_alunos');
        //echo $this->db->last_query();   //  exibe o sql executado
        return $query;
    }









    //  atualizo a ordem do embarque
    function atualiza_ordem_embarque($idenderecoaluno, $id_usuario, $id_aluno, $ordem_embarque){

            //  verifico se ele vai ser o primeiro aluno
            if($ordem_embarque == 0){
                //  seta o aluno para 0 esse número é utilizado apenas para colocar em primeiro antes de ordenar novamente
                $dados['ordem_embarque'] = 1;
                $this->db->where('idenderecoaluno', $idenderecoaluno);
                $this->db->update('enderecos_alunos', $dados);
                // echo "<br />" . $this->db->last_query();   //  exibe o sql executado
    
            }else{  //  BUSCO A ORDEM DO USUARIO DEPOIS DO ALUNO PARA ORDERNAR
                           
                // $dados['ordem_embarque'] = $this->get_ordem_embarque_desembarque($id_usuario, $ordem_embarque, '', 'embarque') + 1;
                $dados['ordem_embarque'] = $ordem_embarque + 1;
                // $dados[$rota_embarque] = $dados['ordem_embarque'];
                //  ATUALIZO A ORDEM DO ALUNO ATUAL + 1 PRA FICAR NA ORDEM CORRERA
                $this->db->where('idenderecoaluno', $idenderecoaluno);
                $this->db->update('enderecos_alunos', $dados);
            }
    
    
            //  busco os alunos para atualizar a ordem do embarque retirando o atual aluno pois a ordem já foi atualizada
            $this->db->where('idenderecoaluno <> ', $idenderecoaluno);
            $this->db->where('id_usuario', $id_usuario);
            $this->db->where('ordem_embarque >= ', $dados['ordem_embarque']);
            $this->db->order_by('ordem_embarque ASC');
            $query = $this->db->get('enderecos_alunos');
            
           
            //  atualizo a ordem dos alunos
            foreach ($query->result_array() as $row){
                    $dados['ordem_embarque'] = $dados['ordem_embarque'] + 1;
                    $this->db->where('idenderecoaluno', $row['idenderecoaluno']);
                    $this->db->update('enderecos_alunos', $dados);
            }
            
        }



    


}