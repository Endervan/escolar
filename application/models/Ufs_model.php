<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ufs_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }



    public function getUfs(){
        
        $query = $this->db->order_by('titulo', 'ASC');
        $query = $this->db->get('ufs');
        //echo $this->db->last_query();   //  exibe o sql executado
        return $query;
 

    }


    public function getCidades($iduf){
        
        $query = $this->db->where('id_uf', $iduf);
        $query = $this->db->order_by('titulo', 'ASC');
        $query = $this->db->get('cidades');
      //  echo $this->db->last_query();   //  exibe o sql executado
        return $query;
 

    }



    public function getBairros($idcidade){
        $this->db->where('id_cidade', $idcidade);
        $this->db->order_by('titulo', 'ASC');
        $query = $this->db->get('bairros');
        //echo $this->db->last_query();   //  exibe o sql executado
        return $query;
 

    }


    



}