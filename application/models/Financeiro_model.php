<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Financeiro_model extends CI_Model
{

    function __construct(){
        $this->load->database();
    }


    //  atualiza os dados
    public function update($dados, $idfluxocaixa, $action){
        $dados['valor'] = str_replace(['.', ','], ['', '.'], $dados['valor']);
        $this->db->where('idfluxocaixa', $idfluxocaixa);
        $result = $this->db->update('fluxo_caixa', $dados);
        // echo $this->db->last_query();   //  exibe o sql executado
    }


    //  cadastra o registro
    public function cadastra($dados){
        $dados['valor'] = str_replace(['.', ','], ['', '.'], $dados['valor']);
        $result = $this->db->insert('fluxo_caixa', $dados);
        $idaluno = $this->db->insert_id();
    }
       





}