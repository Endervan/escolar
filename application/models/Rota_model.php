<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rota_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }
 

    //  cadastra o registro
    public function cadastra($dados, $action){
        $this->db->insert('rotas', $dados);
        return $this->db->insert_id();
    }


    // editar  o registro
    public function update($dados, $idrota){
        $this->db->where('idrota', $idrota);
//        echo $this->db->last_query();   //  exibe o sql executado
        return $this->db->update('rotas', $dados);
    }


    //  busca a rota
    public function getRota($idrota){
        $this->db->where_in('idrota', $idrota);
        $query = $this->db->get('rotas');
        return $query->result_array();
    }


    //  atualiza a rota
    function updateRota($dados){

        
        //  verifico o tipo de inserção
        if($dados['tipo_parada'] == 'escola'){
            $this->db->where('id_escola', $dados['id_escola']);
        }else{
            $this->db->where('id_aluno', $dados['id_aluno']);
        }

        //  verifico se ja existe o cadastro na tabela
        $this->db->where('id_usuario', $dados['id_usuario']);
        $this->db->where('id_rota', $dados['id_rota']);
        $query = $this->db->get('rotas_paradas');
        $row = $query->row();
        $num_rows = $query->num_rows();

        //  verifico se tem inserido na rota
        if($num_rows > 0){  //  desativo o endereco
            
            //  verifico se devo ativar ou desativar
            if($row->ativo == true){
                $this->db->set('ativo', false);
            }else{
                $this->db->set('ativo', true);
            }            

            //  verifico o tipo de atualizacao
            if($dados['tipo_parada'] == 'escola'){
                $this->db->where('id_escola', $dados['id_escola']);
                $this->db->where('id_rota', $dados['id_rota']);
            }else{
                $this->db->where('idrotaparada', $row->idrotaparada);
            }
            
            
            $this->db->update('rotas_paradas');
            
            
            // //  verifico o tipo de inserção
            // if($dados['tipo_parada'] == 'escola'){
            //     $this->db->where('id_escola', $dados['id_escola']);
            // }else{
            //     $this->db->where('id_usuario', $dados['id_aluno']);
            // }
            // $this->db->where('id_usuario', $dados['id_usuario']);
            // $this->db->where('id_rota', $dados['id_rota']);
            // $this->db->delete('rotas_paradas');
        }else{  //  insert
            
            //  verifico o tipo de inserção
            if($dados['tipo_parada'] == 'escola'){
                $this->db->where('id_escola', $dados['id_escola']);
            }else{
                $this->db->where('id_usuario', $dados['id_aluno']);
            }
            //$dados['ordem'] = 0;    // ordem principal
            $this->db->insert('rotas_paradas', $dados);
        }

        //echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        //  reordeno tods acrescentando um número para deixa esse como 0 
        $this->reordena_paradas($dados['id_rota']);


        
        if(empty($num_rows)){
            return 0;
        }else{
            return $row->ativo;
        }
        

    }



    //  reordeno tods acrescentando um número para deixa esse como 0 
    function reordena_paradas($id_rota){

        $this->db->where('id_rota', $id_rota);
        $this->db->where('ativo', true);
        $this->db->order_by('ordem asc');
        $query = $this->db->get('rotas_paradas');

        $i = 0;
        foreach ($query->result() as $row){
            $this->db->set('ordem', $i);
            $this->db->where('idrotaparada', $row->idrotaparada);
            $this->db->update('rotas_paradas');
            $i++;
            // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        }

         

    }




    //  reordeno todas as paradas a partir de uma parada especifica
    function atualizaOrdemParada($id_rota, $from, $to, $idrotaparada){

        //  atualizo a ordem da parada pra a selecionada pelo usario
        $this->db->set('ordem', $to);
        $this->db->where('idrotaparada', $idrotaparada);
        $this->db->update('rotas_paradas');
        // echo '<pre> 1 ===='. $this->db->last_query() . '</pre>';   //  exibe o sql executado

      

        //  verifico a partir de qual ordem devo buscar
        if($to < $from){
            $this->db->where('ordem >=', $to);
            $i = $to + 1;
            // echo 'SUBI <br />';
            // echo "TO = $to <br />";
            // echo "FROM = $from <br />";
            // echo "Estava no $from irei para $to <br />";
        }else{
            $this->db->where('ordem <=', $to);
            $i = 0;
            // echo 'DESCI <br />';
            // echo "TO = $to <br />";
            // echo "FROM = $from <br />";
            // echo "Estava no $from irei para $to";
        }       
        $this->db->where('idrotaparada <>', $idrotaparada);
        $this->db->where('id_rota', $id_rota);
        $this->db->order_by('ordem asc');
        $query = $this->db->get('rotas_paradas');
        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

        
        //  AJUSTA 
        foreach ($query->result() as $row){
            $this->db->set('ordem', $i);
            $this->db->where('idrotaparada', $row->idrotaparada);
            $this->db->update('rotas_paradas');
            $i++;
            echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado
        }

        // echo '<pre>'. $this->db->last_query() . '</pre>';   //  exibe o sql executado

    }










}