<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cep_model extends CI_Model
{

    function __construct(){
        $this->load->database();
    }



    //      busca o cep no banco
    function getCep($cep){
      $this->db->where('cep', $cep);
      $query = $this->db->get('ceps');
      $result = $query->result_array();
      // echo $this->db->last_query();   //  exibe o sql executado
      //    verifico se existe no banco
      if($query->num_rows() > 0){
            return $result;
      }else{
            //    busca no via cep
            return $this->getcep_viacep($cep);
      }

    }



    function getcep_viacep($cep){
        
            $url = "http://viacep.com.br/ws". '/' . $cep . '/json/';

            // abre a conexão
            $ch = curl_init();
      
            // define url
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      
            // executa o post
            $result = json_decode(curl_exec($ch));
 

           

            if(isset($result->erro)){
                  return false;
            }else{
                  //    guardo os dados no banco
                  $dados['cep'] = str_replace("-", "", $result->cep);
                  $dados['logradouro'] = $result->logradouro;
                  $dados['complemento'] = $result->complemento;
                  $dados['bairro'] = $result->bairro;
                  $dados['localidade'] = $result->localidade;
                  $dados['uf'] = $result->uf;
                  $this->db->insert('ceps', $dados);
                  $dados['idcep'] = $this->db->insert_id();

                  if (curl_error($ch)) {
                    //    echo 'Erro:' . curl_error($ch);
                        return false;
                  }
                  return $dados;
            }
 
            // fecha a conexão
            curl_close($ch);

    }















}