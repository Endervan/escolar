<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    //  busca os dados do usuario pelo token
    function getIdUsuario($token){
        $this->db->where('token', $token);
        $row = $this->db->get('usuarios')->row_array();
        return $row;
    }


    public function getConfiguracoesUsuario($id_usuario){
        $this->db->where('id_usuario', $id_usuario);
        $row = $this->db->get('configuracoes')->row_array();
        // echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
        return $row;
    }



    function validando_login($email, $senha, $onesignail_idplayer){
        
        //  codificando a senha
        $email = strtolower (trim($email)); //  minusculas e limpando
        $senha = sha1(trim($senha));

        $this->db->where('email', $email);
        $this->db->where('senha', $senha); 
        //$this->db->where('ativo', 'sim');
        $query = $this->db->get('usuarios');
        //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
        
        if ($query->num_rows() > 0) {

            //  verifico se tem algum onesignalid cadastrado igual
            $this->verificaOnesignal($onesignail_idplayer);
            

            //  armazeno o idplayer
            $this->atualiza_onesignail_idplayer($email, $onesignail_idplayer);

            $row = $query->row();
            //print_r($row);
            return $row;
        } else {
            return false;
        }

    }



    




    //  armazena o onesignal
    function atualiza_onesignail_idplayer($email, $onesignail_idplayer){

        $data = array(
            'onesignail_idplayer' => $onesignail_idplayer
        );
    
        $this->db->where('email', $email);
        $this->db->update('usuarios', $data);
        //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
    }
    

    function insert($dados){

        //  verifico se tem algum onesignalid cadastrado igual
        $this->verificaOnesignal($dados['onesignail_idplayer']);

        $dados['senha'] = sha1(trim($dados['senha']));
        $dados['token'] = md5(uniqid(rand(), true));
        $this->db->insert('usuarios', $dados);
        $dados['idusuario'] = $this->db->insert_id();
        //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado

        //  cadastro o id do usuario na tb_configuracoes
        $dados1['id_usuario'] = $dados['idusuario'];
        $this->db->insert('configuracoes', $dados1);

        return $dados;

    }

    //  limpa qualquer igual pois o usuario pode ter alterado o celular ou pego de um conhecido
    function verificaOnesignal($onesignail_idplayer){

        if(!empty($onesignail_idplayer)){

            $this->db->where('onesignail_idplayer', $onesignail_idplayer);
            $row = $this->db->get('usuarios')->row_array();
            //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado

            if($row > 0){

                $data = array(
                    'onesignail_idplayer' => ''
                );
            
                $this->db->where('onesignail_idplayer', $onesignail_idplayer);
                $this->db->update('usuarios', $data);
                //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado

            }

        }

    }


    //  atualiza a senha
    function updatePassword($senha_anterior, $nova_senha, $idusuario){

        //  verifica senha anterior
        $senha_anterior = sha1(trim($senha_anterior));
        $dados['senha'] = sha1(trim($nova_senha));
        $this->db->where('senha', $senha_anterior);
        $this->db->where('idusuario', $idusuario);
        $query = $this->db->get('usuarios');
        // echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
        
        if ($query->num_rows() > 0) {
            $this->db->where('idusuario', $idusuario);
            $this->db->update('usuarios', $dados);
            // echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
            return true;
        }else{
            return 0;
        }

    }


    //  atualiza a senha
    function updateDadosUsuario($dados){
        
        $idusuario = $dados['idusuario'];
        unset($dados['idusuario']);
        
        $this->db->where('idusuario', $idusuario);
        $this->db->update('usuarios', $dados);
        // echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
        return true;

    }
    
    













    //funcao verifica email existente
    public function verificarEmail($email, $id_usuario = "")
    {

        if(!empty($id_usuario)){
            $this->db->where('idusuario <>', $id_usuario);    
        }

        $this->db->where('email', $email);
        $return = $this->db->get('usuarios')->row_array();
        // echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
        return $return;
    }

    //funcao verifica Telefone existente
    public function verificarTelefone($telefone)
    {
        $this->db->where('telefone', $telefone);
        return $this->db->get('usuarios')->row_array();
    }



    

    function recupera_senha($email){
        //nao efetua cadstro caso email ja exista no banco
        $rows = $this->banco->verificarEmail($email);

        if (count($rows) > 0) {
            $password = rand(100000, 999999);
            $data = array(
                'senha' => sha1($password)
            );

            $this->db->where('email', $email);
            $this->db->update('usuarios', $data);

            // $this->envia_senha_recuperada($password, $email);

            return $password;
        }else{
            return false;
        }

    }



    function envia_senha_recuperada($password, $email){

        $usuario = $this->get_dado_usuario($email);
        $message = '
                    <p>Olá '.$usuario['nome'].', </p>
                    <p>Estamos aqui pra te ajudar.</p>
                    <p>Esqueceu sua senha? Não esquenta :)</p>
                    <p>A gente cria uma pra você bem rapidinho.</p>
                    <p>Sua senha foi alterada como solicitado. </p>
                    <p>Sua Nova senha é '.$password.' </p>
                    ';


        //Load email library
        $this->load->library('email');
        $this->email->from('no-reply@cadeavan.com.br', 'Cadê a Van');
        $this->email->to($email);
        $this->email->subject('Sua senha foi alterada');
        $this->email->message($message);
        $this->email->send();
        
    }

    function get_dado_usuario($email)
    {
        $this->db->where('email', $email);
        return $this->db->get('usuarios')->row_array();
    }


    function get_usuario_id2($id){
        
        // $this->db->where('idusuario', $id);
        // $row = $this->db->get('usuarios')->row_array();
        // return $row;
        
        $this->db->where('idusuario', $id);
        $query = $this->db->get('usuarios');
        // echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado

        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $query->row_array();;
        } else{
            return array();
        }

    }

    function get_usuario_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('usuarios');
        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $data;
        } else
            return array();

    }

    function update($token){

        $dados = array(
            'nome' => $this->input->post('nome'),
            'email' => $this->input->post('email'),
            'telefone' => $this->input->post('telefone')
        );


        $this->db->where('tk', $token);
        $this->db->update('usuarios',$dados);
    }

}

?>