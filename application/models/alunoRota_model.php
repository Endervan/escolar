<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class alunoRota_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }



    //  cadastra o registro
    public function cadastra($dados, $action){
        return $this->db->insert('alunos_rotas', $dados);
    }


    //  atualiza os dados
    public function update($dados, $id_aluno, $id_usuario){

        $this->db->where('id_aluno', $id_aluno);
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->update('alunos_rotas', $dados);
       // echo $this->db->last_query();   //  exibe o sql executado
        
    }









}