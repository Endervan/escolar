<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Escola_Model extends CI_Model
{
    private $table = 'escolas';


    function __construct()
    {
        $this->load->database();

    }



    //  colunas da tabela
    public function array_sql($dados, $update = 'nao'){
        
        // $dados = array(
        //     'nome' => $dados['nome'],
        //     'endereco' => $dados['endereco'],
        //     'complemento' => $dados['complemento'],
        //     'telefone' => $dados['telefone'],
        //     'id_usuario' => $dados['id_usuario'],
        // );

        //  VERIFICO SE E PARA ATUALIZAR, CASO TRUE ADICIONO O ARRYA
        if($update == 'nao'){
            $dados['idescola'] = '';
            $array = array('idescola' => $dados['idescola']);
            array_merge($dados, $array);
        }

        return $dados;
    }


    //  cadastra o registro
    public function cadastra($dados){
        // $dados = $this->array_sql($dados);       
        return $this->db->insert($this->table, $dados);
    }

    //  deletar o registro
    public function delete($idusuario, $idescola){
    
        //  verifico se existe aluno cadastrado
        $this->db->where('id_escola', $idescola);
        $query = $this->db->get('alunos');
        $row = $query->num_rows();
        
        if($row == 0){
            $this->db->where('idescola', $idescola);
            return $this->db->delete($this->table);
        }else{
            return false;
        }

        

    }




     // editar  o registro
    public function update($dados, $idescola){
        // $dados = $this->array_sql($dados, 'sim');
        $this->db->where('idescola', $idescola);
//        echo $this->db->last_query();   //  exibe o sql executado
        return $this->db->update($this->table, $dados);
    }


}