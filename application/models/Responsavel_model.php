<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Responsavel_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }








    function validando_login($email, $senha, $onesignail_idplayer){
        
        //  codificando a senha
        $email = strtolower (trim($email)); //  minusculas e limpando
        $senha = sha1(trim($senha));

        $this->db->where('email', $email);
        $this->db->where('senha', $senha); 
        //$this->db->where('ativo', 'sim');
        $query = $this->db->get('logins_responsaveis'); 
        //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
        
        
        if ($query->num_rows() > 0) {

            //  verifico se tem algum onesignalid cadastrado igual
            $this->verificaOnesignal($onesignail_idplayer);
            

            //  armazeno o idplayer
            $this->atualiza_onesignail_idplayer($email, $onesignail_idplayer);

            $row = $query->row();
            //print_r($row);
            return $row;
        } else {
            return false;
        }

    }






    function recupera_senha($email){
        //nao efetua cadstro caso email ja exista no banco
        $rows = $this->verificarEmail($email);

        if (count($rows) > 0) {
            $password = rand(100000, 999999);
            $data = array(
                'senha' => sha1($password)
            );

            $this->db->where('email', $email);
            $this->db->update('logins_responsaveis', $data);

            $this->envia_senha_recuperada($password, $email);

            return true;
        }else{
            return false;
        }

    }

    //funcao verifica email existente
    public function verificarEmail($email)
    {
        $this->db->where('email', $email);
        return $this->db->get('logins_responsaveis')->row_array();
    }



    function envia_senha_recuperada($password, $email){

        $usuario = $this->get_dado_usuario($email);
        $message = '
                    Olá '.$usuario['nome'].', <br />
                    Sua senha foi alterada como solicitado. <br />
                    Sua Nova senha é '.$password.' 
                    ';

        //Load email library
        $this->load->library('email');
        $this->email->from('no-reply@cadeavan.com.br', 'Cadê a Van');
        $this->email->to($email);
        $this->email->subject('Sua senha foi alterada');
        $this->email->message($message);
        $this->email->send();

        /*
        $this->email->from('no-reply@cadeavan.com.br', 'Cadê a Van');
        $this->email->to($email);
        // $this->email->cc('another@another-example.com');
        // $this->email->bcc('them@their-example.com');
        $this->email->subject('Sua senha foi alterada');
        $this->email->message($message);
        $this->email->send();
        */
        
    }


    function get_dado_usuario($email)
    {
        $this->db->where('email', $email);
        return $this->db->get('logins_responsaveis')->row_array();
    }





    //  limpa qualquer igual pois o usuario pode ter alterado o celular ou pego de um conhecido
    function verificaOnesignal($onesignail_idplayer){

        if(!empty($onesignail_idplayer)){

                $data = array( 'onesignail_idplayer' => '' );
            
                $this->db->where('onesignail_idplayer', $onesignail_idplayer);
                $this->db->update('logins_responsaveis', $data);
                //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
        }

    }


    //  armazena o onesignal
    function atualiza_onesignail_idplayer($email, $onesignail_idplayer){

        $data = array(
            'onesignail_idplayer' => $onesignail_idplayer
        );
    
        $this->db->where('email', $email);
        $this->db->update('logins_responsaveis', $data);
        //echo '<pre>'.$this->db->last_query().'</pre>';   //  exibe o sql executado
    }













    //  exclui o responsavel
    public function delete($idusuario, $idresponsavel){
        $this->db->where('id_usuario', $idusuario);
        $this->db->where('idresponsavel', $idresponsavel);
        $query = $this->db->delete('responsaveis');
        //echo $this->db->last_query();   //  exibe o sql executado
        return $query;
    }



    //  colunas da tabela
    public function array_sql($dados, $action){

        $dados = array(
            'nome' => $dados['responsavel_nome'],
            'cep' => $dados['responsavel_cpf'],
            'endereco' => $dados['responsavel_endereco'],
            'complemento' => $dados['responsavel_complemento'],
            'cep' => $dados['responsavel_cep'],
            'id_usuario' => $dados['id_usuario'],
          //  'rg' => $dados['responsavel_rg'],
            'cpf' => $dados['responsavel_cpf'],
            'email' => $dados['responsavel_email'],
            'telefone_residencial' => $dados['responsavel_telefone_residencial'],
            'telefone_celular' => $dados['responsavel_telefone_celular'],
            'telefone_trabalho' => $dados['responsavel_telefone_trabalho'],
            'bairro' => $dados['bairro'],
            'cidade' => $dados['cidade'],
            'uf' => $dados['uf'],
        );

        return $dados;
    }


    //  atualiza os dados
    public function update($dados, $idresponsavel, $action){

        $dados = $this->array_sql($dados, $action);
        $this->db->where('idresponsavel', $idresponsavel);
        return $this->db->update('responsaveis', $dados);
       // echo $this->db->last_query();   //  exibe o sql executado
        
    }


    //  cadastra o registro
    public function cadastra($dados, $action)
    {
        $dados = $this->array_sql($dados, $action);

        return $this->db->insert('responsaveis', $dados);
    }


    //  cadastra na tabela de login do responsavel
    public function cadastra_login_responsavel($dados){
        //$dados = $this->array_sql($dados, $action);

        $dados['senha'] = sha1(trim($dados['senha']));
        $dados['token'] = md5(uniqid(rand(), true));
        $this->db->insert('logins_responsaveis', $dados);
        return $dados;
    }
    

    //  busca os responsaveis cadastrada pelo usuario
    public function get_responsaveis($idusuario, $idresponsavel = '')
    {

        //  verifico se e apra exibir um reponsvel
        if(!empty($idresponsavel)){
            $this->db->where('responsaveis.idresponsavel', $idresponsavel);
        }

        $this->db->select("responsaveis.idresponsavel, responsaveis.nome as responsavel_nome, responsaveis.cpf as responsavel_cpf, responsaveis.rg as responsavel_rg, responsaveis.endereco as responsavel_endereco, responsaveis.cep as responsavel_cep, responsaveis.complemento as responsavel_complemento, responsaveis.id_usuario, responsaveis.telefone_celular as responsavel_telefone_celular, responsaveis.telefone_residencial as responsavel_telefone_residencial, responsaveis.telefone_trabalho as responsavel_telefone_trabalho, responsaveis.email as responsavel_email, responsaveis.cep as responsavel_cep, responsaveis.bairro as responsavel_bairro, responsaveis.cidade as responsavel_cidade, responsaveis.uf as responsavel_uf, 
                            usuarios.nome as usuario_nome, usuarios.telefone as usuario_telefone, usuarios.email as usuario_email ");
        $this->db->from('responsaveis');
        $this->db->join('usuarios', 'responsaveis.id_usuario = usuarios.idusuario');
        $this->db->where('responsaveis.id_usuario', $idusuario);  
        $this->db->order_by('responsaveis.nome', 'asc');
        // $this->db->limit(30, $start_row); //  30 = QTD REGISTRO POR PAGINA
        $query = $this->db->get();
        //  echo $this->db->last_query();   //  exibe o sql executado
        return $query;
        
    }


    //  verifica se ja existe o cpf cadastrado para o usuario
    public function verifica_cpf_cadastro($idusuario, $cpf, $idresponsavel = ""){
        
        // verifico se a alteracao e alteracao
        if($idresponsavel != 0){
            $this->db->where("idresponsavel <>  $idresponsavel ");
        }
        
        $this->db->where("id_usuario = $idusuario AND cpf = '$cpf' ");
        $query = $this->db->get("responsaveis");
         // echo $this->db->last_query();   //  exibe o sql executado
        return $query->num_rows();
    }



    //  verifica se ja existe o cpf cadastrado para o usuario
    public function verifica_cpf_cadastro_logins_responsaveis($cpf){
        
        
        $this->db->where("cpf", $cpf);
        $query = $this->db->get("logins_responsaveis");
         // echo $this->db->last_query();   //  exibe o sql executado
        return $query->num_rows();
    }


    //  verifica se ja existe o email cadastrado para o usuario
    public function verifica_email_cadastro($idusuario, $email, $idresponsavel = ""){

        // verifico se a alteracao e alteracao
        if($idresponsavel != 0){
            $this->db->where("idresponsavel <>  $idresponsavel ");
        }

        $this->db->where("id_usuario = $idusuario AND email = '$email' ");
        $query = $this->db->get("responsaveis");
          //echo $this->db->last_query();   //  exibe o sql executado
        return $query->num_rows();
    }





}