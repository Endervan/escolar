-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 22-Jul-2019 às 21:59
-- Versão do servidor: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appescolar`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_rotas_paradas_diarias`
--

CREATE TABLE IF NOT EXISTS `tb_rotas_paradas_diarias` (
  `idrotaparadadiaria` int(11) NOT NULL,
  `id_rotaparada` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `embarque` tinyint(4) DEFAULT '1' COMMENT 'Armazena se o aluno fara o embarque',
  `hora_embarque` time DEFAULT NULL,
  `concluida` tinyint(4) DEFAULT '0' COMMENT 'Arazena se a parada foi concluída.',
  `id_usuario` int(11) NOT NULL,
  `id_faltaaluno` int(11) DEFAULT NULL,
  `aluno_embarcou` tinyint(1) DEFAULT '0' COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou',
  `responsavel_notificado` tinyint(1) DEFAULT '0',
  `aluno_embarcou_desembarcou_escola` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  ADD PRIMARY KEY (`idrotaparadadiaria`),
  ADD KEY `fk_tb_rotas_diarias_tb_rotas_paradas1_idx` (`id_rotaparada`),
  ADD KEY `fk_tb_rotas_paradas_diarias_tb_usuarios1_idx` (`id_usuario`),
  ADD KEY `fk_tb_rotas_paradas_diarias_tb_faltas_alunos1_idx` (`id_faltaaluno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  MODIFY `idrotaparadadiaria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  ADD CONSTRAINT `fk_tb_rotas_diarias_tb_rotas_paradas1` FOREIGN KEY (`id_rotaparada`) REFERENCES `tb_rotas_paradas` (`idrotaparada`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_faltas_alunos1` FOREIGN KEY (`id_faltaaluno`) REFERENCES `tb_faltas_embarques_alunos` (`idfataembraquealuno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
