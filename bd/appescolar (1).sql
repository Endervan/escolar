-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 22-Jul-2019 às 21:58
-- Versão do servidor: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appescolar`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_alunos`
--

CREATE TABLE IF NOT EXISTS `tb_alunos` (
  `idaluno` int(11) NOT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `sala` int(11) DEFAULT NULL,
  `turno` varchar(150) DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral',
  `id_responsavel` int(11) NOT NULL,
  `id_escola` int(11) NOT NULL,
  `genero` varchar(150) DEFAULT NULL,
  `foto` longtext,
  `id_usuario` int(11) NOT NULL,
  `contrato_ativo` tinyint(4) DEFAULT '1' COMMENT 'Aqui armazeno se o aluno está com o contrato ativo.',
  `transporte_ida` tinyint(4) DEFAULT '1',
  `transporte_volta` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_alunos`
--

INSERT INTO `tb_alunos` (`idaluno`, `nome`, `sala`, `turno`, `id_responsavel`, `id_escola`, `genero`, `foto`, `id_usuario`, `contrato_ativo`, `transporte_ida`, `transporte_volta`) VALUES
(1, 'Alice Riacho 2', 10, 'M', 2, 3, 'M', '', 3, 1, 1, 1),
(2, 'Jaqueson', 1, 'M', 2, 4, 'F', '', 3, 1, 1, 1),
(3, 'Talita', 5, 'M', 3, 2, 'F', '', 3, 1, 1, 1),
(6, 'Pedro 101', 2, 'M', 2, 2, 'M', NULL, 3, 1, 1, 1),
(7, 'Esthephany', 6, 'M', 3, 2, 'F', NULL, 3, 1, 1, 1),
(8, 'Ketheli ', 12, 'M', 2, 2, 'M', NULL, 3, 1, 1, 1),
(9, 'Jhonwallace', 5, 'M', 3, 2, 'F', NULL, 3, 1, 1, 1),
(13, 'Levi', 10, 'I', 2, 3, 'M', '', 3, 1, 1, 1),
(14, 'Ryan Gabriel', 10, 'M', 2, 3, 'M', '', 3, 1, 1, 1),
(15, 'Ana Paula', 5, 'M', 2, 3, 'F', '', 1, 1, 1, 1),
(16, 'Alice Quero Quero', 10, 'M', 2, 3, 'M', '', 1, 1, 1, 1),
(19, 'Maria', 10, 'M', 3, 2, 'M', NULL, 1, 1, 1, 1),
(20, 'Vitor Gabriel', 10, 'M', 2, 2, 'M', NULL, 3, 1, 1, 1),
(22, 'Raquel ', 2, 'M', 2, 3, 'M', NULL, 1, 1, 1, 1),
(23, 'Livia', 1, 'M', 2, 2, 'F', NULL, 3, 1, 1, 1),
(25, 'Vitor', 5, 'M', 3, 3, 'M', NULL, 1, 1, 1, 1),
(26, 'PEDRO RIACHO 2', 5, 'M', 2, 2, 'M', NULL, 3, 1, 1, 1),
(27, 'Fernando', 1, 'M', 2, 3, 'M', NULL, 1, 1, 1, 1),
(28, 'Eloisa', 1, 'M', 3, 3, 'M', NULL, 3, 1, 1, 1),
(29, 'Ana Ester', 8, 'M', 3, 3, 'F', NULL, 1, 1, 1, 1),
(30, 'Gabriela', 8, 'M', 2, 2, 'F', NULL, 3, 1, 1, 1),
(33, 'Vivian', NULL, 'V', 3, 2, NULL, NULL, 1, 1, 1, 1),
(34, 'Larisa', 3, 'I', 3, 3, NULL, NULL, 3, 1, 1, 1),
(35, 'Ana Lídia', 3, 'V', 3, 3, NULL, NULL, 3, 1, 1, 1),
(36, 'Kalebe', 3, 'V', 3, 3, 'M', NULL, 1, 1, 1, 1),
(37, 'Marcos Paulo', 3, 'V', 3, 3, NULL, NULL, 3, 1, 1, 1),
(38, 'Daysa', 3, 'I', 3, 3, NULL, NULL, 1, 1, 1, 1),
(39, 'Juliana', 3, 'I', 3, 3, NULL, NULL, 3, 1, 1, 1),
(40, 'Ana Júlia', NULL, 'V', 3, 3, NULL, NULL, 3, 1, 1, 1),
(41, 'Enzo', NULL, 'V', 3, 3, NULL, NULL, 3, 1, 1, 1),
(42, 'Lara Tavares', 2, 'N', 3, 3, NULL, NULL, 1, 1, 1, 1),
(43, 'alves 2000', 105, 'I', 2, 2, 'M', NULL, 1, 1, 1, 1),
(45, 'Ana Paula', 5, 'M', 2, 3, 'F', '', 1, 1, 1, 1),
(47, 'Ana Paula', 5, 'M', 2, 6, 'F', '', 1, 1, 1, 1),
(51, 'Escola 401', NULL, 'V', 2, 2, NULL, NULL, 1, 1, 1, 1),
(52, 'Marcio André da Silva', 1, 'M', 108, 5, 'M', NULL, 1, 1, 1, 1),
(53, 'Marcio André da Silva', 1, 'M', 108, 5, 'M', NULL, 1, 1, 1, 1),
(54, 'Marcio André da Silva', 1, 'M', 108, 5, 'M', NULL, 1, 1, 1, 1),
(55, 'Marcio André da Silva', 1, 'M', 108, 5, 'M', NULL, 1, 1, 1, 1),
(56, 'Marcio André da Silva', 1, 'M', 108, 5, 'M', NULL, 1, 1, 1, 1),
(57, 'Esthephany', 10, 'M', 1000, 7, 'F', NULL, 1000, 1, 1, 1),
(58, 'Ketheli', 2, 'M', 1000, 8, 'F', NULL, 1000, 1, 1, 1),
(59, 'Jhonwallace', 1, 'M', 1000, 8, 'M', NULL, 1000, 1, 1, 1),
(60, 'Talita', 2, 'M', 1000, 7, 'F', NULL, 1000, 1, 1, 1),
(61, 'Levi', 1, 'I', 1000, 9, 'M', NULL, 1000, 1, 1, 1),
(62, 'Alice 800 - Mãe', 3, 'I', 1001, 9, 'F', NULL, 1000, 1, 1, 1),
(63, 'Lolo', 10, 'I', 1000, 9, 'F', NULL, 1000, 1, 1, 1),
(64, 'Juju', 10, 'I', 1000, 9, 'F', NULL, 1000, 1, 1, 1),
(65, 'Cauã', 10, 'V', 1000, 10, 'M', NULL, 1000, 1, 1, 1),
(66, 'Hellen', 3, 'V', 1000, 8, 'F', NULL, 1000, 1, 1, 1),
(67, 'Daniela', 6, 'V', 1000, 7, 'F', NULL, 1000, 1, 1, 1),
(68, 'Lara Sophia', 11, 'V', 1000, 8, 'F', NULL, 1000, 1, 1, 1),
(69, 'Miguel José', 10, 'M', 1000, 8, 'M', NULL, 1000, 1, 1, 1),
(70, 'Samuel', 10, 'V', 1000, 11, 'M', NULL, 1000, 1, 1, 1),
(72, 'Natanael', 10, 'M', 1000, 8, 'M', NULL, 1000, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_escolas`
--

CREATE TABLE IF NOT EXISTS `tb_escolas` (
  `idescola` int(11) NOT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `localizacao_confirmada` varchar(3) DEFAULT 'NAO'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_escolas`
--

INSERT INTO `tb_escolas` (`idescola`, `nome`, `endereco`, `complemento`, `telefone`, `id_usuario`, `latitude`, `longitude`, `localizacao_confirmada`) VALUES
(1, 'ESCOLA DO SISTEMA', 'ESCOLA DO SISTEMA', NULL, NULL, 1, NULL, NULL, 'NAO'),
(2, 'Escola 401 - Alt', 'Quadra 401', '', '(61) 3025-1154', 1, NULL, NULL, 'NAO'),
(3, 'Escola 601', 'Quadra 601 ', NULL, '6135412587', 2, NULL, NULL, 'NAO'),
(4, 'Escola 803', 'Quadra 803', NULL, '6132548796', 2, NULL, NULL, 'NAO'),
(5, 'Escola Classe 803', 'Quadra 803 área especial 3', 'Lote 1 casa 2', '(61) 3425-0987', 1, '', NULL, 'NAO'),
(6, 'escola classe 104', '104', '104 01', '(61) 9999-999', 1, NULL, NULL, 'NAO'),
(7, 'Escola Classe 104', 'Quadra 104', NULL, '(61)3456-0987', 1000, NULL, NULL, 'NAO'),
(8, 'Escola Infantil 401', 'Quadra 401', NULL, '(61)3245-0938', 1000, NULL, NULL, 'NAO'),
(9, 'Creche Quero Quero', 'Quadra 407', NULL, '(61)3216-0987', 1000, NULL, NULL, 'NAO'),
(10, 'Escola Classe 206', 'Quadra 206', NULL, '(61)3245-0098', 1000, NULL, NULL, 'NAO'),
(11, 'Escola Fundamental 101', 'Quadra 101', NULL, '(61)3425-0982', 1000, NULL, NULL, 'NAO'),
(13, 'Teste', NULL, NULL, NULL, 1, NULL, NULL, 'NAO'),
(14, 'teste 01', 'rua tal que', 'csdf', '(54) 5545-454', 1000, NULL, NULL, 'NAO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_faltas_embarques_alunos`
--

CREATE TABLE IF NOT EXISTS `tb_faltas_embarques_alunos` (
  `idfataembraquealuno` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `data` varchar(150) DEFAULT NULL,
  `id_rotaparada` int(11) NOT NULL,
  `id_responsavel` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `embarque` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COMMENT='Armazena a falta ou ida do aluno informada pelo usuario ou responsavel.';

--
-- Extraindo dados da tabela `tb_faltas_embarques_alunos`
--

INSERT INTO `tb_faltas_embarques_alunos` (`idfataembraquealuno`, `id_aluno`, `data`, `id_rotaparada`, `id_responsavel`, `id_usuario`, `embarque`) VALUES
(59, 69, '2019-07-22', 323, NULL, 1000, 0),
(60, 69, '2019-07-22', 323, NULL, 1000, 1),
(61, 69, '2019-07-22', 323, NULL, 1000, 0),
(62, 69, '2019-07-22', 323, NULL, 1000, 1),
(63, 69, '2019-07-22', 323, NULL, 1000, 0),
(64, 70, '2019-07-22', 320, NULL, 1000, 0),
(65, 70, '2019-07-22', 320, NULL, 1000, 1),
(66, 69, '2019-07-22', 323, NULL, 1000, 1),
(67, 70, '2019-07-22', 320, NULL, 1000, 0),
(68, 57, '2019-07-22', 319, NULL, 1000, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins_responsaveis`
--

CREATE TABLE IF NOT EXISTS `tb_logins_responsaveis` (
  `idloginresponsavel` int(11) NOT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `nome` varchar(145) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `onesignail_idplayer` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins_responsaveis`
--

INSERT INTO `tb_logins_responsaveis` (`idloginresponsavel`, `cpf`, `nome`, `senha`, `onesignail_idplayer`, `email`) VALUES
(1, '917.639.161-20', 'Marcio André', '56879845646548921', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', 'marciomas@gmail.com'),
(2, '383.499.901.630', 'Terezinha Aparecida', '564884846', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', 'terezinhadedonno@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_notificacoes_onesignal`
--

CREATE TABLE IF NOT EXISTS `tb_notificacoes_onesignal` (
  `idnotificacaoonesignal` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `mensagem` longtext,
  `player_ids` varchar(245) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_notificacoes_onesignal`
--

INSERT INTO `tb_notificacoes_onesignal` (`idnotificacaoonesignal`, `titulo`, `mensagem`, `player_ids`, `datetime`) VALUES
(10, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:41:50'),
(11, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:41:50'),
(12, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Juju. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:41:50'),
(13, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:41:51'),
(14, 'Iniciando nosso itinerário.', 'Boa tarde, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-16 14:41:52'),
(15, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:48'),
(16, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:49'),
(17, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:50'),
(18, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:52'),
(19, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lolo. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:52'),
(20, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:53'),
(21, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:56'),
(22, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:57'),
(23, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:42:57'),
(24, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:43:01'),
(25, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Juju. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:43:02'),
(26, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:43:03'),
(27, 'Iniciando nosso itinerário.', 'Boa tarde, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-16 14:43:04'),
(28, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:43:23'),
(29, 'Falta no embarque de Talita', 'Talita não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:43:38'),
(30, 'Falta no embarque de Jhonwallace', 'Jhonwallace não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:44:30'),
(31, 'Embarque de Natanael', 'Natanael já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 14:44:41'),
(32, 'Embarque de Cauã', 'Cauã já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 15:32:42'),
(33, 'Embarque de Lolo', 'Lolo já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 15:33:10'),
(34, 'Falta no embarque de Hellen', 'Hellen não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:50:00'),
(35, 'Falta no embarque de Hellen', 'Hellen não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:50:01'),
(36, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:50:07'),
(37, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:50:08'),
(38, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:50:15'),
(39, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:50:18'),
(40, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:51:27'),
(41, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:51:28'),
(42, 'Falta no embarque de Juju', 'Juju não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:52:48'),
(43, 'Falta no embarque de Juju', 'Juju não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:53:20'),
(44, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:53:56'),
(45, 'Embarque de Samuel', 'Samuel já está a bordo de nosso veículo, em breve chegaremos a Escola Fundamental 101.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:56:41'),
(46, 'Falta no embarque de Alice 800 - Mãe', 'Alice 800 - Mãe não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-16 17:56:54'),
(47, 'Embarque de Esthephany', 'Esthephany já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:57:55'),
(48, 'Falta no embarque de Ketheli', 'Ketheli não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:57:59'),
(49, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:36'),
(50, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:37'),
(51, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:38'),
(52, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:38'),
(53, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lolo. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:39'),
(54, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:39'),
(55, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:40'),
(56, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:40'),
(57, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:41'),
(58, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:41'),
(59, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Juju. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:41'),
(60, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 17:59:42'),
(61, 'Iniciando nosso itinerário.', 'Boa tarde, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-16 17:59:42'),
(62, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:01:58'),
(63, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:41'),
(64, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:41'),
(65, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:42'),
(66, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:42'),
(67, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lolo. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:43'),
(68, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:43'),
(69, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:44'),
(70, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:44'),
(71, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:45'),
(72, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:45'),
(73, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Juju. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:46'),
(74, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-16 18:02:46'),
(75, 'Iniciando nosso itinerário.', 'Boa noite, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-16 18:02:47'),
(76, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:39'),
(77, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:40'),
(78, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:40'),
(79, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:41'),
(80, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lolo. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:41'),
(81, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:42'),
(82, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:42'),
(83, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:43'),
(84, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:43'),
(85, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:44'),
(86, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Juju. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:44'),
(87, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 03:01:44'),
(88, 'Iniciando nosso itinerário.', 'Bom dia, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 03:01:45'),
(89, 'Embarque de ', ' já está a bordo de nosso veículo, em breve chegaremos a .', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 15:06:48'),
(90, 'Chegada ao destino', 'Já estamos na Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 15:28:50'),
(91, 'Chegada ao destino', 'Já estamos na Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 15:29:05'),
(92, 'Chegada ao destino', 'Já estamos na Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 15:29:18'),
(93, 'Chegada ao destino', 'Já estamos à Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:32:47'),
(94, 'Chegada ao destino', 'Já chegamos à Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:33:41'),
(95, 'Chegada ao destino', 'Já chegamos à Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:33:49'),
(96, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:37:54'),
(97, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:37:56'),
(98, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 15:42:29'),
(99, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:42:32'),
(100, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 15:42:34'),
(101, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:42:41'),
(102, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 15:42:43'),
(103, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 15:43:22'),
(104, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:00:04'),
(105, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:00:07'),
(106, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:00:10'),
(107, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:00:59'),
(108, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:01:03'),
(109, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:01:28'),
(110, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:03:03'),
(111, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:03:34'),
(112, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:07:35'),
(113, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:09:59'),
(114, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:10:03'),
(115, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:14:14'),
(116, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:14:28'),
(117, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:19:54'),
(118, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:20:07'),
(119, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:24:26'),
(120, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:24:28'),
(121, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:27:23'),
(122, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:27:25'),
(123, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:27:28'),
(124, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:27:30'),
(125, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:27:32'),
(126, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:27:36'),
(127, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:27:40'),
(128, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:29:31'),
(129, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:29:34'),
(130, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:29:35'),
(131, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:31:35'),
(132, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:31:37'),
(133, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:32:03'),
(134, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:32:04'),
(135, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:32:06'),
(136, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:32:07'),
(137, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:32:08'),
(138, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:32:21'),
(139, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:32:23'),
(140, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:33:17'),
(141, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:33:19'),
(142, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:36:29'),
(143, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 17:36:30'),
(144, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:36:31'),
(145, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:36:33'),
(146, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:37:26'),
(147, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:37:27'),
(148, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:59:44'),
(149, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 17:59:59'),
(150, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 18:00:08'),
(151, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:01:08'),
(152, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:01:12'),
(153, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:01:15'),
(154, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 18:01:17'),
(155, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:01:26'),
(156, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:12:18'),
(157, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:12:20'),
(158, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:12:22'),
(159, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:17:08'),
(160, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:17:10'),
(161, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:17:12'),
(162, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:17:13'),
(163, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:17:15'),
(164, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:17:17'),
(165, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:22:24'),
(166, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 18:56:23'),
(167, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:56:26'),
(168, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:56:53'),
(169, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:57:00'),
(170, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:57:02'),
(171, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 18:57:05'),
(172, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:59:49'),
(173, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 18:59:53'),
(174, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:00:01'),
(175, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:05:37'),
(176, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:05:48'),
(177, 'Chegada ao destino', 'Já chegamos à Creche Quero Quero.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:08:39'),
(178, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:17'),
(179, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:18'),
(180, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:18'),
(181, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:19'),
(182, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:19'),
(183, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:20'),
(184, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:20'),
(185, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:20'),
(186, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:21'),
(187, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:24:21'),
(188, 'Iniciando nosso itinerário.', 'Boa noite, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:24:22'),
(189, 'Chegada ao destino', 'Já chegamos à Escola 601.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:34:35'),
(190, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:36:36'),
(191, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:41'),
(192, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:42'),
(193, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:42'),
(194, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:43'),
(195, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:43'),
(196, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:44'),
(197, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:44'),
(198, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:45'),
(199, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:45'),
(200, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:46'),
(201, 'Iniciando nosso itinerário.', 'Boa noite, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:37:46'),
(202, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:37:54'),
(203, 'Chegada ao destino', 'Já chegamos à Escola 601.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:38:02'),
(204, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:39:34'),
(205, 'Chegada ao destino', 'Já chegamos à Escola 601.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:39:41'),
(206, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:44'),
(207, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:45'),
(208, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:45'),
(209, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:46'),
(210, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:46'),
(211, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:47'),
(212, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:47'),
(213, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:48'),
(214, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:48'),
(215, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:40:49'),
(216, 'Iniciando nosso itinerário.', 'Boa noite, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:40:49'),
(217, 'Chegada ao destino', 'Já chegamos à Escola 601.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:40:58'),
(218, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:46:38'),
(219, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:29'),
(220, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:29'),
(221, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:30'),
(222, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:30'),
(223, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:31'),
(224, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:31'),
(225, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:32'),
(226, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:32'),
(227, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:33'),
(228, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:49:33'),
(229, 'Iniciando nosso itinerário.', 'Boa noite, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:49:34'),
(230, 'Chegada ao destino', 'Já chegamos à Escola 601.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:49:46'),
(231, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 19:50:37'),
(232, 'Chegada ao destino', 'Já chegamos à Escola 601.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 19:51:30'),
(233, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:00:00'),
(234, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:00:41'),
(235, 'Chegada ao destino', 'Já chegamos à Escola 601.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 20:00:47'),
(236, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 20:00:51'),
(237, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:01:21'),
(238, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 20:01:23'),
(239, 'Chegada ao destino', 'Já chegamos à ESCOLA DO SISTEMA.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:01:29'),
(240, 'Chegada ao destino', 'Já chegamos à Escola 601.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:01:31'),
(241, 'Chegada ao destino', 'Já chegamos à Escola Fundamental 101.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:01:33'),
(242, 'Chegada ao destino', 'Já chegamos à Escola 401 - Alt.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:01:34'),
(243, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 20:01:37'),
(244, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-17 20:02:11'),
(245, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-17 20:02:14'),
(246, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:07'),
(247, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:08'),
(248, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:08'),
(249, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:09');
INSERT INTO `tb_notificacoes_onesignal` (`idnotificacaoonesignal`, `titulo`, `mensagem`, `player_ids`, `datetime`) VALUES
(250, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:10'),
(251, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:10'),
(252, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:11'),
(253, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:11'),
(254, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:12'),
(255, 'Iniciando nosso itinerário.', 'Bom dia, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:12'),
(256, 'Iniciando nosso itinerário.', 'Bom dia, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice 800 - Mãe. Caso o aluno não irá utilizar o transporte por favor nos avise.', '8cd872f4-eab3-4bbb-98b4-4357944e0e0b', '2019-07-18 04:29:13'),
(257, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:29:46'),
(258, 'Falta no embarque de Talita', 'Talita não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:30:31'),
(259, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:30:57'),
(260, 'Falta no embarque de Natanael', 'Natanael não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:33:35'),
(261, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:33:40'),
(262, 'Falta no embarque de Esthephany', 'Esthephany não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:34:18'),
(263, 'Embarque de Ketheli', 'Ketheli já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:34:26'),
(264, 'Embarque de Cauã', 'Cauã já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:34:31'),
(265, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 04:35:05'),
(266, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 17:53:38'),
(267, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 17:53:38'),
(268, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 17:53:38'),
(269, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 17:54:54'),
(270, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 18:19:26'),
(271, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 18:28:29'),
(272, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:03'),
(273, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:30'),
(274, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:31'),
(275, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:31'),
(276, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:31'),
(277, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:32'),
(278, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:32'),
(279, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:33'),
(280, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:34'),
(281, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:34'),
(282, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:02:35'),
(283, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:06:04'),
(284, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:06:10'),
(285, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:09:40'),
(286, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:09:56'),
(287, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:10:29'),
(288, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:11:05'),
(289, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:11:07'),
(290, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:11:38'),
(291, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:11:41'),
(292, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:13:45'),
(293, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:13:51'),
(294, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:14:21'),
(295, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:14:32'),
(296, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:14:48'),
(297, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:15:10'),
(298, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:17:09'),
(299, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:21:53'),
(300, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:37:00'),
(301, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:39:35'),
(302, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:41:18'),
(303, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:44:48'),
(304, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Natanael.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:44:48'),
(305, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:45:49'),
(306, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Natanael.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:45:50'),
(307, 'Embarque de Natanael', 'Natanael já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:47:19'),
(308, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Lolo.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:47:19'),
(309, 'Falta no embarque de Hellen', 'Hellen não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:49:49'),
(310, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Esthephany.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:49:49'),
(311, 'Embarque de Esthephany', 'Esthephany já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:52:16'),
(312, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado, em breve chegaremos para buscar o aluno(a) Ketheli.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-18 19:52:17'),
(313, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:50'),
(314, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:51'),
(315, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:51'),
(316, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:52'),
(317, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:52'),
(318, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:53'),
(319, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:53'),
(320, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:54'),
(321, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:55'),
(322, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-19 17:42:55'),
(323, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:11'),
(324, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:12'),
(325, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:12'),
(326, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:13'),
(327, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:13'),
(328, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:14'),
(329, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:14'),
(330, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:15'),
(331, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:15'),
(332, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 12:28:16'),
(333, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 16:29:21'),
(334, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 16:29:22'),
(335, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 16:29:22'),
(336, 'Iniciando nosso itinerário.', 'Boa tarde, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Daniela. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 16:29:23'),
(337, 'Chegada ao destino', 'Já chegamos à escola classe 104.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 18:31:00'),
(338, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 18:31:06'),
(339, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 18:31:38'),
(340, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 18:31:38'),
(341, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 18:31:39'),
(342, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Daniela. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 18:31:39'),
(343, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 18:31:43'),
(344, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:12:09'),
(345, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:39'),
(346, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:40'),
(347, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lolo. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:41'),
(348, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Levi. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:41'),
(349, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:41'),
(350, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:42'),
(351, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Juju. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:42'),
(352, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:43'),
(353, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:43'),
(354, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:21:44'),
(355, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Talita. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:39'),
(356, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Natanael. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:39'),
(357, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lolo. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:40'),
(358, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Levi. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:41'),
(359, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Lara Sophia. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:41'),
(360, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Ketheli. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:42'),
(361, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Juju. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:42'),
(362, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Jhonwallace. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:42'),
(363, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Hellen. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:43'),
(364, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:44'),
(365, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Miguel José. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:44'),
(366, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:45'),
(367, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Esthephany. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:45'),
(368, 'Iniciando nosso itinerário.', 'Boa noite, Maria de Paula estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Daniela. Caso o aluno não irá utilizar o transporte por favor nos avise.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:33:46'),
(369, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:34:10'),
(370, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:34:57'),
(371, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:35:11'),
(372, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:35:12'),
(373, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:36:54'),
(374, 'Chegada ao destino', 'Já chegamos à Escola 401 - Alt.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:39:54'),
(375, 'Chegada ao destino', 'Já chegamos à Escola 601.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:40:37'),
(376, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:44:16'),
(377, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:57:10'),
(378, 'Chegada ao destino', 'Já chegamos à ESCOLA DO SISTEMA.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 20:58:19'),
(379, 'Chegada ao destino', 'Já chegamos à ESCOLA DO SISTEMA.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:00:09'),
(380, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:05:01'),
(381, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:17:39'),
(382, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:18:44'),
(383, 'Chegada ao destino', 'Já chegamos à Escola 401 - Alt.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:30:46'),
(384, 'Chegada ao destino', 'Já chegamos à Escola Fundamental 101.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:31:56'),
(385, 'Chegada ao destino', 'Já chegamos à ESCOLA DO SISTEMA.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:33:06'),
(386, 'Chegada ao destino', 'Já chegamos à Escola 601.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:33:46'),
(387, 'Chegada ao destino', 'Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:33:57'),
(388, 'Chegada ao destino', 'Marcio André Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:35:15'),
(389, 'Chegada ao destino', 'Marcio André Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:35:59'),
(390, 'Chegada ao destino', 'Marcio André Já chegamos à Escola Classe 206.', '398d7c3e-9189-412c-b4b4-a054ba8aebc8', '2019-07-22 21:50:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_notificacoes_responsaveis`
--

CREATE TABLE IF NOT EXISTS `tb_notificacoes_responsaveis` (
  `idnotificacaoresponsavel` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `mensagem` longtext,
  `data` datetime DEFAULT NULL,
  `id_responsavel` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_notificacoes_responsaveis`
--

INSERT INTO `tb_notificacoes_responsaveis` (`idnotificacaoresponsavel`, `titulo`, `mensagem`, `data`, `id_responsavel`) VALUES
(1, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(2, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(3, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(4, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(5, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(6, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(7, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(8, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(9, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(10, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(11, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(12, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(13, 'Embarque de Juju', 'Juju já está a bordo do nosso veículo, estamos indo em direção a Creche Quero Quero.', NULL, 1000),
(14, 'Embarque de Juju', 'Juju já está a bordo do nosso veículo, estamos indo em direção a Creche Quero Quero.', NULL, 1000),
(15, 'Embarque de Samuel', 'Samuel já está a bordo do nosso veículo, estamos indo em direção a Escola Fundamental 101.', NULL, 1000),
(16, 'Embarque de Samuel', 'Samuel já está a bordo do nosso veículo, estamos indo em direção a Escola Fundamental 101.', NULL, 1000),
(17, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(18, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(19, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(20, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(21, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(22, 'Embarque de Alice 800', 'Alice 800 já está a bordo do nosso veículo, estamos indo em direção a Creche Quero Quero.', NULL, 1000),
(23, 'Embarque de Alice 800', 'Alice 800 já está a bordo do nosso veículo, estamos indo em direção a Creche Quero Quero.', NULL, 1000),
(24, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(25, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(26, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(27, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(28, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(29, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(30, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(31, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(32, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(33, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(34, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(35, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(36, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(37, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(38, 'Embarque de Esthephany', 'Esthephany já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 104.', NULL, 1000),
(39, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(40, 'Embarque de Ketheli', 'Ketheli já está a bordo do nosso veículo, estamos indo em direção a Escola Infantil 401.', NULL, 1000),
(41, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(42, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(43, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(44, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(45, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(46, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, avisaremos assim que chegarmos a Escola Classe 206.', NULL, 1000),
(47, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, estamos indo em direção a Escola Classe 206.', NULL, 1000),
(48, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, logo chegaremos a Escola Classe 206.', NULL, 1000),
(49, 'Embarque de Cauã', 'Cauã já está a bordo do nosso veículo, em breve chegaremos a Escola Classe 206.', NULL, 1000),
(50, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(51, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(52, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(53, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(54, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(55, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(56, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(57, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(58, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(59, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(60, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(61, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(62, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(63, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(64, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(65, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(66, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(67, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(68, 'Falta no embarque de Juju', 'Juju não estava no local de embarque, informamos que demos continuidado ao próximo aluno.', NULL, 1000),
(69, 'Falta no embarque de Juju', 'Juju não estava no local de embarque, informamos que demos continuidado ao próximo aluno.', NULL, 1000),
(70, 'Falta no embarque de Miguel José', 'Miguel José não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(71, 'Falta no embarque de Miguel José', 'Miguel José não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(72, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(73, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(74, 'Embarque de Natanael', 'Natanael já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(75, 'Embarque de Natanael', 'Natanael já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(76, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(77, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(78, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1001),
(79, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1001),
(80, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(81, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1001),
(82, 'Falta no embarque de Alice 800 - Mãe', 'Alice 800 - Mãe não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1001),
(83, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(84, 'Falta no embarque de Talita', 'Talita não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(85, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(86, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(87, 'Falta no embarque de Talita', 'Talita não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(88, 'Falta no embarque de Jhonwallace', 'Jhonwallace não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(89, 'Embarque de Natanael', 'Natanael já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(90, 'Embarque de Cauã', 'Cauã já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 206.', NULL, 1000),
(91, 'Embarque de Lolo', 'Lolo já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(92, 'Falta no embarque de Hellen', 'Hellen não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(93, 'Falta no embarque de Hellen', 'Hellen não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(94, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(95, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(96, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(97, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(98, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(99, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(100, 'Falta no embarque de Juju', 'Juju não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(101, 'Falta no embarque de Juju', 'Juju não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(102, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(103, 'Falta no embarque de Juju', 'Juju não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(104, 'Embarque de Juju', 'Juju já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1000),
(105, 'Embarque de Samuel', 'Samuel já está a bordo de nosso veículo, em breve chegaremos a Escola Fundamental 101.', NULL, 1000),
(106, 'Falta no embarque de Alice 800 - Mãe', 'Alice 800 - Mãe não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1001),
(107, 'Embarque de Esthephany', 'Esthephany já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(108, 'Falta no embarque de Ketheli', 'Ketheli não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(109, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(110, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(111, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(112, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(113, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(114, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(115, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(116, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1001),
(117, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1001),
(118, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1001),
(119, 'Embarque de Alice 800 - Mãe', 'Alice 800 - Mãe já está a bordo de nosso veículo, em breve chegaremos a Creche Quero Quero.', NULL, 1001),
(120, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(121, 'Falta no embarque de Talita', 'Talita não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(122, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(123, 'Falta no embarque de Natanael', 'Natanael não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(124, 'Falta no embarque de Esthephany', 'Esthephany não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(125, 'Embarque de Ketheli', 'Ketheli já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(126, 'Embarque de Cauã', 'Cauã já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 206.', NULL, 1000),
(127, 'Embarque de Lara Sophia', 'Lara Sophia já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(128, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(129, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(130, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(131, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(132, 'Embarque de Hellen', 'Hellen já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(133, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(134, 'Falta no embarque de Cauã', 'Cauã não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(135, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(136, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(137, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(138, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(139, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(140, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(141, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(142, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(143, 'Embarque de Miguel José', 'Miguel José já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(144, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(145, 'Embarque de Talita', 'Talita já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(146, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(147, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(148, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(149, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(150, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(151, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(152, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(153, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(154, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(155, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(156, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Natanael.', NULL, 1000),
(157, 'Embarque de Jhonwallace', 'Jhonwallace já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(158, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Natanael.', NULL, 1000),
(159, 'Embarque de Natanael', 'Natanael já está a bordo de nosso veículo, em breve chegaremos a Escola Infantil 401.', NULL, 1000),
(160, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Lolo.', NULL, 1000),
(161, 'Falta no embarque de Hellen', 'Hellen não estava no local de embarque, informamos que demos continuidado a nosso itinerário.', NULL, 1000),
(162, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado em breve chegaremos para buscar o aluno Esthephany.', NULL, 1000),
(163, 'Embarque de Esthephany', 'Esthephany já está a bordo de nosso veículo, em breve chegaremos a Escola Classe 104.', NULL, 1000),
(164, 'Aviso de embarque', 'Marcio André já estamos a caminho do seu local de embarque, por favor nos aguardo no local combinado, em breve chegaremos para buscar o aluno(a) Ketheli.', NULL, 1000);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_responsaveis`
--

CREATE TABLE IF NOT EXISTS `tb_responsaveis` (
  `idresponsavel` int(11) NOT NULL COMMENT 'Aqui é o responsável pelo aluno, pode ser a mãe, tio e quem assinou o contrato foi outra pessoa por exemplo como o pai.',
  `nome` varchar(150) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `rg` int(11) DEFAULT NULL,
  `telefone_residencial` varchar(15) DEFAULT NULL,
  `telefone_celular` varchar(15) DEFAULT NULL,
  `telefone_trabalho` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `bairro` varchar(150) DEFAULT NULL,
  `cidade` varchar(150) DEFAULT NULL,
  `uf` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_responsaveis`
--

INSERT INTO `tb_responsaveis` (`idresponsavel`, `nome`, `cpf`, `endereco`, `complemento`, `id_usuario`, `rg`, `telefone_residencial`, `telefone_celular`, `telefone_trabalho`, `email`, `senha`, `cep`, `bairro`, `cidade`, `uf`) VALUES
(1, 'REPONSÁVEL DO SISTEMA', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Maria Paula - Teste', '98741251420', 'Quadra 101 lote 20', 'Apt 201', 1, 545674856, '32415698', '98745-6321', '3241-9854', 'mariapaula@email.com', NULL, '72541-748', NULL, NULL, NULL),
(3, 'Marcos Paulo - Teste', '85412365470', 'Quadra 202 conjunto 3 lote 15', 'Casa 2', 2, 465456487, '32149852', '96547-9854', '3014-8784', 'marcospaulo@email.com', NULL, '72610-417', NULL, NULL, NULL),
(7, 'Wenia Maria', '98764987123', 'Quadra 104 conjunto 11 lote 10', 'casa 2', 1, 1952145, '32541254', '987640123', '33256844', 'weniamaria@email.com', NULL, NULL, NULL, NULL, NULL),
(47, 'Marcio André da Silva 1', '65489795', 'Qd 201 lote 29', NULL, 1, NULL, NULL, '98645689', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Marcio André da Silva2', '654897951', 'Qd 201 lote 29', '', 1, 0, '', '98645689', '', 'mar1@gmail.com', NULL, NULL, NULL, NULL, NULL),
(49, 'Marcio André da Silva3', '38349990163', 'Qd 201 lote 29', '', 1, 0, '', '98645689', '', 'mar3@gmail.com', NULL, NULL, NULL, NULL, NULL),
(50, 'Marcio André da Silva4', '878978989897', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'mar4@gmail.com', NULL, NULL, NULL, NULL, NULL),
(51, 'Marcio André da Silva', '8789789898973', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'mar24@gmail.com', NULL, NULL, NULL, NULL, NULL),
(52, 'Marcio André da Silva', '82789789898973', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'mar224@gmail.com', NULL, NULL, NULL, NULL, NULL),
(53, 'Marcio André da Silva', '827893898973', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm22224@gmail.com', NULL, NULL, NULL, NULL, NULL),
(54, 'Marcio André da Silva', '827892398973', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm45224@gmail.com', NULL, NULL, NULL, NULL, NULL),
(55, 'Marcio André da Silva', '823398973', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm4534@gmail.com', NULL, NULL, NULL, NULL, NULL),
(56, 'Marcio André da Silva', '8233985973', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm45354@gmail.com', NULL, NULL, NULL, NULL, NULL),
(57, 'Marcio André da Silva', '82339859732', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm453534@gmail.com', NULL, NULL, NULL, NULL, NULL),
(58, ' Super Mário', '823339859732', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm4535334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(59, ' Super Mário', '823333', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm454334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(61, ' Super Mário', '822331233333', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm44534254334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(64, 'Super Mário', '823123333333', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm33422544334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(65, ' Super Mário', '11111113', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm334232544334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(66, ' Super Mário', '11111114', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm5232544334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(67, ' Super Mário', '82312343333', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm5232544334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(68, ' Super Mário', '82312343333', 'Qd 201 lote 29', 'Casa 2', 1, 2147483647, '30261474', '98645689', '35649874', 'm5232544334@gmail.com', NULL, NULL, NULL, NULL, NULL),
(69, 'Ana Clara Braga', '4564564564', 'Qd 200 lote 10', NULL, 2, NULL, NULL, '(82) 7489-2303', NULL, 'anacl@gmail.com', NULL, NULL, NULL, NULL, NULL),
(71, 'pedro paulo', '897483972', 'Qd 20', 'lote 1', 2, 897234, NULL, '(89) 78923-7488', NULL, 'pasoipup@iouasdi.combr', NULL, NULL, NULL, NULL, NULL),
(97, 'Marcio Postmans 1010 - 4040 - ABC - JJJJ', '38349990165', '18879456', 'lote 1', 1, 18879456, '(61)3098-2231', '(61)98745-2231', '(61)3098-3333', 'cad02@gmail.com', NULL, '383499901', NULL, NULL, NULL),
(108, 'Cadastro 01 - ALT - 01999999', '111111151', '18879456', 'lote 1', 1, 18879456, '(61)3098-2231', '(61)98745-2231', '(61)3098-3333', 'cad0w3@gmail.com', NULL, '111111151', NULL, NULL, NULL),
(110, 'Cadastro 01', '11111111', '18879456', 'lote 1', 1, 18879456, '(61)3098-2231', '(61)98745-2231', '(61)3098-3333', 'cad01@gmail.com', NULL, '72610417', NULL, NULL, NULL),
(111, 'Cadastro 01', '111111311', '18879456', 'lote 1', 1, 18879456, '(61)3098-2231', '(61)98745-2231', '(61)3098-3333', 'card01@gmail.com', NULL, '72610417', NULL, NULL, NULL),
(112, 'ender01', '001.745.013-35', '1060640', 'quadra 25', 1, 1060640, '0613303353', '061 99999-99999', '0613303353', 'endvan@gmail.com', NULL, NULL, NULL, NULL, NULL),
(1000, 'Maria de Paula', '917.639.161-20', 'Quadra 202 conjunto 10', 'lote 10', 1000, 1984321, '(61)3245-6584', '(61)98547-9658', '(61)3541-9874', 'suporte@masmidia.com.br', NULL, '72154-874', 'Recanto das Emas', 'Brasília', 'DF'),
(1001, 'Terezinha Aparecida Dedonno', '383.499.901.63', 'Quadra 204 Conjunto 17', '8', 1000, NULL, NULL, '(61)98547-9655', NULL, '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_rotas`
--

CREATE TABLE IF NOT EXISTS `tb_rotas` (
  `idrota` int(11) NOT NULL,
  `titulo` varchar(150) DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `id_veiculo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_rotas`
--

INSERT INTO `tb_rotas` (`idrota`, `titulo`, `hora_inicio`, `id_veiculo`, `id_usuario`) VALUES
(1, 'Taguatinga Manhã', '06:00:10', 1, 1),
(2, 'Ceilandia', '08:00:00', 2, 1),
(3, 'Matutino', '06:00:00', 2, 1000),
(4, 'Vespertino', '05:45:00', 8, 1000),
(5, 'Noturno', '18:00:00', 2, 1000),
(6, 'Tewste 1111', '06:00:00', 8, 1000);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_rotas_paradas`
--

CREATE TABLE IF NOT EXISTS `tb_rotas_paradas` (
  `idrotaparada` int(11) NOT NULL,
  `tipo_parada` varchar(150) DEFAULT NULL COMMENT 'A parada pode ser do tipo, ALUNO ou ESCOLA',
  `id_rota` int(11) NOT NULL,
  `id_escola` int(11) DEFAULT NULL,
  `id_aluno` int(11) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `localizacao_confirmada` tinyint(4) DEFAULT '0',
  `id_usuario` int(11) NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_rotas_paradas`
--

INSERT INTO `tb_rotas_paradas` (`idrotaparada`, `tipo_parada`, `id_rota`, `id_escola`, `id_aluno`, `latitude`, `longitude`, `localizacao_confirmada`, `id_usuario`, `ordem`, `ativo`) VALUES
(314, 'escola', 3, 7, NULL, NULL, NULL, 0, 1000, 17, 1),
(316, 'escola', 3, 11, NULL, NULL, NULL, 0, 1000, 13, 1),
(317, 'escola', 3, 11, NULL, NULL, NULL, 0, 1000, 20, 1),
(318, 'aluno', 3, NULL, 67, NULL, NULL, 0, 1000, 19, 1),
(319, 'aluno', 3, NULL, 57, NULL, NULL, 0, 1000, 18, 1),
(320, 'aluno', 3, NULL, 70, NULL, NULL, 0, 1000, 16, 1),
(322, 'escola', 3, 9, NULL, NULL, NULL, 0, 1000, 15, 1),
(323, 'aluno', 3, NULL, 69, NULL, NULL, 0, 1000, 14, 1),
(325, 'escola', 3, 10, NULL, NULL, NULL, 0, 1000, 12, 1),
(328, 'aluno', 3, NULL, 65, NULL, NULL, 0, 1000, 11, 1),
(329, 'aluno', 3, NULL, 66, NULL, NULL, 0, 1000, 10, 1),
(330, 'aluno', 3, NULL, 59, NULL, NULL, 0, 1000, 9, 1),
(331, 'aluno', 3, NULL, 64, NULL, NULL, 0, 1000, 7, 1),
(332, 'aluno', 3, NULL, 58, NULL, NULL, 0, 1000, 6, 1),
(333, 'aluno', 3, NULL, 68, NULL, NULL, 0, 1000, 5, 1),
(334, 'aluno', 3, NULL, 61, NULL, NULL, 0, 1000, 3, 1),
(335, 'aluno', 3, NULL, 63, NULL, NULL, 0, 1000, 2, 1),
(336, 'aluno', 3, NULL, 72, NULL, NULL, 0, 1000, 1, 1),
(337, 'aluno', 3, NULL, 60, NULL, NULL, 0, 1000, 0, 1),
(338, 'escola', 3, 8, NULL, NULL, NULL, 0, 1000, 4, 1),
(339, 'escola', 3, 8, NULL, NULL, NULL, 0, 1000, 8, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_rotas_paradas_diarias`
--

CREATE TABLE IF NOT EXISTS `tb_rotas_paradas_diarias` (
  `idrotaparadadiaria` int(11) NOT NULL,
  `id_rotaparada` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `embarque` tinyint(4) DEFAULT '1' COMMENT 'Armazena se o aluno fara o embarque',
  `hora_embarque` time DEFAULT NULL,
  `concluida` tinyint(4) DEFAULT '0' COMMENT 'Arazena se a parada foi concluída.',
  `id_usuario` int(11) NOT NULL,
  `id_faltaaluno` int(11) DEFAULT NULL,
  `aluno_embarcou` tinyint(1) DEFAULT '0' COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou',
  `responsavel_notificado` tinyint(1) DEFAULT '0',
  `aluno_embarcou_desembarcou_escola` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuarios`
--

CREATE TABLE IF NOT EXISTS `tb_usuarios` (
  `idusuario` int(11) NOT NULL COMMENT 'Pessoa responsável pelo contrato, pode ser uma empresa ou um pai, tio, avô e etc.',
  `nome` varchar(150) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `senha` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`idusuario`, `nome`, `telefone`, `email`, `senha`) VALUES
(1, 'Usuário Teste', '6198606484', 'usuarioteste@gmail.com', '123456'),
(2, 'Ana Paula', '6187459857', 'anapaula@masmidia.net', '589874'),
(3, 'usuário teste', '61986066481', 'teste@gmail.com', '123456'),
(1000, 'Marcio André da Silva', '61986066488', 'marciomas@gmail.com', '89987');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_veiculos`
--

CREATE TABLE IF NOT EXISTS `tb_veiculos` (
  `idveiculo` int(11) NOT NULL,
  `placa` varchar(8) DEFAULT NULL,
  `marca` varchar(150) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `modelo` varchar(150) DEFAULT NULL,
  `cor` varchar(150) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_veiculos`
--

INSERT INTO `tb_veiculos` (`idveiculo`, `placa`, `marca`, `ano`, `modelo`, `cor`, `ativo`, `id_usuario`) VALUES
(1, 'PAI-1512', 'Renault Clio', 2015, 'Clio', 'Branca', 'SIM', 1),
(2, 'PAO-0999', 'Mercedes', 2018, 'Sprinter', 'Branca', 'SIM', 1000),
(4, 'PAI-1509', 'Marcio Postmans', 0, 'lote 1', '3425-0987', 'SIM', 1),
(6, 'PAI-1508', 'Renault', 2015, 'Clio', 'Branca', 'SIM', 2),
(7, 'PAS-1052', 'Ford', 2018, 'A3', 'cinza', 'SIM', 1),
(8, 'JKL-3045', 'Renault', 2015, 'Master', 'Cinza', 'SIM', 1000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_alunos`
--
ALTER TABLE `tb_alunos`
  ADD PRIMARY KEY (`idaluno`),
  ADD KEY `fk_tb_alunos_tb_responsaveis1_idx` (`id_responsavel`),
  ADD KEY `fk_tb_alunos_tb_escolas1_idx` (`id_escola`),
  ADD KEY `fk_tb_alunos_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_escolas`
--
ALTER TABLE `tb_escolas`
  ADD PRIMARY KEY (`idescola`),
  ADD KEY `fk_tb_escolas_tb_empresas1_idx` (`id_usuario`);

--
-- Indexes for table `tb_faltas_embarques_alunos`
--
ALTER TABLE `tb_faltas_embarques_alunos`
  ADD PRIMARY KEY (`idfataembraquealuno`),
  ADD KEY `fk_tb_faltas_alunos_tb_alunos1_idx` (`id_aluno`),
  ADD KEY `fk_tb_faltas_alunos_tb_rotas_paradas1_idx` (`id_rotaparada`),
  ADD KEY `fk_tb_faltas_alunos_tb_responsaveis1_idx` (`id_responsavel`),
  ADD KEY `fk_tb_faltas_alunos_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_logins_responsaveis`
--
ALTER TABLE `tb_logins_responsaveis`
  ADD PRIMARY KEY (`idloginresponsavel`);

--
-- Indexes for table `tb_notificacoes_onesignal`
--
ALTER TABLE `tb_notificacoes_onesignal`
  ADD PRIMARY KEY (`idnotificacaoonesignal`);

--
-- Indexes for table `tb_notificacoes_responsaveis`
--
ALTER TABLE `tb_notificacoes_responsaveis`
  ADD PRIMARY KEY (`idnotificacaoresponsavel`),
  ADD KEY `fk_tb_notificacoes_responsaveis_tb_responsaveis1_idx` (`id_responsavel`);

--
-- Indexes for table `tb_responsaveis`
--
ALTER TABLE `tb_responsaveis`
  ADD PRIMARY KEY (`idresponsavel`),
  ADD KEY `fk_tb_responsaveis_tb_empresas1_idx` (`id_usuario`);

--
-- Indexes for table `tb_rotas`
--
ALTER TABLE `tb_rotas`
  ADD PRIMARY KEY (`idrota`),
  ADD KEY `fk_tb_rotas_tb_veiculos1_idx` (`id_veiculo`),
  ADD KEY `fk_tb_rotas_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_rotas_paradas`
--
ALTER TABLE `tb_rotas_paradas`
  ADD PRIMARY KEY (`idrotaparada`),
  ADD KEY `fk_tb_rotas_paradas_tb_rotas1_idx` (`id_rota`),
  ADD KEY `fk_tb_rotas_paradas_tb_escolas1_idx` (`id_escola`),
  ADD KEY `fk_tb_rotas_paradas_tb_alunos1_idx` (`id_aluno`),
  ADD KEY `fk_tb_rotas_paradas_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  ADD PRIMARY KEY (`idrotaparadadiaria`),
  ADD KEY `fk_tb_rotas_diarias_tb_rotas_paradas1_idx` (`id_rotaparada`),
  ADD KEY `fk_tb_rotas_paradas_diarias_tb_usuarios1_idx` (`id_usuario`),
  ADD KEY `fk_tb_rotas_paradas_diarias_tb_faltas_alunos1_idx` (`id_faltaaluno`);

--
-- Indexes for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indexes for table `tb_veiculos`
--
ALTER TABLE `tb_veiculos`
  ADD PRIMARY KEY (`idveiculo`),
  ADD KEY `fk_tb_veiculos_tb_empresas1_idx` (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_alunos`
--
ALTER TABLE `tb_alunos`
  MODIFY `idaluno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `tb_escolas`
--
ALTER TABLE `tb_escolas`
  MODIFY `idescola` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_faltas_embarques_alunos`
--
ALTER TABLE `tb_faltas_embarques_alunos`
  MODIFY `idfataembraquealuno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `tb_logins_responsaveis`
--
ALTER TABLE `tb_logins_responsaveis`
  MODIFY `idloginresponsavel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_notificacoes_onesignal`
--
ALTER TABLE `tb_notificacoes_onesignal`
  MODIFY `idnotificacaoonesignal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=391;
--
-- AUTO_INCREMENT for table `tb_notificacoes_responsaveis`
--
ALTER TABLE `tb_notificacoes_responsaveis`
  MODIFY `idnotificacaoresponsavel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `tb_responsaveis`
--
ALTER TABLE `tb_responsaveis`
  MODIFY `idresponsavel` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Aqui é o responsável pelo aluno, pode ser a mãe, tio e quem assinou o contrato foi outra pessoa por exemplo como o pai.',AUTO_INCREMENT=1002;
--
-- AUTO_INCREMENT for table `tb_rotas`
--
ALTER TABLE `tb_rotas`
  MODIFY `idrota` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_rotas_paradas`
--
ALTER TABLE `tb_rotas_paradas`
  MODIFY `idrotaparada` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=340;
--
-- AUTO_INCREMENT for table `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  MODIFY `idrotaparadadiaria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Pessoa responsável pelo contrato, pode ser uma empresa ou um pai, tio, avô e etc.',AUTO_INCREMENT=1001;
--
-- AUTO_INCREMENT for table `tb_veiculos`
--
ALTER TABLE `tb_veiculos`
  MODIFY `idveiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_alunos`
--
ALTER TABLE `tb_alunos`
  ADD CONSTRAINT `fk_tb_alunos_tb_escolas1` FOREIGN KEY (`id_escola`) REFERENCES `tb_escolas` (`idescola`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_alunos_tb_responsaveis1` FOREIGN KEY (`id_responsavel`) REFERENCES `tb_responsaveis` (`idresponsavel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_alunos_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_escolas`
--
ALTER TABLE `tb_escolas`
  ADD CONSTRAINT `fk_tb_escolas_tb_empresas1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_faltas_embarques_alunos`
--
ALTER TABLE `tb_faltas_embarques_alunos`
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_alunos1` FOREIGN KEY (`id_aluno`) REFERENCES `tb_alunos` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_responsaveis1` FOREIGN KEY (`id_responsavel`) REFERENCES `tb_responsaveis` (`idresponsavel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_rotas_paradas1` FOREIGN KEY (`id_rotaparada`) REFERENCES `tb_rotas_paradas` (`idrotaparada`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_notificacoes_responsaveis`
--
ALTER TABLE `tb_notificacoes_responsaveis`
  ADD CONSTRAINT `fk_tb_notificacoes_responsaveis_tb_responsaveis1` FOREIGN KEY (`id_responsavel`) REFERENCES `tb_responsaveis` (`idresponsavel`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_responsaveis`
--
ALTER TABLE `tb_responsaveis`
  ADD CONSTRAINT `fk_tb_responsaveis_tb_empresas1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_rotas`
--
ALTER TABLE `tb_rotas`
  ADD CONSTRAINT `fk_tb_rotas_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_rotas_tb_veiculos1` FOREIGN KEY (`id_veiculo`) REFERENCES `tb_veiculos` (`idveiculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_rotas_paradas`
--
ALTER TABLE `tb_rotas_paradas`
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_alunos1` FOREIGN KEY (`id_aluno`) REFERENCES `tb_alunos` (`idaluno`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_escolas1` FOREIGN KEY (`id_escola`) REFERENCES `tb_escolas` (`idescola`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_rotas1` FOREIGN KEY (`id_rota`) REFERENCES `tb_rotas` (`idrota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  ADD CONSTRAINT `fk_tb_rotas_diarias_tb_rotas_paradas1` FOREIGN KEY (`id_rotaparada`) REFERENCES `tb_rotas_paradas` (`idrotaparada`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_faltas_alunos1` FOREIGN KEY (`id_faltaaluno`) REFERENCES `tb_faltas_embarques_alunos` (`idfataembraquealuno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_veiculos`
--
ALTER TABLE `tb_veiculos`
  ADD CONSTRAINT `fk_tb_veiculos_tb_empresas1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
