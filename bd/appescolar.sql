-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 14-Ago-2019 às 17:38
-- Versão do servidor: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appescolar`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_alunos`
--

CREATE TABLE IF NOT EXISTS `tb_alunos` (
  `idaluno` int(11) NOT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `sala` varchar(20) DEFAULT NULL,
  `turno` varchar(150) DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral',
  `id_responsavel` int(11) NOT NULL,
  `id_escola` int(11) NOT NULL,
  `genero` varchar(150) DEFAULT NULL,
  `foto` longtext,
  `id_usuario` int(11) NOT NULL,
  `contrato_ativo` tinyint(4) DEFAULT '1' COMMENT 'Aqui armazeno se o aluno está com o contrato ativo.',
  `transporte_ida` tinyint(4) DEFAULT '1',
  `transporte_volta` tinyint(4) DEFAULT '1',
  `qtd_parcelas` int(11) DEFAULT NULL,
  `valor_parcela` double DEFAULT NULL,
  `dia_pagamento` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_alunos`
--

INSERT INTO `tb_alunos` (`idaluno`, `nome`, `sala`, `turno`, `id_responsavel`, `id_escola`, `genero`, `foto`, `id_usuario`, `contrato_ativo`, `transporte_ida`, `transporte_volta`, `qtd_parcelas`, `valor_parcela`, `dia_pagamento`) VALUES
(1, 'Alice', '10', 'M', 1, 1, 'F', NULL, 1, 1, 1, 1, 5, 165, 10),
(2, 'Marcos Paulo', '13', 'V', 1, 2, 'M', NULL, 1, 1, 1, 1, 5, 15, 15),
(3, 'Samuel', '02', 'M', 2, 1, 'M', NULL, 1, 1, 1, 1, 5, 17, 10),
(4, 'Laís Luiza Isis da Mata', '6', 'V', 2, 2, 'F', NULL, 1, 1, 1, 1, 5, 15, 10),
(5, 'Olivia Elisa Gabriela Cardoso', '4', 'V', 2, 1, 'F', NULL, 1, 1, 1, 1, 5, 18, 12),
(6, 'Cauã', '12', 'M', 3, 1, 'M', NULL, 1, 1, 1, 1, 5, 18, 10),
(7, 'Ana Clara', '12', 'M', 4, 3, 'F', NULL, 2, 1, 1, 1, 5, 16, 10),
(8, 'Raysa', '10', 'M', 5, 3, 'M', NULL, 2, 1, 1, 1, 5, 18, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_fluxo_caixa`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_fluxo_caixa` (
  `idcategoriafluxocaixa` int(11) NOT NULL,
  `titulo` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_escolas`
--

CREATE TABLE IF NOT EXISTS `tb_escolas` (
  `idescola` int(11) NOT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `localizacao_confirmada` varchar(3) DEFAULT 'NAO'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_escolas`
--

INSERT INTO `tb_escolas` (`idescola`, `nome`, `endereco`, `complemento`, `telefone`, `id_usuario`, `latitude`, `longitude`, `localizacao_confirmada`) VALUES
(1, 'Escola Classe 401', 'Quadra 401 lote', '10', '(61) 3548-7485', 1, NULL, NULL, 'NAO'),
(2, 'Escola infantil 101', 'Quadra 101', '10', '(61) 3847-8965', 1, NULL, NULL, 'NAO'),
(3, 'Escola Classe 104', 'Quadra 104 conjunto 19', '10', '(61) 3548-7545', 2, NULL, NULL, 'NAO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_faltas_embarques_alunos`
--

CREATE TABLE IF NOT EXISTS `tb_faltas_embarques_alunos` (
  `idfataembraquealuno` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `data` varchar(150) DEFAULT NULL,
  `id_rotaparada` int(11) NOT NULL,
  `id_responsavel` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `embarque` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Armazena a falta ou ida do aluno informada pelo usuario ou responsavel.';

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_fluxo_caixa`
--

CREATE TABLE IF NOT EXISTS `tb_fluxo_caixa` (
  `idfluxocaixa` int(11) NOT NULL,
  `titulo` varchar(245) DEFAULT NULL,
  `id_aluno` int(11) DEFAULT NULL,
  `id_veiculo` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `pago` tinyint(1) DEFAULT '0',
  `tipo_fluxo` varchar(45) DEFAULT NULL COMMENT 'Pode ser ''receita'' ou ''despesa''',
  `id_categoriafluxocaixa` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_fluxo_caixa`
--

INSERT INTO `tb_fluxo_caixa` (`idfluxocaixa`, `titulo`, `id_aluno`, `id_veiculo`, `data`, `valor`, `pago`, `tipo_fluxo`, `id_categoriafluxocaixa`) VALUES
(1, 'Mensalidade aluno(a) Alice - 1/5', 1, NULL, '2019-09-10', 165, 1, 'receita', NULL),
(2, 'Mensalidade aluno(a) Alice - 2/5', 1, NULL, '2019-10-10', 165, 1, 'receita', NULL),
(3, 'Mensalidade aluno(a) Alice - 3/5', 1, NULL, '2019-11-10', 165, 1, 'receita', NULL),
(4, 'Mensalidade aluno(a) Alice - 4/5', 1, NULL, '2019-12-10', 165, 1, 'receita', NULL),
(5, 'Mensalidade aluno(a) Alice - 5/5', 1, NULL, '2020-01-10', 165, 0, 'receita', NULL),
(6, 'Mensalidade aluno(a) Marcos Paulo - 1/5', 2, NULL, '2019-09-15', 15, 0, 'receita', NULL),
(7, 'Mensalidade aluno(a) Marcos Paulo - 2/5', 2, NULL, '2019-10-15', 15, 0, 'receita', NULL),
(8, 'Mensalidade aluno(a) Marcos Paulo - 3/5', 2, NULL, '2019-11-15', 15, 0, 'receita', NULL),
(9, 'Mensalidade aluno(a) Marcos Paulo - 4/5', 2, NULL, '2019-12-15', 15, 0, 'receita', NULL),
(10, 'Mensalidade aluno(a) Marcos Paulo - 5/5', 2, NULL, '2020-01-15', 15, 0, 'receita', NULL),
(11, 'Mensalidade aluno(a) Samuel - 1/5', 3, NULL, '2019-09-10', 17, 0, 'receita', NULL),
(12, 'Mensalidade aluno(a) Samuel - 2/5', 3, NULL, '2019-10-10', 17, 0, 'receita', NULL),
(13, 'Mensalidade aluno(a) Samuel - 3/5', 3, NULL, '2019-11-10', 17, 0, 'receita', NULL),
(14, 'Mensalidade aluno(a) Samuel - 4/5', 3, NULL, '2019-12-10', 17, 0, 'receita', NULL),
(15, 'Mensalidade aluno(a) Samuel - 5/5', 3, NULL, '2020-01-10', 17, 0, 'receita', NULL),
(16, 'Mensalidade aluno(a) Laís Luiza Isis da Mata - 1/5', 4, NULL, '2019-09-10', 15, 0, 'receita', NULL),
(17, 'Mensalidade aluno(a) Laís Luiza Isis da Mata - 2/5', 4, NULL, '2019-10-10', 15, 0, 'receita', NULL),
(18, 'Mensalidade aluno(a) Laís Luiza Isis da Mata - 3/5', 4, NULL, '2019-11-10', 15, 0, 'receita', NULL),
(19, 'Mensalidade aluno(a) Laís Luiza Isis da Mata - 4/5', 4, NULL, '2019-12-10', 15, 0, 'receita', NULL),
(20, 'Mensalidade aluno(a) Laís Luiza Isis da Mata - 5/5', 4, NULL, '2020-01-10', 15, 0, 'receita', NULL),
(21, 'Mensalidade aluno(a) Olivia Elisa Gabriela Cardoso - 1/5', 5, NULL, '2019-09-12', 18, 0, 'receita', NULL),
(22, 'Mensalidade aluno(a) Olivia Elisa Gabriela Cardoso - 2/5', 5, NULL, '2019-10-12', 18, 0, 'receita', NULL),
(23, 'Mensalidade aluno(a) Olivia Elisa Gabriela Cardoso - 3/5', 5, NULL, '2019-11-12', 18, 0, 'receita', NULL),
(24, 'Mensalidade aluno(a) Olivia Elisa Gabriela Cardoso - 4/5', 5, NULL, '2019-12-12', 18, 0, 'receita', NULL),
(25, 'Mensalidade aluno(a) Olivia Elisa Gabriela Cardoso - 5/5', 5, NULL, '2020-01-12', 18, 0, 'receita', NULL),
(26, 'Mensalidade aluno(a) Cauã - 1/5', 6, NULL, '2019-09-10', 18, 0, 'receita', NULL),
(27, 'Mensalidade aluno(a) Cauã - 2/5', 6, NULL, '2019-10-10', 18, 0, 'receita', NULL),
(28, 'Mensalidade aluno(a) Cauã - 3/5', 6, NULL, '2019-11-10', 18, 0, 'receita', NULL),
(29, 'Mensalidade aluno(a) Cauã - 4/5', 6, NULL, '2019-12-10', 18, 0, 'receita', NULL),
(30, 'Mensalidade aluno(a) Cauã - 5/5', 6, NULL, '2020-01-10', 18, 0, 'receita', NULL),
(31, 'Mensalidade aluno(a) Ana Clara - 1/5', 7, NULL, '2019-09-10', 16, 0, 'receita', NULL),
(32, 'Mensalidade aluno(a) Ana Clara - 2/5', 7, NULL, '2019-10-10', 16, 0, 'receita', NULL),
(33, 'Mensalidade aluno(a) Ana Clara - 3/5', 7, NULL, '2019-11-10', 16, 0, 'receita', NULL),
(34, 'Mensalidade aluno(a) Ana Clara - 4/5', 7, NULL, '2019-12-10', 16, 0, 'receita', NULL),
(35, 'Mensalidade aluno(a) Ana Clara - 5/5', 7, NULL, '2020-01-10', 16, 0, 'receita', NULL),
(36, 'Mensalidade aluno(a) Raysa - 1/5', 8, NULL, '2019-09-10', 18, 0, 'receita', NULL),
(37, 'Mensalidade aluno(a) Raysa - 2/5', 8, NULL, '2019-10-10', 18, 0, 'receita', NULL),
(38, 'Mensalidade aluno(a) Raysa - 3/5', 8, NULL, '2019-11-10', 18, 0, 'receita', NULL),
(39, 'Mensalidade aluno(a) Raysa - 4/5', 8, NULL, '2019-12-10', 18, 0, 'receita', NULL),
(40, 'Mensalidade aluno(a) Raysa - 5/5', 8, NULL, '2020-01-10', 18, 0, 'receita', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins_responsaveis`
--

CREATE TABLE IF NOT EXISTS `tb_logins_responsaveis` (
  `idloginresponsavel` int(11) NOT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `nome` varchar(145) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `onesignail_idplayer` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins_responsaveis`
--

INSERT INTO `tb_logins_responsaveis` (`idloginresponsavel`, `cpf`, `nome`, `senha`, `onesignail_idplayer`, `email`, `token`) VALUES
(1, '383.499.901-63', 'Terezinha Aparecida Dedonno', '7c4a8d09ca3762af61e59520943dc26494f8941b', '112APOS11111', 'terezinhadedonno@gmail.com', '6049347f448f4854632f74a89c61beb2'),
(2, '243.230.042-49', 'Vera Sebastiana Ester Freitas', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 'vera@masmidia.net\r\n', '89a0f03c80c4702cf45f53bb3cf49a8b44'),
(3, '650.185.321-49', 'Emily Alana Gomes', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', 'emily@masmidia.net', '8745847f448f4854632f74a89yw82u862'),
(4, '851.345.077-40', 'Eduardo Bento Lima', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 'edu@masmidia.net', '78975657f448f4854632f74a89c6uiyqw3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_notificacoes_onesignal`
--

CREATE TABLE IF NOT EXISTS `tb_notificacoes_onesignal` (
  `idnotificacaoonesignal` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `mensagem` longtext,
  `player_ids` varchar(245) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_notificacoes_onesignal`
--

INSERT INTO `tb_notificacoes_onesignal` (`idnotificacaoonesignal`, `titulo`, `mensagem`, `player_ids`, `datetime`) VALUES
(1, 'Iniciando nosso itinerário.', 'Boa noite, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice. Caso o aluno não irá utilizar o transporte por favor nos avise.', 'null', '2019-08-13 19:40:46'),
(2, 'Iniciando nosso itinerário.', 'Boa tarde, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice. Caso o aluno não irá utilizar o transporte por favor nos avise.', 'null', '2019-08-14 14:29:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_notificacoes_responsaveis`
--

CREATE TABLE IF NOT EXISTS `tb_notificacoes_responsaveis` (
  `idnotificacaoresponsavel` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `mensagem` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_responsavel` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `notificacao_lida` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_notificacoes_responsaveis`
--

INSERT INTO `tb_notificacoes_responsaveis` (`idnotificacaoresponsavel`, `titulo`, `mensagem`, `data`, `hora`, `id_responsavel`, `id_usuario`, `notificacao_lida`) VALUES
(1, 'Iniciando nosso itinerário.', 'Boa noite, Vera Sebastiana Ester Freitas estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '2019-08-13', '19:40:46', 2, 1, 0),
(2, 'Iniciando nosso itinerário.', 'Boa noite, Emily Alana Gomes estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '2019-08-13', '19:40:46', 3, 1, 0),
(3, 'Iniciando nosso itinerário.', 'Boa noite, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice. Caso o aluno não irá utilizar o transporte por favor nos avise.', '2019-08-13', '19:40:46', 1, 1, 0),
(4, 'Iniciando nosso itinerário.', 'Boa tarde, Vera Sebastiana Ester Freitas estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Samuel. Caso o aluno não irá utilizar o transporte por favor nos avise.', '2019-08-14', '14:29:44', 2, 1, 0),
(5, 'Iniciando nosso itinerário.', 'Boa tarde, Emily Alana Gomes estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Cauã. Caso o aluno não irá utilizar o transporte por favor nos avise.', '2019-08-14', '14:29:46', 3, 1, 0),
(6, 'Iniciando nosso itinerário.', 'Boa tarde, Terezinha Aparecida Dedonno estamos iniciando nosso itinerário, logo estaremos chegando ao seu local para embarque de Alice. Caso o aluno não irá utilizar o transporte por favor nos avise.', '2019-08-14', '14:29:48', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_responsaveis`
--

CREATE TABLE IF NOT EXISTS `tb_responsaveis` (
  `idresponsavel` int(11) NOT NULL COMMENT 'Aqui é o responsável pelo aluno, pode ser a mãe, tio e quem assinou o contrato foi outra pessoa por exemplo como o pai.',
  `nome` varchar(150) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `rg` int(11) DEFAULT NULL,
  `telefone_residencial` varchar(15) DEFAULT NULL,
  `telefone_celular` varchar(15) DEFAULT NULL,
  `telefone_trabalho` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `bairro` varchar(150) DEFAULT NULL,
  `cidade` varchar(150) DEFAULT NULL,
  `uf` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_responsaveis`
--

INSERT INTO `tb_responsaveis` (`idresponsavel`, `nome`, `cpf`, `endereco`, `complemento`, `id_usuario`, `rg`, `telefone_residencial`, `telefone_celular`, `telefone_trabalho`, `email`, `senha`, `cep`, `bairro`, `cidade`, `uf`) VALUES
(1, 'Terezinha Aparecida Dedonno', '383.499.901-63', 'Quadra 204 conjunto 17 lote', '08', 1, NULL, '(61) 3025-6584', '(61) 98548-7465', '(61) 3025-4874', 'terezinhadedonno@masmidia.net', NULL, '72610-417', 'Recanto das Emas', 'Brasília', 'DF'),
(2, 'Vera Sebastiana Ester Freitas', '243.230.042-49', 'Quadra 802 conjunto 11', '08', 1, NULL, '(61) 3457-8598', '(61) 99874-5211', NULL, 'vera@masmidia.net', NULL, '72610-478', 'Recanto das Emas', 'Brasília', 'DF'),
(3, 'Emily Alana Gomes', '650.185.321-49', 'Quadra 101 conjunto 19 lote', '10', 1, NULL, '(61) 3554-8785', '(61) 98654-8741', NULL, 'emily@masmidia.net', NULL, '72154-566', 'Recanto das Emas', 'Brasília', 'DF'),
(4, 'Terezinha Aparecida Dedonno', '383.499.901-63', 'Quadra 204 conjunto 17 lote', '8', 2, NULL, '(61) 3254-8745', '(61) 98654-7854', '(61) 3214-5478', 'terezinhadedonoo@gmail.com', NULL, '72654-787', 'Recanto das Emas', 'Brasília', 'DF'),
(5, 'Eduardo Bento Lima', '851.345.077-40', 'Quadra 401 lote', '10', 2, NULL, '(61) 3254-8745', '(61) 98754-8964', '', 'edu@masmidia.net', NULL, '72658-774', 'Recanto das Emas', 'Brasília', 'DF');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_rotas`
--

CREATE TABLE IF NOT EXISTS `tb_rotas` (
  `idrota` int(11) NOT NULL,
  `titulo` varchar(150) DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `id_veiculo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `online` tinyint(1) DEFAULT '0' COMMENT '0 = false'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_rotas`
--

INSERT INTO `tb_rotas` (`idrota`, `titulo`, `hora_inicio`, `id_veiculo`, `id_usuario`, `latitude`, `longitude`, `online`) VALUES
(1, 'Matutino', '06:00:00', 1, 1, '-15.9074788', '-48.064080499999996', 1),
(2, 'Vespertino', '11:45:00', 1, 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_rotas_paradas`
--

CREATE TABLE IF NOT EXISTS `tb_rotas_paradas` (
  `idrotaparada` int(11) NOT NULL,
  `tipo_parada` varchar(150) DEFAULT NULL COMMENT 'A parada pode ser do tipo, ALUNO ou ESCOLA',
  `id_rota` int(11) NOT NULL,
  `id_escola` int(11) DEFAULT NULL,
  `id_aluno` int(11) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `localizacao_confirmada` tinyint(4) DEFAULT '0',
  `id_usuario` int(11) NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_rotas_paradas`
--

INSERT INTO `tb_rotas_paradas` (`idrotaparada`, `tipo_parada`, `id_rota`, `id_escola`, `id_aluno`, `latitude`, `longitude`, `localizacao_confirmada`, `id_usuario`, `ordem`, `ativo`) VALUES
(2, 'escola', 1, 1, NULL, NULL, NULL, 0, 1, 4, 1),
(4, 'escola', 1, 2, NULL, NULL, NULL, 0, 1, 2, 1),
(5, 'aluno', 1, NULL, 1, NULL, NULL, 0, 1, 3, 1),
(6, 'aluno', 1, NULL, 6, NULL, NULL, 0, 1, 1, 1),
(7, 'aluno', 1, NULL, 3, NULL, NULL, 0, 1, 0, 1),
(10, 'escola', 2, 1, NULL, NULL, NULL, 0, 1, 2, 1),
(11, 'escola', 2, 1, NULL, NULL, NULL, 0, 1, 4, 1),
(13, 'escola', 2, 2, NULL, NULL, NULL, 0, 1, 5, 1),
(14, 'aluno', 2, NULL, 2, NULL, NULL, 0, 1, 3, 1),
(15, 'aluno', 2, NULL, 4, NULL, NULL, 0, 1, 1, 1),
(16, 'aluno', 2, NULL, 5, NULL, NULL, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_rotas_paradas_diarias`
--

CREATE TABLE IF NOT EXISTS `tb_rotas_paradas_diarias` (
  `idrotaparadadiaria` int(11) NOT NULL,
  `id_rotaparada` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `embarque` tinyint(4) DEFAULT '1' COMMENT 'Armazena se o aluno fara o embarque',
  `hora_embarque` time DEFAULT NULL,
  `concluida` tinyint(4) DEFAULT '0' COMMENT 'Arazena se a parada foi concluída.',
  `id_usuario` int(11) NOT NULL,
  `id_faltaaluno` int(11) DEFAULT NULL,
  `aluno_embarcou` tinyint(1) DEFAULT '0' COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou',
  `responsavel_notificado` tinyint(1) DEFAULT '0',
  `aluno_embarcou_desembarcou_escola` tinyint(1) DEFAULT '0' COMMENT 'Armazena se o aluno embarcou ou desermbarcou na escola'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_rotas_paradas_diarias`
--

INSERT INTO `tb_rotas_paradas_diarias` (`idrotaparadadiaria`, `id_rotaparada`, `data`, `embarque`, `hora_embarque`, `concluida`, `id_usuario`, `id_faltaaluno`, `aluno_embarcou`, `responsavel_notificado`, `aluno_embarcou_desembarcou_escola`) VALUES
(1, 7, '2019-08-13', 1, NULL, 0, 1, NULL, 0, 1, 0),
(2, 6, '2019-08-13', 1, NULL, 0, 1, NULL, 0, 1, 0),
(3, 4, '2019-08-13', 1, NULL, 0, 1, NULL, 0, 1, 0),
(4, 5, '2019-08-13', 1, NULL, 0, 1, NULL, 0, 1, 0),
(5, 2, '2019-08-13', 1, NULL, 0, 1, NULL, 0, 1, 0),
(6, 7, '2019-08-14', 1, NULL, 0, 1, NULL, 0, 1, 0),
(7, 6, '2019-08-14', 1, NULL, 0, 1, NULL, 0, 1, 0),
(8, 4, '2019-08-14', 1, NULL, 0, 1, NULL, 0, 1, 0),
(9, 5, '2019-08-14', 1, NULL, 0, 1, NULL, 0, 1, 0),
(10, 2, '2019-08-14', 1, NULL, 0, 1, NULL, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuarios`
--

CREATE TABLE IF NOT EXISTS `tb_usuarios` (
  `idusuario` int(11) NOT NULL COMMENT 'Pessoa responsável pelo contrato, pode ser uma empresa ou um pai, tio, avô e etc.',
  `nome` varchar(150) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `senha` varchar(150) DEFAULT NULL,
  `onesignail_idplayer` varchar(245) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'sim',
  `token` varchar(145) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`idusuario`, `nome`, `telefone`, `email`, `senha`, `onesignail_idplayer`, `ativo`, `token`) VALUES
(1, 'Marcio André da Silva', '(61) 98606-6484', 'marciomas@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 'sim', '08dbaae8c33fc28d857eef682ad58a07'),
(2, 'André de Freitas', '(61) 98574-5456', 'andre@masmidia.net', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 'sim', 'd886989c47809ad1de1431cd37977116');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_veiculos`
--

CREATE TABLE IF NOT EXISTS `tb_veiculos` (
  `idveiculo` int(11) NOT NULL,
  `placa` varchar(8) DEFAULT NULL,
  `marca` varchar(150) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `modelo` varchar(150) DEFAULT NULL,
  `cor` varchar(150) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_veiculos`
--

INSERT INTO `tb_veiculos` (`idveiculo`, `placa`, `marca`, `ano`, `modelo`, `cor`, `ativo`, `id_usuario`) VALUES
(1, 'OXA-1458', 'Mercedes', 2011, 'Splinter', 'Prata', 'SIM', 1),
(2, 'URH-1225', 'Volare', 2004, 'A6', 'Branco', 'SIM', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_alunos`
--
ALTER TABLE `tb_alunos`
  ADD PRIMARY KEY (`idaluno`),
  ADD KEY `fk_tb_alunos_tb_responsaveis1_idx` (`id_responsavel`),
  ADD KEY `fk_tb_alunos_tb_escolas1_idx` (`id_escola`),
  ADD KEY `fk_tb_alunos_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_categorias_fluxo_caixa`
--
ALTER TABLE `tb_categorias_fluxo_caixa`
  ADD PRIMARY KEY (`idcategoriafluxocaixa`);

--
-- Indexes for table `tb_escolas`
--
ALTER TABLE `tb_escolas`
  ADD PRIMARY KEY (`idescola`),
  ADD KEY `fk_tb_escolas_tb_empresas1_idx` (`id_usuario`);

--
-- Indexes for table `tb_faltas_embarques_alunos`
--
ALTER TABLE `tb_faltas_embarques_alunos`
  ADD PRIMARY KEY (`idfataembraquealuno`),
  ADD KEY `fk_tb_faltas_alunos_tb_alunos1_idx` (`id_aluno`),
  ADD KEY `fk_tb_faltas_alunos_tb_rotas_paradas1_idx` (`id_rotaparada`),
  ADD KEY `fk_tb_faltas_alunos_tb_responsaveis1_idx` (`id_responsavel`),
  ADD KEY `fk_tb_faltas_alunos_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_fluxo_caixa`
--
ALTER TABLE `tb_fluxo_caixa`
  ADD PRIMARY KEY (`idfluxocaixa`),
  ADD KEY `fk_tb_fluxo_caixa_tb_alunos1_idx` (`id_aluno`),
  ADD KEY `fk_tb_fluxo_caixa_tb_veiculos1_idx` (`id_veiculo`),
  ADD KEY `fk_tb_fluxo_caixa_tb_categorias_fluxo_caixa1_idx` (`id_categoriafluxocaixa`);

--
-- Indexes for table `tb_logins_responsaveis`
--
ALTER TABLE `tb_logins_responsaveis`
  ADD PRIMARY KEY (`idloginresponsavel`);

--
-- Indexes for table `tb_notificacoes_onesignal`
--
ALTER TABLE `tb_notificacoes_onesignal`
  ADD PRIMARY KEY (`idnotificacaoonesignal`);

--
-- Indexes for table `tb_notificacoes_responsaveis`
--
ALTER TABLE `tb_notificacoes_responsaveis`
  ADD PRIMARY KEY (`idnotificacaoresponsavel`),
  ADD KEY `fk_tb_notificacoes_responsaveis_tb_responsaveis1_idx` (`id_responsavel`),
  ADD KEY `fk_tb_notificacoes_responsaveis_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_responsaveis`
--
ALTER TABLE `tb_responsaveis`
  ADD PRIMARY KEY (`idresponsavel`),
  ADD KEY `fk_tb_responsaveis_tb_empresas1_idx` (`id_usuario`);

--
-- Indexes for table `tb_rotas`
--
ALTER TABLE `tb_rotas`
  ADD PRIMARY KEY (`idrota`),
  ADD KEY `fk_tb_rotas_tb_veiculos1_idx` (`id_veiculo`),
  ADD KEY `fk_tb_rotas_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_rotas_paradas`
--
ALTER TABLE `tb_rotas_paradas`
  ADD PRIMARY KEY (`idrotaparada`),
  ADD KEY `fk_tb_rotas_paradas_tb_rotas1_idx` (`id_rota`),
  ADD KEY `fk_tb_rotas_paradas_tb_escolas1_idx` (`id_escola`),
  ADD KEY `fk_tb_rotas_paradas_tb_alunos1_idx` (`id_aluno`),
  ADD KEY `fk_tb_rotas_paradas_tb_usuarios1_idx` (`id_usuario`);

--
-- Indexes for table `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  ADD PRIMARY KEY (`idrotaparadadiaria`),
  ADD KEY `fk_tb_rotas_diarias_tb_rotas_paradas1_idx` (`id_rotaparada`),
  ADD KEY `fk_tb_rotas_paradas_diarias_tb_usuarios1_idx` (`id_usuario`),
  ADD KEY `fk_tb_rotas_paradas_diarias_tb_faltas_alunos1_idx` (`id_faltaaluno`);

--
-- Indexes for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indexes for table `tb_veiculos`
--
ALTER TABLE `tb_veiculos`
  ADD PRIMARY KEY (`idveiculo`),
  ADD KEY `fk_tb_veiculos_tb_empresas1_idx` (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_alunos`
--
ALTER TABLE `tb_alunos`
  MODIFY `idaluno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_categorias_fluxo_caixa`
--
ALTER TABLE `tb_categorias_fluxo_caixa`
  MODIFY `idcategoriafluxocaixa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_escolas`
--
ALTER TABLE `tb_escolas`
  MODIFY `idescola` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_faltas_embarques_alunos`
--
ALTER TABLE `tb_faltas_embarques_alunos`
  MODIFY `idfataembraquealuno` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_fluxo_caixa`
--
ALTER TABLE `tb_fluxo_caixa`
  MODIFY `idfluxocaixa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tb_logins_responsaveis`
--
ALTER TABLE `tb_logins_responsaveis`
  MODIFY `idloginresponsavel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_notificacoes_onesignal`
--
ALTER TABLE `tb_notificacoes_onesignal`
  MODIFY `idnotificacaoonesignal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_notificacoes_responsaveis`
--
ALTER TABLE `tb_notificacoes_responsaveis`
  MODIFY `idnotificacaoresponsavel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_responsaveis`
--
ALTER TABLE `tb_responsaveis`
  MODIFY `idresponsavel` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Aqui é o responsável pelo aluno, pode ser a mãe, tio e quem assinou o contrato foi outra pessoa por exemplo como o pai.',AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_rotas`
--
ALTER TABLE `tb_rotas`
  MODIFY `idrota` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_rotas_paradas`
--
ALTER TABLE `tb_rotas_paradas`
  MODIFY `idrotaparada` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  MODIFY `idrotaparadadiaria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Pessoa responsável pelo contrato, pode ser uma empresa ou um pai, tio, avô e etc.',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_veiculos`
--
ALTER TABLE `tb_veiculos`
  MODIFY `idveiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_alunos`
--
ALTER TABLE `tb_alunos`
  ADD CONSTRAINT `fk_tb_alunos_tb_escolas1` FOREIGN KEY (`id_escola`) REFERENCES `tb_escolas` (`idescola`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_alunos_tb_responsaveis1` FOREIGN KEY (`id_responsavel`) REFERENCES `tb_responsaveis` (`idresponsavel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_alunos_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_escolas`
--
ALTER TABLE `tb_escolas`
  ADD CONSTRAINT `fk_tb_escolas_tb_empresas1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_faltas_embarques_alunos`
--
ALTER TABLE `tb_faltas_embarques_alunos`
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_alunos1` FOREIGN KEY (`id_aluno`) REFERENCES `tb_alunos` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_responsaveis1` FOREIGN KEY (`id_responsavel`) REFERENCES `tb_responsaveis` (`idresponsavel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_rotas_paradas1` FOREIGN KEY (`id_rotaparada`) REFERENCES `tb_rotas_paradas` (`idrotaparada`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_faltas_alunos_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_fluxo_caixa`
--
ALTER TABLE `tb_fluxo_caixa`
  ADD CONSTRAINT `fk_tb_fluxo_caixa_tb_alunos1` FOREIGN KEY (`id_aluno`) REFERENCES `tb_alunos` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_fluxo_caixa_tb_categorias_fluxo_caixa1` FOREIGN KEY (`id_categoriafluxocaixa`) REFERENCES `tb_categorias_fluxo_caixa` (`idcategoriafluxocaixa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_fluxo_caixa_tb_veiculos1` FOREIGN KEY (`id_veiculo`) REFERENCES `tb_veiculos` (`idveiculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_notificacoes_responsaveis`
--
ALTER TABLE `tb_notificacoes_responsaveis`
  ADD CONSTRAINT `fk_tb_notificacoes_responsaveis_tb_responsaveis1` FOREIGN KEY (`id_responsavel`) REFERENCES `tb_responsaveis` (`idresponsavel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_notificacoes_responsaveis_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_responsaveis`
--
ALTER TABLE `tb_responsaveis`
  ADD CONSTRAINT `fk_tb_responsaveis_tb_empresas1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_rotas`
--
ALTER TABLE `tb_rotas`
  ADD CONSTRAINT `fk_tb_rotas_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_rotas_tb_veiculos1` FOREIGN KEY (`id_veiculo`) REFERENCES `tb_veiculos` (`idveiculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_rotas_paradas`
--
ALTER TABLE `tb_rotas_paradas`
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_alunos1` FOREIGN KEY (`id_aluno`) REFERENCES `tb_alunos` (`idaluno`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_escolas1` FOREIGN KEY (`id_escola`) REFERENCES `tb_escolas` (`idescola`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_rotas1` FOREIGN KEY (`id_rota`) REFERENCES `tb_rotas` (`idrota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_rotas_paradas_diarias`
--
ALTER TABLE `tb_rotas_paradas_diarias`
  ADD CONSTRAINT `fk_tb_rotas_diarias_tb_rotas_paradas1` FOREIGN KEY (`id_rotaparada`) REFERENCES `tb_rotas_paradas` (`idrotaparada`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_faltas_alunos1` FOREIGN KEY (`id_faltaaluno`) REFERENCES `tb_faltas_embarques_alunos` (`idfataembraquealuno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_veiculos`
--
ALTER TABLE `tb_veiculos`
  ADD CONSTRAINT `fk_tb_veiculos_tb_empresas1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
