-- MySQL Workbench Synchronization
-- Generated: 2020-01-03 08:24
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `appescolar`.`tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `appescolar`.`tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `appescolar`.`tb_ajuda_respostas` 
DROP COLUMN `id_ajudacategoria`,
ADD COLUMN `id_categoriaajuda` INT(11) NOT NULL AFTER `idajudaresposta`,
ADD COLUMN `views` INT(11) NULL DEFAULT NULL AFTER `categoria`,
ADD COLUMN `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `views`,
ADD INDEX `fk_tb_ajuda_respostas_tb_categorias_ajudas1_idx` (`id_categoriaajuda` ASC);
;

CREATE TABLE IF NOT EXISTS `appescolar`.`tb_categorias_ajudas` (
  `idcategoriaajuda` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(245) NOT NULL,
  `imagem` VARCHAR(145) NOT NULL,
  `ativo` TINYINT(1) NULL DEFAULT 1,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo` VARCHAR(45) NOT NULL COMMENT 'Pode ser \"motorista\" ou \"responsavel\"',
  PRIMARY KEY (`idcategoriaajuda`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `appescolar`.`tb_ajuda_respostas` 
ADD CONSTRAINT `fk_tb_ajuda_respostas_tb_categorias_ajudas1`
  FOREIGN KEY (`id_categoriaajuda`)
  REFERENCES `appescolar`.`tb_categorias_ajudas` (`idcategoriaajuda`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
