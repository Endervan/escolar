-- MySQL Workbench Synchronization
-- Generated: 2019-07-30 19:27
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

CREATE TABLE IF NOT EXISTS `tb_fluxo_caixa` (
  `idfluxocaixa` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(245) NULL DEFAULT NULL,
  `id_aluno` INT(11) NULL DEFAULT NULL,
  `id_veiculo` INT(11) NULL DEFAULT NULL,
  `data` DATE NULL DEFAULT NULL,
  `valor` DOUBLE NULL DEFAULT NULL,
  `pago` VARCHAR(3) NULL DEFAULT 'nao',
  `tipo_fluxo` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Pode ser \'receita\' ou \'despesa\'',
  `id_categoriafluxocaixa` INT(11) NOT NULL,
  PRIMARY KEY (`idfluxocaixa`),
  INDEX `fk_tb_fluxo_caixa_tb_alunos1_idx` (`id_aluno` ASC),
  INDEX `fk_tb_fluxo_caixa_tb_veiculos1_idx` (`id_veiculo` ASC),
  INDEX `fk_tb_fluxo_caixa_tb_categorias_fluxo_caixa1_idx` (`id_categoriafluxocaixa` ASC),
  CONSTRAINT `fk_tb_fluxo_caixa_tb_alunos1`
    FOREIGN KEY (`id_aluno`)
    REFERENCES `tb_alunos` (`idaluno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_fluxo_caixa_tb_veiculos1`
    FOREIGN KEY (`id_veiculo`)
    REFERENCES `tb_veiculos` (`idveiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_fluxo_caixa_tb_categorias_fluxo_caixa1`
    FOREIGN KEY (`id_categoriafluxocaixa`)
    REFERENCES `tb_categorias_fluxo_caixa` (`idcategoriafluxocaixa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tb_categorias_fluxo_caixa` (
  `idcategoriafluxocaixa` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(245) NULL DEFAULT NULL,
  PRIMARY KEY (`idcategoriafluxocaixa`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;





-- MySQL Workbench Synchronization
-- Generated: 2019-07-30 19:29
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
ADD COLUMN `qtd_parcelas` INT(11) NULL DEFAULT NULL AFTER `transporte_volta`,
ADD COLUMN `valor_parcela` DOUBLE NULL DEFAULT NULL AFTER `qtd_parcelas`,
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




-- MySQL Workbench Synchronization
-- Generated: 2019-07-30 19:42
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
ADD COLUMN `dia_pagamento` INT(11) NULL DEFAULT NULL AFTER `valor_parcela`,
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




-- MySQL Workbench Synchronization
-- Generated: 2019-07-31 10:45
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_fluxo_caixa` 
DROP FOREIGN KEY `fk_tb_fluxo_caixa_tb_categorias_fluxo_caixa1`;

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `tb_fluxo_caixa` 
CHANGE COLUMN `id_categoriafluxocaixa` `id_categoriafluxocaixa` INT(11) NULL DEFAULT NULL ;

ALTER TABLE `tb_fluxo_caixa` 
ADD CONSTRAINT `fk_tb_fluxo_caixa_tb_categorias_fluxo_caixa1`
  FOREIGN KEY (`id_categoriafluxocaixa`)
  REFERENCES `tb_categorias_fluxo_caixa` (`idcategoriafluxocaixa`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;









-- MySQL Workbench Synchronization
-- Generated: 2019-07-31 12:05
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;


ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `tb_fluxo_caixa` 
CHANGE COLUMN `pago` `pago` TINYINT(1) NULL DEFAULT 0 ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;








-- MySQL Workbench Synchronization
-- Generated: 2019-08-04 12:50
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
ADD COLUMN `latitude` VARCHAR(100) NULL DEFAULT NULL AFTER `aluno_embarcou_desembarcou_escola`,
ADD COLUMN `longitude` VARCHAR(100) NULL DEFAULT NULL AFTER `latitude`,
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;






-- MySQL Workbench Synchronization
-- Generated: 2019-08-05 09:35
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas` 
ADD COLUMN `latitude` VARCHAR(100) NULL DEFAULT NULL AFTER `id_usuario`,
ADD COLUMN `longitude` VARCHAR(100) NULL DEFAULT NULL AFTER `latitude`;

ALTER TABLE `tb_rotas_paradas_diarias` 
DROP COLUMN `longitude`,
DROP COLUMN `latitude`,
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;





-- MySQL Workbench Synchronization
-- Generated: 2019-08-06 10:34
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `tb_logins_responsaveis` 
ADD COLUMN `token` VARCHAR(150) NULL DEFAULT NULL AFTER `email`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;





-- MySQL Workbench Synchronization
-- Generated: 2019-08-09 19:21
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
ADD COLUMN `online` TINYINT(1) NULL DEFAULT 0 AFTER `aluno_embarcou_desembarcou_escola`,
ADD COLUMN `latitude` VARCHAR(145) NULL DEFAULT NULL AFTER `online`,
ADD COLUMN `longitude` VARCHAR(145) NULL DEFAULT NULL AFTER `latitude`,
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




-- MySQL Workbench Synchronization
-- Generated: 2019-08-09 19:30
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas` 
ADD COLUMN `online` TINYINT(1) NULL DEFAULT 0 COMMENT '0 = false' AFTER `longitude`;

ALTER TABLE `tb_rotas_paradas_diarias` 
DROP COLUMN `longitude`,
DROP COLUMN `latitude`,
DROP COLUMN `online`,
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

CREATE TABLE IF NOT EXISTS `tb_rotas_paradas_diarias_copy1` (
  `idrotaparadadiaria` INT(11) NOT NULL AUTO_INCREMENT,
  `id_rotaparada` INT(11) NOT NULL,
  `data` DATE NULL DEFAULT NULL,
  `embarque` TINYINT(4) NULL DEFAULT 1 COMMENT 'Armazena se o aluno fara o embarque',
  `hora_embarque` TIME NULL DEFAULT NULL,
  `concluida` TINYINT(4) NULL DEFAULT 0 COMMENT 'Arazena se a parada foi concluída.',
  `id_usuario` INT(11) NOT NULL,
  `id_faltaaluno` INT(11) NULL DEFAULT NULL,
  `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou',
  `responsavel_notificado` TINYINT(1) NULL DEFAULT 0,
  `aluno_embarcou_desembarcou_escola` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena se o aluno embarcou ou desermbarcou na escola',
  PRIMARY KEY (`idrotaparadadiaria`),
  INDEX `fk_tb_rotas_diarias_tb_rotas_paradas1_idx` (`id_rotaparada` ASC),
  INDEX `fk_tb_rotas_paradas_diarias_tb_usuarios1_idx` (`id_usuario` ASC),
  INDEX `fk_tb_rotas_paradas_diarias_tb_faltas_alunos1_idx` (`id_faltaaluno` ASC),
  CONSTRAINT `fk_tb_rotas_diarias_tb_rotas_paradas10`
    FOREIGN KEY (`id_rotaparada`)
    REFERENCES `tb_rotas_paradas` (`idrotaparada`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_usuarios10`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `tb_usuarios` (`idusuario`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_rotas_paradas_diarias_tb_faltas_alunos10`
    FOREIGN KEY (`id_faltaaluno`)
    REFERENCES `tb_faltas_embarques_alunos` (`idfataembraquealuno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;









-- MySQL Workbench Synchronization
-- Generated: 2019-08-12 21:02
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `tb_notificacoes_responsaveis` 
ADD COLUMN `id_usuario` INT(11) NULL DEFAULT NULL AFTER `id_responsavel`,
ADD INDEX `fk_tb_notificacoes_responsaveis_tb_usuarios1_idx` (`id_usuario` ASC);
;

DROP TABLE IF EXISTS `tb_rotas_paradas_diarias_copy1` ;

ALTER TABLE `tb_notificacoes_responsaveis` 
ADD CONSTRAINT `fk_tb_notificacoes_responsaveis_tb_usuarios1`
  FOREIGN KEY (`id_usuario`)
  REFERENCES `tb_usuarios` (`idusuario`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;







-- MySQL Workbench Synchronization
-- Generated: 2019-08-12 21:06
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `tb_notificacoes_responsaveis` 
ADD COLUMN `notificacao_lida` TINYINT(1) NULL DEFAULT 0 AFTER `id_usuario`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;







-- MySQL Workbench Synchronization
-- Generated: 2019-08-13 00:48
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `tb_notificacoes_responsaveis` 
ADD COLUMN `hora` TIME NULL DEFAULT NULL AFTER `data`,
CHANGE COLUMN `data` `data` DATE NULL DEFAULT NULL ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




-- MySQL Workbench Synchronization
-- Generated: 2019-08-27 20:12
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER SCHEMA `appescolar`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;

ALTER TABLE `appescolar`.`tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `appescolar`.`tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

CREATE TABLE IF NOT EXISTS `appescolar`.`tb_ajuda_respostas` (
  `idajudaresposta` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(255) NULL DEFAULT NULL,
  `descricao` LONGTEXT NULL DEFAULT NULL,
  `ativo` TINYINT(1) NULL DEFAULT 1,
  `id_ajudacategoria` INT(11) NOT NULL,
  `tipo_ajuda` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Pode ser \"responsavel\" ou \"usuario\"',
  `categoria` VARCHAR(145) NULL DEFAULT NULL,
  PRIMARY KEY (`idajudaresposta`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
