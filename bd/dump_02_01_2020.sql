-- MySQL Workbench Synchronization
-- Generated: 2020-01-02 19:12
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Marcio Andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `appescolar`.`tb_alunos` 
CHANGE COLUMN `turno` `turno` VARCHAR(150) NULL DEFAULT NULL COMMENT 'Turno\n2 = Manhã \n3 = tarde\nN = Noite\n1 = Integral' ;

ALTER TABLE `appescolar`.`tb_rotas_paradas_diarias` 
CHANGE COLUMN `aluno_embarcou` `aluno_embarcou` TINYINT(1) NULL DEFAULT 0 COMMENT 'Armazena o status do embarque 0 - False ( não embarcou)\n1 - True - Embarcou' ;

ALTER TABLE `appescolar`.`tb_configuracoes` 
ADD COLUMN `notificacao_embarque_desembarque_escola` TINYINT(1) NULL DEFAULT 1 AFTER `notificacao_iniciar_rota`,
ADD COLUMN `notificacao_finalizar_rota` TINYINT(1) NULL DEFAULT 1 AFTER `notificacao_embarque_desembarque_escola`,
ADD COLUMN `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `notificacao_finalizar_rota`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
